define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/dom-style',
  '../defaults',
  'di18n/localize',
  'rdforms/view/Presenter',
  'entryscape-commons/util/dateUtil',
  'entryscape-commons/list/common/ExpandRow',
], (declare, lang, on, domConstruct, domStyle, defaults, localize, Presenter,
    dateUtil, ExpandRow) =>
  declare([ExpandRow], {
    compact: true,
    showCol3: false,
    postCreate() {
      this.revURI = this.entry.uri; // Hack to reuse EntryRow code
      this.currentRevision = this.list.entry.getEntryInfo().getGraph().findFirstValue(this.revURI, 'owl:sameAs') != null;
      this.inherited(arguments);
    },
    getPreviousRevision() {
      const entry = this.list.entry;
      const revs = entry.getEntryInfo().getMetadataRevisions();
      let currentRevFound = false;
      return revs.find((el) => {
        if (el.uri === this.revURI) {
          currentRevFound = true;
        } else if (currentRevFound) {
          return true;
        }
        return false;
      });
    },
    initExpandArea(node) {
      const entry = this.list.entry;
      const ei = entry.getEntryInfo();
      ei.getMetadataRevisionGraph(this.revURI).then((graph) => {
        this.graph = graph;
        const prev = this.getPreviousRevision();
        if (prev) {
          ei.getMetadataRevisionGraph(prev.uri).then((prevGraph) => {
            this.renderExpandArea(node, entry.getMetadata(), graph, prevGraph);
          });
        } else {
          this.renderExpandArea(node, entry.getMetadata(), graph);
        }
      });
    },

    renderExpandArea(node, currentGraph, revisionGraph, prevRevisionGraph) {
      const p = new Presenter({ compact: this.compact }, domConstruct.create('div', {
        style: { padding: '0px 0px 10px 15px' },
      }, node));
      const template = this.list.getTemplate();
      const entry = this.list.entry;
      const dialog = this.list.dialog;
      let disClass = '';
      let revertTitle = dialog.getRevertTitle();

      // Is this the current revision?
      if (this.currentRevision) {
        disClass = 'disabled';
        revertTitle = dialog.getCurrentRevisionRevertTitle();
      } else if (dialog.isSimilar(currentGraph, revisionGraph)) {
        if (dialog.hasExcludeDiff(currentGraph, revisionGraph)) {
          // Some change is there in the excluded properties
          disClass = 'disabled';
          revertTitle = dialog.getNoRevertSameGraphExcludeTitle();
        } else {
          // No change to currentGraph
          disClass = 'disabled';
          revertTitle = dialog.getNoRevertSameGraphTitle();
        }
      }

      // Check if the change to previous graph is in the excluded properties
      if (prevRevisionGraph != null && dialog.hasExcludeDiff(revisionGraph, prevRevisionGraph)) {
        domConstruct.create('div', {
          class: 'alert alert-info',
          innerHTML: dialog.getReasonForRevisionMessage(),
        }, node);
      }

      const b = this.nlsSpecificBundle;
      const buttonRow = domConstruct.create('div', { style: { 'min-height': '27px' } }, node);
      const button = domConstruct.create('button', {
        type: 'button',
        style: { 'pointer-events': 'visible' },
        class: `pull-right btn btn-primary ${disClass}`,
        title: revertTitle,
        innerHTML: '<span class="fa fa-level-up"></span>' +
        `&nbsp;<span>${b.revertLabel}</span>`,
      }, buttonRow);
      if (disClass === '') {
        on(button, 'click', lang.hitch(this, this.revert));
      }

      domStyle.set(this.details.firstChild, 'border-top', '0px');
      domStyle.set(this.detailsContainer, 'padding', '15px');

      p.show({ resource: entry.getResourceURI(), graph: revisionGraph, template });
    },

    getRenderNameHTML() {
      const e = this.entry.user;
      const rdfutils = defaults.get('rdfutils');
      const username = e.getEntryInfo().getName()
        || (e.getResource(true) && e.getResource(true).getName());
      const name = rdfutils.getLabel(e) || username;
      let date = dateUtil.getMultipleDateFormats(this.entry.time);
      date = `${date.dateMedium} ${date.timeMedium}`;
      const b = this.nlsSpecificBundle;
      const current = this.currentRevision ? b.currentRevision : '';
      if (name == null && username == null) {
        return localize(b, 'noUserNameRevision', { datetime: date, id: e.getId() }) + current;
      }
      return `${date}, ${name} ${current}`;
    },
    revert() {
      this.list.dialog.revert(this.list.entry, this.graph);
    },
  }));
