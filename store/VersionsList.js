define([
  'dojo/_base/declare',
  'dojo/promise/all',
  'dojo/dom-attr',
  './ArrayList',
  '../list/List',
  '../defaults',
  './VersionRow',
  'di18n/NLSMixin',
  'i18n!nls/escoList',
  'i18n!nls/escoVersions',
], (declare, all, domAttr, ArrayList, List, defaults, VersionRow, NLSMixin) =>
  declare([List, NLSMixin.Dijit], {
    nlsBundles: ['escoList', 'escoVersions'],
    includeHead: false,
    searchInList: true,
    rowClickDialog: 'expand',
    rowClass: VersionRow,

    postCreate() {
      this.registerRowAction({
        name: 'expand',
        button: 'link',
        iconType: 'fa',
        icon: 'chevron-right',
      });
      this.inherited(arguments);
    },
    localeChange() {
      this.updateLocaleStrings(this.NLSBundle0, this.NLSBundle1);
    },
    getTemplate() {
      return this.template;
    },
    show(entry, template) {
      this.entry = entry;
      this.template = template;
      this.render();
    },
    search() {
      const es = defaults.get('entrystore');
      const revs = this.entry.getEntryInfo().getMetadataRevisions();
      all(revs.map(rev => es.getEntry(rev.by).then((userEntry) => {
        rev.user = userEntry;
      }))).then(() => {
        this.listView.showEntryList(new ArrayList({ arr: revs }));
      });
    },
  }));
