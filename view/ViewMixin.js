define([
  'dojo/_base/declare',
  '../defaults',
], (declare, defaults) =>
  declare([], {
    /**
     * Check if there's a user sign-ed in.
     *
     * If overridden, follow the signature.
     *
     * @return {Promise}
     */
    canShowView() {
      const userEntryInfo = defaults.get('userInfo');
      const canShow = userEntryInfo != null && userEntryInfo.id !== '_guest';

      return new Promise(resolve => resolve(canShow));
    },
  }));
