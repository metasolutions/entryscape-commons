define([
  'dojo/_base/declare',
], declare => declare([], {
  /**
   * Provide a simple function that is checked in spa.
   * @return {Promise}
   */
  canShowView() {
    return Promise.resolve(true);
  },
}));
