define([
  'dojo/_base/declare',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./CommentDialogTemplate.html',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/html',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'dojo/dom-class',
  'entryscape-commons/list/common/ListDialogMixin',
  '../defaults',
  'dojo/on',
  './comments',
  './Comment',
  'i18n!nls/escoComment',
], (declare, _WidgetsInTemplateMixin, NLSMixin, TitleDialog, template, domAttr, domStyle,
    html, lang, domConstruct, domClass, ListDialogMixin, defaults, on, comments, Comment) =>

  declare([TitleDialog.ContentNLS, ListDialogMixin], {
    bid: 'escoCommentDialog',
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['escoComment'],
    nlsHeaderTitle: 'commentHeader',
    nlsFooterButtonLabel: 'commentFooterButton',
    __subjectInput: null,
    __commentInput: null,

    postCreate() {
      this.inherited(arguments);
      on(this.__commentInput, 'keyup', lang.hitch(this, this.check));
    },
    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.row = params.row;
      this._clear();
      this.clearcomments();
      this.listComments();
      this.dialog.show();
    },
    _clear() {
      domAttr.set(this.__commentInput, 'value', '');
      domAttr.set(this.__subjectInput, 'value', '');
    },
    check() {
      if (this.checkTimer) {
        clearTimeout(this.checkTimer);
        delete this.checkTimer;
      }
      this.checkTimer = setTimeout(lang.hitch(this, () => {
        delete this.checkTimer;
        if (lang.isFunction(this.domNode.checkValidity)) {
          valid = this.domNode.checkValidity();
        }
        this.validateComment();
      }), 300);
    },
    validateComment() {
      if (domAttr.get(this.__subjectInput, 'value').length === 0) {
        this.setStatus(this.__subjectInputStatus, this.NLSBundles0.invalidSubject);
      } else if (domAttr.get(this.__commentInput, 'value').length === 0) {
        this.setStatus(this.__commentInputStatus, this.NLSBundle0.invalidComment);
      } else {
        this.setStatus(this.__subjectInputStatus);
        this.setStatus(this.__commentInputStatus);
      }
    },
    setStatus(node, message) {
      if (message) {
        domStyle.set(node, 'display', '');
        html.set(node, message);
      } else {
        domStyle.set(node, 'display', 'none');
      }
    },
    decreaseReplyCount() {
      // Contract with row implementations.
      if (typeof this.row.decreaseReplyCount === 'function') {
        this.row.decreaseReplyCount();
      }
      this.clearcomments();
      this.listComments();
    },
    clearcomments() {
      domConstruct.empty(this.__commentsList);
    },
    listComments() {
      const self = this;
      comments.getReplyList(this.entry).forEach((commentEntry) => {
        Comment({ entry: commentEntry, parent: self, initialOpenReplies: true },
          domConstruct.create('div', null, self.__commentsList));
      });
    },
    footerButtonAction() {
      const subj = domAttr.get(this.__subjectInput, 'value');
      const comment = domAttr.get(this.__commentInput, 'value');

      if (subj === '') return this.NLSBundle0.invalidSubject;
      if (comment === '') return this.NLSBundle0.invalidComment;

      return comments.createReply(this.entry, subj, comment).then((commentEntry) => {
        domAttr.set(this.__commentInput, 'value', '');
        domAttr.set(this.__subjectInput, 'value', '');
        /* eslint-disable no-new */
        new Comment({ entry: commentEntry, parent: this },
          domConstruct.create('div', null, this.__commentsList, 'first'));
        // update comment count on list of candidates
        this.row.noOfComments = this.row.noOfComments + 1;
        this.row.renderCommentCount();
        return { stopHide: true };
      });
    },
  }));
