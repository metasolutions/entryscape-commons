define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/string',
  'dojo/date/locale',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'dojo/text!./CommentTemplate.html',
  '../defaults',
  'dojo/dom-style',
  'dojo/dom-construct',
  './comments',
  'di18n/localize',
  'dijit/focus',
  'entryscape-commons/util/dateUtil',
  'i18n!nls/escoComment',
], (declare, lang, string, locale, domAttr, domClass, _WidgetBase, _TemplatedMixin, NLSMixin,
    template, defaults, domStyle, domConstruct, comments, localize, focusUtil,
    dateUtil) => {
  const CommentCls = declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    bid: 'escoComment',
    templateString: template,
    __subject: null,
    __replies: null,
    __replyIcon: null,
    __commentInput: null,
    __commentEditButtons: null,
    __commentEditSave: null,
    __commentEditCancel: null,
    __userInfo: null,
    __dateInfo: null,
    __commentDisplay: null,
    __commentEditNode: null,
    __editComment: null,
    __commentReply: null,
    __commentsList: null,
    initialOpenReplies: false,

    nlsBundles: ['escoComment'],
    nlsSpecificBundle: 'escoComment',

    postCreate() {
      this.renderComments();
      if (this.isReply) {
        domClass.add(this.domNode, `${this.bid}--reply`);
      }
      this.inherited('postCreate', arguments);
    },
    localeChange() {
      this.updateLocaleStrings();
    },
    updateLocaleStrings() {
      if (this.NLSBundle0) {
        this.creationDateTitle = this.NLSBundle0.creationDateTitle;
        const tStr = string.substitute(this.creationDateTitle, { date: this.cDateFull });
        domAttr.set(this.__dateInfo, { title: tStr });
        if (this.noOfReplies > 0) {
          domStyle.set(this.__replies, 'display', '');
          domStyle.set(this.__replyIcon, 'display', '');
          domAttr.set(this.__replies, 'innerHTML', localize(this.NLSBundle0, 'reply', this.noOfReplies));
        } else {
          domStyle.set(this.__replies, 'display', 'none');
          domStyle.set(this.__replyIcon, 'display', 'none');
          domAttr.set(this.__editComment, 'disabled', false);
        }
      }
    },
    renderComments() {
      let commentTxt = comments.getCommentText(this.entry);
      const subjectTxt = comments.getCommentSubject(this.entry);
      domAttr.set(this.__commentInput, 'innerHTML', commentTxt);
      commentTxt = commentTxt.replace(/(\r\n|\r|\n)/g, '<br/>');
      domAttr.set(this.__commentDisplay, 'innerHTML', commentTxt);
      domAttr.set(this.__subject, 'innerHTML', subjectTxt);
      const self = this;
      const es = defaults.get('entrystore');
      const userResourceUri = this.entry.getEntryInfo().getCreator();
      es.getEntry(es.getEntryURIFromURI(userResourceUri)).then((userEntry) => {
        const user = defaults.get('rdfutils').getLabel(userEntry);
        domAttr.set(self.__userInfo, { innerHTML: user, title: 'User' });
      });
      const cDate = this.entry.getEntryInfo().getCreationDate();
      const mDateFormats = dateUtil.getMultipleDateFormats(cDate);
      this.cDateFull = mDateFormats.full;
      domAttr.set(this.__dateInfo, { innerHTML: mDateFormats.short });
      this.updateRepliesCount();
      if (this.initialOpenReplies && this.noOfReplies > 0) {
        this.toggleReplies();
      }
    },
    updateRepliesCount() {
      // Assuming comment resourceURI is repository local
      this.noOfReplies = comments.getNrOfReplies(this.entry);
      if (this.noOfReplies <= 0) {
        // enable edit
        domAttr.set(this.__editComment, 'disabled', false);
      }
      this.updateLocaleStrings();
    },
    toggleReplies() {
      this.__repliesArr = [];
      if (domStyle.get(this.__commentsList, 'display') === 'none') {
        domClass.remove(this.__replyIcon, 'fa-caret-right', true);
        domClass.add(this.__replyIcon, 'fa-caret-down');
        domStyle.set(this.__commentsList, 'display', '');
        domConstruct.empty(this.__commentsList);
        const self = this;
        comments.getReplyList(this.entry).forEach((commentEntry) => {
          this.__repliesArr.push(new CommentCls({
            entry: commentEntry,
            parent: self,
            isReply: true }, domConstruct.create('div', null, self.__commentsList)));
        });
      } else {
        domClass.remove(this.__replyIcon, 'fa-caret-down', true);
        domClass.add(this.__replyIcon, 'fa-caret-right', true);
        this.closeReplies();
      }
    },
    deleteComment() {
      const dialogs = defaults.get('dialogs');
      const confirmMessage = localize(this.NLSBundle0, 'removeComment', this.noOfReplies);
      dialogs.confirm(confirmMessage, null, null, (confirm) => {
        if (confirm) {
          comments.deleteCommentAndReplies(this.entry)
            .then(lang.hitch(this.parent, this.parent.decreaseReplyCount));
        }
      });
    },
    decreaseReplyCount() {
      this.noOfReplies -= 1;
      this.clearcomments();
      this.listComments();
    },
    editComment() {
      // enable editing and save
      domStyle.set(this.__commentDisplay, 'display', 'none');
      domStyle.set(this.__commentEditNode, 'display', '');
      domAttr.set(this.__commentInput, 'disabled', false);
      domStyle.set(this.__commentEditButtons, 'display', '');
      focusUtil.focus(this.__commentInput);
    },
    editSave() {
      const self = this;
      let commentTxt = domAttr.get(self.__commentInput, 'value');
      const metadata = self.entry.getMetadata();
      const stmt = metadata.find(null, 'oa:hasBody')[0];
      metadata.findAndRemove(null, 'rdf:value');
      metadata.addL(stmt.getValue(), 'rdf:value', commentTxt);
      // var cstmt = metadata.find(null, "cnt:chars")[0];
      // cstmt.setValue(commentTxt);
      self.entry.commitMetadata().then((newCommentEntry) => {
        self.entry = newCommentEntry;
        commentTxt = commentTxt.replace(/(\r\n|\r|\n)/g, '</br>');
        domAttr.set(self.__commentDisplay, 'innerHTML', commentTxt);
        domStyle.set(self.__commentDisplay, 'display', '');
        domAttr.set(self.__commentInput, 'disabled', true);
        domStyle.set(self.__commentEditButtons, 'display', 'none');
        domStyle.set(self.__commentEditNode, 'display', 'none');
      });
    },
    editCancel() {
      domStyle.set(this.__commentDisplay, 'display', '');
      domStyle.set(this.__commentEditNode, 'display', 'none');
      domAttr.set(this.__commentInput, 'disabled', true);
      domStyle.set(this.__commentEditButtons, 'display', 'none');
    },
    replyComment() {
      domStyle.set(this.__commentReply, 'display', '');
      const subjectTxt = `Re: ${this.entry.getMetadata().findFirstValue(null, 'dcterms:title')}`;
      domAttr.set(this.subjectNode, 'value', subjectTxt);
      focusUtil.focus(this.commentNode);// ESCO-41 fix
    },
    postReply() {
      const self = this;
      const commentTxt = domAttr.get(self.commentNode, 'value');
      const subjectTxt = domAttr.get(self.subjectNode, 'value');
      comments.createReply(this.entry, subjectTxt, commentTxt)
        .then((commentEntry) => {
          domAttr.set(self.commentNode, 'value', '');
          self.noOfReplies += 1;
          self.updateLocaleStrings();
          self.cancelReply();
          domClass.remove(self.__replyIcon, 'fa-caret-right', true);
          domClass.add(self.__replyIcon, 'fa-caret-down');
          // domClass.toggle(self.__replyIcon, "fa-compress");
          domStyle.set(self.__commentsList, 'display', '');
          domAttr.set(self.__editComment, 'disabled', true);
          // self.closeReplies();
          if (this.__repliesArr == null) {
            this.__repliesArr = [];
          }
          this.__repliesArr.push(new CommentCls({ entry: commentEntry, parent: self },
            domConstruct.create('div', null, self.__commentsList, 'first')));
        });
    },
    cancelReply() {
      domStyle.set(this.__commentReply, 'display', 'none');
      domAttr.set(this.commentNode, 'innerHTML', '');
      domAttr.set(this.subjectNode, 'innerHTML', '');
    },
    closeReplies() {
      domStyle.set(this.__commentsList, 'display', 'none');
      domConstruct.empty(this.__commentsList);
    },
    clearcomments() {
      domConstruct.empty(this.__commentsList);
    },
    listComments() {
      domStyle.set(this.__commentsList, 'display', 'none');
      this.toggleReplies();
      this.updateLocaleStrings();
    },
  });

  return CommentCls;
});

