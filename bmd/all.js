define([
  'jquery',
  '../defaults',
  './util',
  'dojo/topic',
  'bmd/js/material',
  'bmd/js/ripples',
], (jquery, defaults, util, topic) => {
  const MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

  /**
   * Observer DOM changes (only additions) and call the callback
   * @param obj
   * @param callback
   */
  const observeDOMAdditions = (obj, callback) => {
    // define a new observer
    const obs = new MutationObserver(callback);
    // have the observer observe foo for changes in children

    obs.observe(obj, {
      childList: true,
      subtree: true,
    });
  };

  // initializeMaterial is not called more than once per X ms
  const updateMaterial = util.throttle(() => {
    if (jquery.material) {
      jquery.material.ripples();
      jquery.material.input();
      jquery.material.checkbox();
      jquery.material.radio();
      jquery.material.togglebutton();
//      jquery.material.init();
    }
  }, 500, { leading: false });


  // run only once, when first view is loaded
  const viewListener = topic.subscribe('spa/viewLoaded', () => {
    jquery.material.init();
    observeDOMAdditions(jquery('#viewsNode')[0], updateMaterial); // main content
    observeDOMAdditions(jquery('#entryscapeDialogs')[0], updateMaterial); // side dialogs
    viewListener.remove();
  });
});
