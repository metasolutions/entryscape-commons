define([
  'dojo/_base/declare',
  'dojo/_base/array',
  'dojo/promise/all',
  'dojo/_base/lang',
  'di18n/locale',
  'entryscape-commons/store/ArrayList',
  'store/types',
  'store/promiseUtil',
  'entryscape-commons/defaults',
], (declare, array, all, lang, locale, ArrayList, types, promiseUtil, defaults) => {
  const es = defaults.get('entrystore');

  /**
   * A Group-Context-Entry list (GCEList) provides a list of entries corresponding to objects
   * (like data-catalogs and conceptschemes) maintained in separate contexts.
   * Keeping the objects (and all belonging entries) in separate context provides
   * good maintainability and access control. A separate group is also defined per context
   * to allow collaboration. The current user may have access to zero or more of
   * these objects which is detected by checking memberships of groups, checking
   * their home-contexts and then for a corresponding entry in each context.
   */
  return declare(ArrayList, {
    constructor(params) {
      this.userEntry = params.userEntry;
      this.entryType = params.entryType;
      this.graphType = params.graphType;
      this.contextType = params.contextType;
      this.term = params.term;
      this.sortOrder = params.sortOrder;
      // this.entryId = params.entryId;
      this.contextId2groupId = {};
    },
    setGroupIdForContext(contextId, groupId) {
      this.contextId2groupId[contextId] = groupId;
    },
    getGroupId(contextId) {
      return this.contextId2groupId[contextId];
    },

    loadEntries() {
      let groupURIs = this.userEntry.getParentGroups();
      groupURIs.push(es.getEntryURI('_principals', '_users'));
      const cids = [];
      groupURIs = array.map(groupURIs, euri =>
        es.getResourceURI(es.getContextId(euri), es.getEntryId(euri)));
      const query = es.newSolrQuery().title(this.term).graphType(types.GT_CONTEXT)
        .resourceWrite(groupURIs);
      if (this.contextType) {
        query.rdfType(this.contextType);
      }
      if (this.sortOrder === 'title') {
        if (this.entryType != null && this.entryType !== '') {
          // List contains entries, hence when searching for context
          // the title searched against is copied over without language.
          query.sort('title.nolang+desc');
        } else {
          const l = locale.get();
          query.sort(`title.${l}+desc`);
        }
      } else {
        query.sort('modified+asc');
      }
      return query.list().forEach((contextEntry) => {
        // Do additional checks, e.g. really homecontext of one of the parentGroups?
        cids.push(contextEntry.getId());
      }).then(() => this.getEntriesForContextIds(cids));
      /*      return all(array.map(groupURIs, guri => es.getEntry(guri)))
        .then(lang.hitch(this, this.extractEntriesFromGroups))
        .then((entries) => {
          this.entries = entries;
          return entries;
        });*/
    },

    extractEntriesFromGroups(groupEntryArr) {
      const hcArr = [];
      array.map(groupEntryArr, function (ge) {
        if (ge != null) {
          const hc = ge.getResource(true).getHomeContext();
          if (hc != null) {
            this.setGroupIdForContext(hc, ge.getId());
            hcArr.push(hc);
          }
        }
      }, this);

      return this.getEntriesForContextIds(hcArr);
    },

    getEntriesForContextIds(cidArr) {
      const getEntryForCid = (cid) => {
        if (this.entryType != null && this.entryType !== '') {
          return defaults.get('entrystoreutil').getEntryByType(
            this.entryType, es.getContextById(cid));
        } else if (this.graphType != null && this.graphType !== '') {
          return defaults.get('entrystoreutil').getEntryByGraphType(
            this.graphType, es.getContextById(cid));
        }
        return es.getEntry(es.getEntryURI('_contexts', cid));
      };

      this.entries = [];
      return promiseUtil.forEach(cidArr, (cid) => {
        if (cid) {
          return getEntryForCid(cid).then((entry) => {
            this.entries.push(entry);
          }, () => {}); // Handle missing entry in context, just ignore to show those that do exist.
        }
        return null;
      }).then(() => this.entries);
    },
  });
});
