define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'entryscape-commons/defaults',
  'entryscape-commons/nav/Cards',
], (declare, lang, defaults, Cards) => declare([Cards], {
  entryType: '', // E.g. "dcat:Catalog".
  getViewLabel(view, params, callback) {
    const context = defaults.get('context');
    if (context) {
      if (this.entryType == null || this.entryType === '') {
        context.getEntry().then((entry) => {
          const rdfutils = defaults.get('rdfutils');
          const name = entry === null ? '?' : rdfutils.getLabel(entry) || '-';
          callback(name, name);
        });
      } else {
        defaults.get('entrystoreutil').getEntryByType(this.entryType, context)
          .then(lang.hitch(this, (entry) => {
            const rdfutils = defaults.get('rdfutils');
            const name = entry === null ? '?' : rdfutils.getLabel(entry) || '-';
            callback(name, name);
          }));
      }
    } else {
      callback('?');
    }
  },
}));
