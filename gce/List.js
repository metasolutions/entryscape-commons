define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-style',
  'di18n/locale',
  'entryscape-commons/defaults',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/placeholder/ViewPlaceholder',
  'config',
  'dojo/dom-attr',
  'store/types',
  './GCEList',
  './GCERow',
], (declare, lang, domStyle, locale, defaults, BaseList, ViewPlaceholder, config, domAttr, types,
    GCEList, GCERow) => {
  const OpenGCE = declare([], {
    open(params) {
      const site = defaults.get('siteManager');
      const view = params.row.list.rowClickView;
      const context = params.row.getContext().getId();

      site.render(view, { context });
    },
  });

  return declare([BaseList], {
    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    includeExpandButton: false,
    nlsGCEPublicTitle: '',
    nlsGCEProtectedTitle: '',
    nlsGCESharingNoAccess: '',
    nlsGCEConfirmRemoveRow: '',
    nlsGroupSharingProblem: '',
    rowClass: GCERow,
    rowClickDialog: 'openGCE',
    rowClickView: '',
    entryType: '',
    graphType: '',
    contextType: '',
    entriesAreContextEntries: false,
    searchInList: true,
    searchVisibleFromStart: false,
    placeholderClass: ViewPlaceholder,
    nlsHeaderInfoKey: 'headerInfo',

    postCreate() {
      this.entriesAreContextEntries = this.entryType == null || this.entryType === '';
      this.inherited('postCreate', arguments);
      this.registerDialog('openGCE', OpenGCE);
    },

    render() {
      if (defaults.get('hasAdminRights')) {
        domStyle.set(this.getView().expandButton, 'display', '');
      } else {
        domStyle.set(this.getView().expandButton, 'display', 'none');
      }
      this.inherited(arguments);
    },

    updateLocaleStrings(generic, specific) {
      if (specific) {
        this.nlsSpecificBundle = specific || this.nlsSpecificBundle;
        const nlsText = specific && (specific[this.nlsHeaderInfoKey] || '');
        if (nlsText !== '') {
          domStyle.set(this.getView().headerInfo, 'display', 'block');
          domAttr.set(this.getView().listInfo, 'innerHTML', nlsText);
        } else {
          domStyle.set(this.getView().headerInfo, 'display', 'none');
        }
      }
      this.inherited(arguments);
    },

    rowMetadataUpdated(row, onlyRefresh) {
      this.inherited(arguments);
      const context = row.getContext();
      const es = context.getEntryStore();

      const label = row.getRenderName();
      if (onlyRefresh) {
        return;
      }

      // Update the group name, but only if there is a single group
      // that have the context as homeContext
      const updateGroupForContext = function (contextEntry) {
        const hcs = contextEntry.getReferrers('store:homeContext');
        if (hcs.length === 1) {
          const groupURI = es.getEntryURIFromURI(hcs[0]);
          es.getEntry(groupURI).then((groupEntry) => {
            const md = groupEntry.getMetadata();
            md.findAndRemove(null, 'foaf:name');
            md.addL(groupEntry.getResourceURI(), 'foaf:name', label);
            groupEntry.commitMetadata().then(() => {
              groupEntry.setRefreshNeeded(true);
            });
          });
        }
      };

      if (!this.entriesAreContextEntries) {
        // Must force load context entry since it's modification date is
        // updated whenever a containing entry is changed.
        es.getEntry(context.getEntryURI(), { forceLoad: true }).then((contextEntry) => {
          // Update the context title
          const md = contextEntry.getMetadata();
          md.findAndRemove(null, 'dcterms:title');
          md.addL(contextEntry.getResourceURI(), 'dcterms:title', label);
          contextEntry.commitMetadata().then(() => {
            contextEntry.setRefreshNeeded(true);
          });

          updateGroupForContext(contextEntry);
        });
      } else {
        updateGroupForContext(row.entry);
      }
    },

    installActionOrNot(params, row) {
      if (!defaults.get('hasAdminRights')) {
        // Should work for non-administrators as then the contextEntry has been searched
        // for before accessing the entry within (such as an dcat:Catalog)
        // and is therefore in cache
        const contextEntry = row.getContext().getEntry(true);
        switch (params.name) {
          case 'edit':
            return contextEntry.canWriteMetadata();
          case 'remove':
            return contextEntry.canAdministerEntry();
          case 'info':
            return contextEntry.canReadMetadata();
          default:
        }
      }
      return this.inherited(arguments);
    },

    search(paramsParam) {
      params = paramsParam || {};
      if (!defaults.get('hasAdminRights')) {
        defaults.get('userEntry', lang.hitch(this, (userEntry) => {
          this.entryList = new GCEList({
            userEntry,
            entryType: this.entryType,
            graphType: this.graphType,
            contextType: this.contextType,
            term: params.term,
            sortOrder: params.sortOrder,
          });
          this.listView.showEntryList(this.entryList);
        }));
      } else if (!this.entryType && !this.userEntry && !this.graphType) {
        const query = defaults.get('entrystore').newSolrQuery().graphType(types.GT_CONTEXT);
        if (this.contextType !== '') {
          query.rdfType(this.contextType);
        }
        if (params.term != null && params.term.length > 0) {
          if (config.entrystore.defaultSolrQuery === 'all') {
            query.all(params.term);
          } else {
            query.title(params.term);
          }
        }
        if (config.entrystore.defaultSolrLimit) {
          query.limit(config.entrystore.defaultSolrLimit);
        }
        if (params.sortOrder === 'title') {
          const l = locale.get();
          query.sort(`title.${l}+asc`);
        }
        this.listView.showEntryList(query.list());
      } else {
        this.inherited(arguments);
      }
    },
  });
});
