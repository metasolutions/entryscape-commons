define([
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/html',
  'di18n/locale',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
], (lang, declare, html, locale, domStyle, domConstruct, _WidgetBase) => {
  const defaultPadding = '20px';
  return declare([_WidgetBase], {
    path: '',
    localized: false,
    padding: true,

    buildRendering() {
      this.domNode = domConstruct.create('div', { class: 'textView' });
    },

    show(params) {
      const path = params.path || this.path;
      if (this.padding !== '') {
        if (this.padding === true) {
          domStyle.set(this.domNode, 'padding', defaultPadding);
        } else if (typeof this.padding === 'string') {
          domStyle.set(this.domNode, 'padding', this.padding);
        }
      }
      if (this.localized) {
        const language = locale.get();
        require([`dojo/text!${path}_${language}.html`], lang.hitch(this, (text) => {
          html.set(this.domNode, text);
        }), () => {
          require([`dojo/text!${path}.html`], lang.hitch(this, (text) => {
            html.set(this.domNode, text);
          }));
        });
      } else {
        require([`dojo/text!${path}.html`], lang.hitch(this, (text) => {
          html.set(this.domNode, text);
        }));
      }
    },
  });
});
