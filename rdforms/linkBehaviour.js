define([
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'config',
  'rdforms/model/system',
  'entryscape-commons/defaults',
  'entryscape-commons/create/typeIndex',
  'entryscape-commons/contentview/ContentViewSideDialog',
], (lang, on, domAttr, domConstruct, domClass, config, system, defaults, typeIndex,
    ContentViewSideDialog) => {
  const entrystoreutil = defaults.get('entrystoreutil');
  const choiceClick = (item, conf, entry) => {
    let dialog = null;
    dialog = new ContentViewSideDialog({
      hideComplete() {
        dialog.dialog.destroy();
        dialog.destroy();
      },
    });
    dialog.show(entry, null, conf);
  };
  system.attachLinkBehaviour = (node, binding) => {
    const item = binding.getItem();
    if (item.getType() === 'choice') {
      const conf = typeIndex.getConfFromConstraints(item.getConstraints());
      if (conf) {
        on(node, 'click', (e) => {
          e.preventDefault();
          e.stopPropagation();
          const stmt = binding.getStatement();
          const fromGraph = () => {
            const _temp = defaults.get('entrystore').getContextById('__temp');
            choiceClick(item, conf, _temp.newLink(stmt.getValue()).setMetadata(stmt.getGraph()));
          };
          if (stmt.isObjectBlank()) {
            fromGraph();
          } else {
            entrystoreutil.getEntryByResourceURI(binding.getValue()).then(
              choiceClick.bind(null, item, conf), fromGraph);
          }
        });
        return;
      }
    }
    domAttr.set(node, 'target', '_blank');
    domClass.add(node, 'spaExplicitLink');
    domConstruct.create('i', {
      class: 'fa fa-external-link rdformsExternalLink',
      'aria-hidden': 'true',
    }, node);
  };
  system.attachExternalLinkBehaviour = (node, binding) => {
    domAttr.set(node, 'target', '_blank');
    domClass.add(node, 'spaExplicitLink');
    domConstruct.create('i', {
      class: 'fa fa-external-link rdformsExternalLink',
      'aria-hidden': 'true',
    }, node);
  };
});
