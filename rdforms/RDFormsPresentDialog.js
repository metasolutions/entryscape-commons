define([
  'dojo/_base/declare',
  'dijit/_WidgetsInTemplateMixin',
  'rdfjson/Graph',
  'rdforms/view/Presenter', // In template
  '../dialog/TitleDialog', // In template
  'dojo/text!./RDFormsPresentDialogTemplate.html',
  'i18n!nls/escoRdforms',
], (declare, _WidgetsInTemplateMixin, Graph, Presenter, TitleDialog, template) =>
  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin], {
    templateString: template,
    nlsBundles: ['escoRdforms'],
    nlsHeaderTitle: 'metadataPresentDialogHeader',
    nlsFooterButtonLabel: 'metadataPresentDialogCloseLabel',

    show(uri, graph, tmpl) {
      this.uri = uri;
      this.graph = new Graph(graph.exportRDFJSON());
      this.template = tmpl;
      this.presenter.show({ resource: uri, graph: this.graph, template: tmpl });
      this.dialog.show();
    },
  }));
