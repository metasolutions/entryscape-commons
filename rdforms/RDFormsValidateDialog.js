define([
  'dojo/_base/declare',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'rdfjson/Graph',
  'rdforms/view/ValidationPresenter', // In template
  '../dialog/TitleDialog', // In template
  'dojo/text!./RDFormsPresentDialogTemplate.html',
  'rdforms/view/bootstrap/all',
  'i18n!nls/escoRdforms',
], (declare, _WidgetsInTemplateMixin, NLSMixin, Graph, ValidationPresenter,
    TitleDialog) =>

  declare([TitleDialog.Content, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: "<div><div data-dojo-attach-point='validator' " +
    "data-dojo-props='compact: true' " +
    "data-dojo-type='rdforms/view/ValidationPresenter'></div></div>",
    maxWidth: 0,
    nlsBundles: ['escoRdforms'],
    nlsHeaderTitle: 'metadataValidateDialogHeader',
    nlsFooterButtonLabel: 'metadataValidateDialogCloseLabel',
    title: '',
    closeLabel: '',

    localeChange() {
      if (this.title === '') {
        this.dialog.updateLocaleStrings(this.NLSBundle0);
      } else {
        const mesg = {};
        mesg[this.nlsHeaderTitle] = this.title;
        mesg[this.nlsFooterButtonLabel] = this.closeLabel;
        this.dialog.updateLocaleStrings(mesg);
      }
    },
    show(uri, graph, template) {
      this.uri = uri;
      this.graph = new Graph(graph.exportRDFJSON());
      this.template = template;
      this.validator.show({ resource: uri, graph: this.graph, template });
      this.dialog.show();
    },
  }));
