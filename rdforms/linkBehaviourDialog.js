define([
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'config',
  'rdforms/model/system',
  'entryscape-commons/defaults',
  'entryscape-commons/create/typeIndex',
  'rdforms/view/Presenter',
  'jquery',
], (lang, on, domAttr, domConstruct, config, system, defaults, typeIndex, Presenter, jquery) => {
  const entrystoreutil = defaults.get('entrystoreutil');
  const choiceClick = (aroundNode, item, conf, entry) => {
    const presenter = new Presenter({}, domConstruct.create('div'));
    const template = defaults.get('itemstore').getItem(conf.template);
    presenter.show({
      resource: entry.getResourceURI(),
      graph: entry.getMetadata(),
      template,
    });
    let label = item.getLabel();
    if (label != null && label !== '') {
      label = label.charAt(0).toUpperCase() + label.slice(1);
    } else {
      label = '';
    }

    const popoverOptions = {
      html: true,
      container: jquery('#entryscape_dialogs')[0], // provided in defaults
      placement: 'auto',
      trigger: 'manual',
      template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
      title: label,
      content: presenter.domNode,
    };
    const jn = jquery(aroundNode).popover(popoverOptions).attr('data-toggle', 'popover');
    setTimeout(() => {
      jn.popover('show');
    }, 30);
  };

  system.attachLinkBehaviour = (node, binding) => {
    const item = binding.getItem();
    if (item.getType() === 'choice') {
      const conf = typeIndex.getConfFromConstraints(item.getConstraints());
      if (conf) {
        on(node, 'click', (e) => {
          e.preventDefault();
          entrystoreutil.getEntryByResourceURI(
            binding.getValue()).then(lang.hitch(null, choiceClick, node, item, conf));
        });
        return;
      }
    }
    domAttr.set(node, 'target', '_blank');
  };
});
