define([
  'dojo/_base/declare',
  'dojo/text!./PresentExpandableTemplate.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'rdforms/view/Presenter', // In template
  'rdfjson/Graph',
  'dojo/dom-construct',
  'dojo/dom-style',
  'jquery',
  'di18n/NLSMixin',
  'entryscape-commons/util/uiUtil',
  'dojo/window',
  'dojo/_base/fx',
  'dojo/dom-geometry',
  'dojo/_base/lang',
  'i18n!nls/escoRdforms',
  'i18n!nls/escoPresentExpandable',
], (declare, template, _WidgetBase, _TemplatedMixin, Presenter, Graph, domConstruct, domStyle,
    jquery, NLSMixin, uiUtil, win, basefx, domGeometry, lang) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escoRdforms', 'escoPresentExpandable'],
    maxHeight: 115,
    bid: 'escoPresentExpandable',
    postCreate() {
      this.presenter = new Presenter({ compact: true }, domConstruct.create('div', null, this.__presenter));
      domStyle.set(this.__presenterPanel, 'max-size', this.maxHeight);
      domStyle.set(this.__external, 'display', 'none');
      domStyle.set(this.__showMore, 'display', 'none');
      domStyle.set(this.__showLess, 'display', 'none');
      this.inherited(arguments);
    },
    localeChange() {
      const popoverOptions = uiUtil.getPopoverOptions();
      popoverOptions.title = this.NLSBundle1.popoverHelpTitle;
      popoverOptions.content = this.NLSBundle1.popoverHelpContent;
      jquery(this.__help).popover(popoverOptions);
    },
    show(uri, graph, tmpl) {
      domStyle.set(this.__external, 'display', 'block');
      this.uri = uri;
      this.graph = new Graph(graph.exportRDFJSON());
//            this.graph = new Graph();
//            this.graph.addL(uri, "dcterms:title", "Hello");
      this.template = tmpl;
      this.presenter.show({ resource: uri, graph: this.graph, template: tmpl });
      domStyle.set(this.__showMore, 'display', 'none');
      domStyle.set(this.__showLess, 'display', 'none');
      // add delay to complete rendering
      const delay = 100;
      this._timer = setTimeout(lang.hitch(this, () => {
        // this.computedStyle = domStyle.getComputedStyle(this.__presenterPanel);
        this.prefferedH = domGeometry.getContentBox(this.__presenter).h + 15;
        if (this.prefferedH > this.maxHeight) {
          domStyle.set(this.__showMore, 'display', 'block');
          domStyle.set(this.__shader, 'display', 'block');
          this.animate(1, this.maxHeight);
        } else {
          domStyle.set(this.__footerPanel, 'display', 'none');
          this.animate(1, this.prefferedH);
        }
        delete this._timer;
      }), delay);
    },
    showMore() {
      domStyle.set(this.__showMore, 'display', 'none');
      domStyle.set(this.__showLess, 'display', 'block');
      domStyle.set(this.__shader, 'display', 'none');
      this.animate(this.maxHeight, this.prefferedH);
    },
    showLess() {
      domStyle.set(this.__showMore, 'display', 'block');
      domStyle.set(this.__showLess, 'display', 'none');
      domStyle.set(this.__shader, 'display', 'block');
      this.animate(this.prefferedH, this.maxHeight);
    },
    animate(start, end) {
      const slide = basefx.animateProperty({
        node: this.__presenterPanel,
        properties: {
          height: { end, start, units: 'px' },
        },
      });
      slide.play();
    },
  }));
