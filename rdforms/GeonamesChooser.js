/* global define*/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/event',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'di18n/localize',
  '../defaults',
  'config',
  '../dialog/HeaderDialog', // In template
  'rdforms/view/renderingContext',
  'dojo/text!./GeonamesChooserTemplate.html',
  'i18n!nls/escoRdforms',
], (declare, lang, array, event, on, domConstruct, domAttr,
    _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin, localize,
    defaults, config, HeaderDialog, renderingContext, template) => {
  const username = 'metasolutions';
  const APIconfig = {
    startId: '6295630', // World
    URIPrefix: "http://sws.geonames.org/",
    hierarchyURL: `http://api.geonames.org/hierarchyJSON?userName=${username}&lang=en&geonameId=`,
    childrenURL: `http://api.geonames.org/childrenJSON?userName=${username}&lang=en&geonameId=`,
    geonameURL: `http://api.geonames.org/getJSON?userName=${username}&lang=en&geonameId=`
  };

  const getHierarchy = (geonameId) => {
    const url = `${APIconfig.hierarchyURL}${geonameId}`;
    return defaults.get('entrystore').loadViaProxy(url).then(results => results.geonames);
  };

  const getChildren = (geonameId) => {
    const url = `${APIconfig.childrenURL}${geonameId}`;
    return defaults.get('entrystore').loadViaProxy(url).then(results => results.geonames);
  };

  const getGeoname = (geonameId) => {
    const url = `${APIconfig.geonameURL}${geonameId}`;
    return defaults.get('entrystore').loadViaProxy(url);
  };

  const getGeonameId = uri => uri.substr(APIconfig.URIPrefix.length).replace(/\/$/g, '');

  const getGeonameURI = geonameId => `${APIconfig.URIPrefix}${geonameId}`;

  const GeonamesChooser =
    declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
      templateString: template,
      binding: null,
      onSelect: null,
      nlsBundles: ['escoRdforms'],

      postCreate() {
        this.ownerDocumentBody.appendChild(this.domNode);
        APIconfig.startId = config.itemstore && config.itemstore.geonamesStart != null
          ? config.itemstore.geonamesStart : APIconfig.startId;
        APIconfig.URIPrefix = config.itemstore && config.itemstore.geonamesURIPrefix != null
          ? config.itemstore.geonamesURIPrefix : APIconfig.URIPrefix;
        APIconfig.hierarchyURL = config.itemstore && config.itemstore.geonamesHierarchyURL != null
          ? config.itemstore.geonamesHierarchyURL : APIconfig.hierarchyURL;
        APIconfig.childrenURL = config.itemstore && config.itemstore.geonamesChildrenURL != null
          ? config.itemstore.geonamesChildrenURL : APIconfig.childrenURL;
        APIconfig.geonameURL = config.itemstore && config.itemstore.geonamesGeonameURL != null
          ? config.itemstore.geonamesGeonameURL : APIconfig.geonameURL;
        this.inherited('postCreate', arguments);
      },

      localeChange() {
      },

      setCurrent(geoObj) {
        this.currentGeoObj = geoObj;
        domAttr.set(this.name, 'innerHTML', geoObj.name);
        domAttr.set(this.pop, 'innerHTML', geoObj.population);
        domAttr.set(this.lat, 'innerHTML', geoObj.lat);
        domAttr.set(this.lng, 'innerHTML', geoObj.lng);
        domAttr.set(this.subdivision, 'innerHTML',
          localize(this.NLSBundle0, 'geoNameSubdivision', { 1: geoObj.name }));
      },

      focusOn(geonameId) {
        domAttr.set(this.path, 'innerHTML', '');
        domAttr.set(this.list, 'innerHTML', '');
        getHierarchy(geonameId).then(lang.hitch(this, (hierarchy) => {
          hierarchy.forEach((p) => {
            if (p.geonameId === geonameId) {
              domConstruct.create('li', { class: 'active', innerHTML: p.name }, this.path);
            } else {
              const li = domConstruct.create('li', null, this.path);
              const a = domConstruct.create('a', { href: '#', innerHTML: p.name }, li);
              on(a, 'click', lang.hitch(this, (e) => {
                this.focusOn(p.geonameId);
                event.stop(e);
              }));
            }
          }, this);
          if (hierarchy.length > 0) {
            this.setCurrent(hierarchy[hierarchy.length - 1]);
          }
        }));
        getChildren(geonameId).then(lang.hitch(this, (children) => {
          children.forEach((p) => {
            const a = domConstruct.create('a', {
              href: '#',
              innerHTML: p.name,
              class: 'list-group-item',
            }, this.list);
            on(a, 'click', lang.hitch(this, (e) => {
              this.focusOn(p.geonameId);
              event.stop(e);
            }));
          }, this);
        }));
      },

      select() {
        const choice = {
          value: getGeonameURI(this.currentGeoObj.geonameId),
          label: { en: this.currentGeoObj.name },
          inlineLabel: true,
        };
        this.onSelect(choice);
        this.dialog.hide();
      },
      show(binding, onSelect) {
        // choice.inlineLabel
        this.binding = binding;
        this.onSelect = onSelect;
        if (binding.getValue() === '') {
          this.focusOn(APIconfig.startId);
        } else {
          this.focusOn(getGeonameId(binding.getValue()));
        }
        this.dialog.show();
      },
    });

  let defaultRegistered = false;
  let gChooser = null;

  const init = () => {
    if (gChooser == null) {
      gChooser = new GeonamesChooser();
      gChooser.startup();
    }
  };

  const ext = {
    getChoice(entry, value) {
      init();
      const obj = {
        value,
        load(onSuccess) {
          const async = defaults.get('asynchandler');
          async.addIgnore('loadViaProxy', async.codes.GENERIC_PROBLEM, true);
          getHierarchy(getGeonameId(value)).then((result) => {
            if (result.length > 0 && result[result.length - 1].name) {
              obj.label = { en: result[result.length - 1].name };
              return obj;
            }
            throw Error('Damn');
          }).then(onSuccess, () => {
            obj.label = `Could not load name for geoname ID: ${value.substr(24, value.length - 25)}`;
            obj.mismatch = true; // TODO replace with something else
            onSuccess();
          });
        },
      };
      return obj;
    },
    show(binding, onSelect) {
      init();
      gChooser.show(binding, onSelect);
    },
    registerDefaults() {
      if (!defaultRegistered) {
        defaultRegistered = true;
        renderingContext.chooserRegistry.predicate('http://purl.org/dc/terms/spatial').register(ext);
        renderingContext.chooserRegistry.constraint({ 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'http://www.geonames.org/ontology#Feature' }).register(ext);
      }
    },
    chooser: GeonamesChooser,
  };
  return ext;
});
