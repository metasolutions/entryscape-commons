define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/event',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/dom-prop',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'di18n/localize',
  '../defaults', // Provides namespaces, context, entrystore and rdfutils in registry.
  'entryscape-commons/tree/Tree',
  'entryscape-commons/tree/TreeModel',
  'entryscape-commons/tree/skos/util',
  '../dialog/TitleDialog', // In template
  'rdforms/view/renderingContext',
  'dojo/text!./SkosChooserTemplate.html',
  'config',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/tree/skos/ConceptRow',
  'dojo/promise/all',
  'jquery',
  'entryscape-commons/util/cssUtil',
  'i18n!nls/escoSkosChooser',
  'i18n!nls/escoRdforms',
], (declare, lang, event, domConstruct, domAttr, domStyle, domClass, domProp,
    _WidgetsInTemplateMixin, NLSMixin, localize, defaults, Tree, TreeModel, skosUtil, TitleDialog,
    renderingContext, template, config, BaseList, ConceptRow, all, jquery, cssUtil) => {
  const ns = defaults.get('namespaces');
  const esu = defaults.get('entrystoreutil');
  const rdfType = ns.expand('rdf:type');
  const rdfutils = defaults.get('rdfutils');
  const getLabel = (entry) => {
    let label = rdfutils.getLabel(entry);
    if (label.match('^[0-9]+$')) {
      const alt = entry.getMetadata().findFirstValue(entry.getResourceURI(), 'skos:altLabel');
      if (alt) {
        label += ` - ${alt}`;
      }
    }
    return label;
  };
  const getChoice = (entry, obj) => {
    const o = obj || {};
    o.value = entry.getResourceURI();
    o.label = getLabel(entry);
    o.description = rdfutils.getDescription(entry);
    return o;
  };

  /**
   * Check if the property is a mapping property or a relational property.
   * Mapping property -> false
   * Relational property -> true
   *
   * @param property fully expanded
   * @return {Boolean}
   */
  const canTerminologyMapToSelf = (property) => {
    /** @type Set */
    const mappingProperties = skosUtil.getMappingProperties();
    const expandedMappingProperties = mappingProperties.map(ns.expand);

    return !expandedMappingProperties.includes(property);
  };

  /**
   * Commit the selected choice and close the dialog.
   *
   * Used by both tree and list views.
   */
  const HandleChoiceSelection = declare([], {
    handle(params) {
      const { entry, onSelect, dialog } = params;
      const choice = getChoice(entry);
      onSelect(choice);
      dialog.hide();
    },
  });

  /**
   * On list click call HandleChoiceSelection
   */
  const ConceptRowAction = declare([HandleChoiceSelection], {
    open(params) {
      const { entry, list } = params.row;
      const bundleParams = {
        entry,
        onSelect: list.onSelect,
        dialog: list.skoschooserDialog,
      };

      this.handle(bundleParams);
    },
  });

  const SkosChooserRow = declare([ConceptRow], {
    getRenderName() {
      let titleExtra = '';
      let name = rdfutils.getLabel(this.entry);
      const choice = getChoice(this.entry);
      if (choice.value === this.list.binding.getValue()) {
        titleExtra = ` (${this.nlsSpecificBundle.currentValueMark})`;
        if (name !== null) {
          name += titleExtra;
        }
      }
      return name;
    },
  });

  const SkosChooserTree = declare([Tree], {
    constructor(params) {
      this.srcNodeRef = params.srcNodeRef;
      this.jsTreeConf = params.jsTreeConf;
    },
    bindEvents(onSelect, dialog) {
      jquery(this.domNode).on('select_node.jstree', lang.hitch(this, (ev, obj) => {

        this.getTreeModel().getEntry(obj.node).then((entry) => {
          // commit choice
          HandleChoiceSelection().handle({ entry, onSelect, dialog });

          // unbind events from tree
          this.unBindEvents();
        });
      }));

      jquery(this.domNode).on('click', lang.hitch(this, (ev) => {
        ev.stopPropagation();
        ev.preventDefault();
      }));

      jquery(this.domNode).on('show_icons.jstree', lang.hitch(this, (ev, obj) => {
        console.log(ev);
        console.log(obj);
      }));
    },
    unBindEvents() {
      jquery(this.domNode).off('select_node.jstree');
    },
  });

  const SkosChooserList = declare([BaseList], {
    nlsBundles: ['escoList', 'escoSkosChooser'],
    nlsCreateEntryMessage: null,
    includeRefreshButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeResultSize: false,
    includeCreateButton: false,
    rowClickDialog: 'conceptRowAction',
    rowClass: SkosChooserRow,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('conceptRowAction', ConceptRowAction);
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
    },

    getIconClass() {
      return 'sitemap';
    },

    getSearchObject() {
      let searchObj = defaults.get('entrystore').newSolrQuery();
      const constraints = this.binding.getItem().getConstraints();
      const skosInScheme = defaults.get('namespaces').expand('skos:inScheme');
      if (constraints != null && constraints[rdfType] != null) {
        searchObj = searchObj.rdfType(constraints[rdfType]);
      }
      // check for selectedSkosConceptSchemaURI
      if (this.selectedSkosConceptSchemaURI === 'all') {
        searchObj = searchObj.uriProperty(skosInScheme, this.inSchemeResourceURIs);
        if (!canTerminologyMapToSelf(this.itemProperty)) {
          searchObj = searchObj.objectUri(this.currentContextRURI, true);
        }
      } else {
        searchObj = searchObj.uriProperty(skosInScheme, this.selectedSkosConceptSchemaURI);

        // remove current entry from search results
        // TODO this is potentially dangerous if the same entry exists in other terminlogies
        const en = defaults.get('entry');
        if (en) {
          searchObj = searchObj.uri(en.getURI(), true);
        }
      }

      const searchText = domAttr.get(this.listView.searchTermNode, 'value');
      if (searchText != null && searchText.length > 0) {
        searchObj = searchObj.title(searchText);
      }
      return searchObj.limit(10);
    },
    getTemplate() {
      // check replace with appropriate template
      return defaults.get('itemstore').getItem('skosmos:concept');
    },
  });

  const SkosChooser = declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: template,
    binding: null,
    onSelect: null,
    maxWidth: 800,
    nlsBundles: ['escoRdforms', 'escoSkosChooser'],
    includeFooter: false,

    postCreate() {
      this.inherited(arguments);

      this.currentContextRURI = defaults.get('context').getResourceURI();

      // init list view
      this.skosChooserList =
        new SkosChooserList({
          skoschooserDialog: this.dialog,
          currentContextRURI: this.currentContextRURI,
        }, this.__listView);

      // init tree view
      this.skosChooserTree = new SkosChooserTree({
        srcNodeRef: this.__treeView,
        jsTreeConf: {
          core: {
            multiple: false,
            themes: {
              name: 'proton',
              icons: false,
            },
          },
        },
      });

      // default view selection
      this.viewNodes = [this.__listViewer, this.__treeViewer];
      this.selectedView = 'list';
      domStyle.set(this.__listViewer, 'pointer-events', 'none');
    },
    getInSchemeEntries(skosinSchemeRURIs) {
      const inSchemeEntries = skosinSchemeRURIs.map(
        skosinSchemeRURI => esu.getEntryByResourceURI(skosinSchemeRURI));
      return all(inSchemeEntries);
    },
    getSchemeResourceURIs() {
      // get constraint and property and check in config and get skosconceptURI
      // or display all available concepts and terminologies
      let skosinSchemeRURIs = [];
      const skosInScheme = ns.expand('skos:inScheme');
      if (this.itemConstraints != null && this.itemConstraints[skosInScheme] != null) {
        const sisResourceURIs = this.itemConstraints[skosInScheme];
        skosinSchemeRURIs = lang.isArray(sisResourceURIs) ? sisResourceURIs : [sisResourceURIs];
        // render dropdown
      } else if (this.itemConstraints != null && this.itemConstraints[rdfType] != null) {
        // read property
        // get skosconfig from conf
        if (config && config.skosmapping) {
          const skosmapping = config.skosmapping;
          skosmapping.forEach((skosConf) => {
            if (skosConf.property === this.itemProperty) {
              const sisResourceURIs = skosConf.skosinSchemeRURI;
              skosinSchemeRURIs = lang.isArray(sisResourceURIs)
                ? sisResourceURIs : [sisResourceURIs];
            }
          });
        }
      }
      return skosinSchemeRURIs;
    },
    populateConceptSchemes() {
      this.itemConstraints = this.binding.getItem().getConstraints();
      this.itemProperty = this.binding.getItem().getProperty();
      const inskosinSchemeRURIs = this.getSchemeResourceURIs();

      this.skosChooserList.itemProperty = this.itemProperty; // pass this to the list as well

      if (this.selectedSkosConceptSchemaURI) {
        return this.getInSchemeEntries(inskosinSchemeRURIs)
          .then(terminologies => all(terminologies.forEach(
            terminology => this.addSkosConceptSchemes(terminology))));
      }

      this.inSchemeResourceURIs = [];
      this.optionEle = undefined;
      domConstruct.empty(this.__skosConceptSchemaList);
      if (inskosinSchemeRURIs.length > 0) {
        if (inskosinSchemeRURIs.length === 1) {
          this.selectedSkosConceptSchemaURI = inskosinSchemeRURIs;// setting default value to render
          domStyle.set(this.__skosConceptSchemaList, 'display', 'none');
          return all();
        }
        // add terminologies and display all
        this.skosConceptSchemas = [];
        domStyle.set(this.__skosConceptSchemaList, 'display', 'block');
        this.optionEle = domConstruct.create('option', { value: 'all' }, this.__skosConceptSchemaList);
        this.selectedSkosConceptSchemaURI = null; // setting default value to render
        return this.getInSchemeEntries(inskosinSchemeRURIs)
          .then(terminologies => all(terminologies.forEach((terminology) => {
            // if this is a non-mapping relation OR terminology in the loop is not same
            // as the context teminology then add to the terminology list
            if (canTerminologyMapToSelf(this.itemProperty)) {
              this.addSkosConceptSchemes(terminology);
            } else if (terminology.getContext().getResourceURI() !== this.currentContextRURI) {
              this.addSkosConceptSchemes(terminology);
            }
          })))
          .then(lang.hitch(this, () => {
            if (this.selectedSkosConceptSchemaURI == null) {
              this.selectedSkosConceptSchemaURI = 'all';
            }
          }));
      } // default
      this.skosConceptSchemas = [];
      domStyle.set(this.__skosConceptSchemaList, 'display', 'block');
      this.optionEle = domConstruct.create('option', { value: 'all' }, this.__skosConceptSchemaList);
      this.selectedSkosConceptSchemaURI = null;
      return defaults.get('entrystore').newSolrQuery().rdfType('skos:ConceptScheme')
        .list()
        .forEach((terminology) => {
          // if this is a non-mapping relation OR terminology in the loop is not same
          // as the context teminology then add to the terminology list
          if (canTerminologyMapToSelf(this.itemProperty)) {
            this.addSkosConceptSchemes(terminology);
          } else if (terminology.getContext().getResourceURI() !== this.currentContextRURI) {
            this.addSkosConceptSchemes(terminology);
          }
        })
        .then(() => {
          if (this.selectedSkosConceptSchemaURI == null) {
            this.selectedSkosConceptSchemaURI = 'all';
          }
        });
    },
    addSkosConceptSchemes(terminology) {
      const name = rdfutils.getLabel(terminology);
      const option = domConstruct.create('option', {
        innerHTML: name,
        value: terminology.getResourceURI(),
      }, this.__skosConceptSchemaList);
      if (terminology.getContext() === defaults.get('context') && this.selectedSkosConceptSchemaURI == null) {
        this.selectedSkosConceptSchemaURI = terminology.getResourceURI();
        domProp.set(option, 'selected', true);
      }
      this.skosConceptSchemas.push({ skosConceptScheme: terminology, elementItem: option });
      this.inSchemeResourceURIs.push(terminology.getResourceURI());
    },
    localeChange() {
      if (this.binding != null) {
        domAttr.set(this.dialog.titleNode, 'innerHTML', localize(this.NLSBundles.escoSkosChooser, 'searchForHeader'));
      }
      if (this.optionEle != null) {
        domAttr.set(this.optionEle, 'innerHTML', this.NLSBundles.escoSkosChooser.optionAll);
      }
    },
    selectSkosChooser(e) {
      const target = e.target || e.srcElement;
      this.selectedSkosConceptSchemaURI = target.value;
      this.skosChooserList.selectedSkosConceptSchemaURI = target.value;
      this.renderView();
    },
    renderView() {
      switch (this.selectedView) {
        case 'tree':
          // TODO this mash of ids and domnodes happens because Tree does not honor the predefined
          // id in the template and the list does
          domStyle.set(this.skosChooserList.id, 'display', 'none');
          domStyle.set(this.__treeView, 'display', 'block');

          esu.getEntryByResourceURI(this.selectedSkosConceptSchemaURI)
            .then((entry) => {
              this.skosChooserTree.showEntry(entry);
              this.skosChooserTree.bindEvents(this.onSelect, this.dialog);
            });
          break;
        case 'list':
        default:
          domStyle.set(this.__treeView, 'display', 'none');
          domStyle.set(this.skosChooserList.id, 'display', 'block');

          this.skosChooserList.render();
      }
    },
    populateListView(binding, onSelect) {
      this.binding = binding;
      this.onSelect = onSelect;
      this.skosChooserList.binding = binding;
      this.skosChooserList.onSelect = onSelect;
      return this.populateConceptSchemes().then(lang.hitch(this, () => {
        this.skosChooserList.inSchemeResourceURIs = this.inSchemeResourceURIs;
        this.skosChooserList.selectedSkosConceptSchemaURI = this.selectedSkosConceptSchemaURI;
      }));
    },
    show(binding, onSelect) {
      this.populateListView(binding, onSelect).then(() => this.skosChooserList.render());
      this.dialog.show();
    },
    hide() {
      this.dialog.hide();
    },
    _displaySelectTerminologyMsg() {
      // show some message to select a terminology first and exit
      cssUtil.togglePropertyValue(
        [this.__terminologyFirstAlert], cssUtil.toggleDisplayNoneParams);
      setTimeout(() => {
        jquery(this.__terminologyFirstAlert).fadeOut('slow');
      }, 2000);
    },
    selectSkosChooserView(e) {
      e.stopPropagation();
      e.preventDefault();
      const view = e.currentTarget.dataset.view || '';

      // set view
      switch (view) {
        case 'tree':
          if (this.selectedSkosConceptSchemaURI === 'all') {
            this._displaySelectTerminologyMsg();
            return false;
          }
          this.selectedView = 'tree';
          break;
        case 'list':
        default:
          this.selectedView = 'list';
          break;
      }

      // render tree or list viewer
      this.renderView();

      // Manage CSS
      cssUtil.toggleClass(this.viewNodes, 'active'); // NOTE! this only works for 2 views
      cssUtil.togglePropertyValue(this.viewNodes, {
        property: 'pointer-events',
        value1: 'none',
        value2: 'auto',
      });

      return true;
    },
  });

  let asyncCounter = 0;
  let defaultRegistered = false;
  const ext = {
    getChoice(item, value) {
      const obj = {
        value,
        load(onSuccess) {
          const store = defaults.get('entrystore');
          const async = defaults.get('asynchandler');
          const onError = () => {
            obj.label = `Could not load concept: ${value}`;
            obj.mismatch = true; // TODO replace with something else
            onSuccess();
          };
          if (value.indexOf(store.getBaseURI()) === 0) {
            const euri = store.getEntryURI(store.getContextId(value), store.getEntryId(value));
            const ac = `ignoreSC${asyncCounter}`;
            asyncCounter += 1;
            async.addIgnore(ac, async.codes.GENERIC_PROBLEM, true);
            store.getEntry(euri, { asyncContext: ac }).then((entry) => {
              getChoice(entry, obj);
              delete obj.load;
              return obj;
            }).then(onSuccess, onError);
          } else {
            const storeUtil = defaults.get('entrystoreutil');
            async.addIgnore('search', async.codes.GENERIC_PROBLEM, true);
            storeUtil.getEntryByResourceURI(value).then((entry) => {
              getChoice(entry, obj);
              delete obj.load;
              return obj;
            }).then(onSuccess, onError);
          }
        },
      };
      return obj;
    },
    show(binding, onSelect) {
      const skosChooser = new SkosChooser();
      skosChooser.startup();
      skosChooser.show(binding, onSelect);
    },
    search(item, term) {
      const qo = defaults.get('entrystore').newSolrQuery().limit(10);
      const constraints = item.getConstraints();
      qo.rdfType('skos:Concept');

      // check for selectedSkosConceptSchemaURI
      const inS = constraints[ns.expand('skos:inScheme')];
      if (inS) {
        qo.uriProperty('skos:inScheme', inS);
      }
      if (!canTerminologyMapToSelf(item.getProperty())) {
        qo.context(defaults.get('context'), true); // don't search in the current context
      }
      const en = defaults.get('entry');
      if (en) {
        qo.uri(en.getURI(), true);
      }
      if (term != null && term.length > 0) {
        qo.title(term);
      }
      return qo.limit(10).list().getEntries().then((entries) => {
        return entries.map((e) => {
          return {
            value: e.getResourceURI(),
            label: getLabel(e),
            description: rdfutils.getDescriptionMap(e)
          };
        });
      });
    },
    registerDefaults() {
      if (!defaultRegistered) {
        renderingContext.chooserRegistry.constraint({
          'http://www.w3.org/1999/02/22-rdf-syntax-ns#type': 'http://www.w3.org/2004/02/skos/core#Concept',
          'http://www.w3.org/2004/02/skos/core#inScheme': '',
        }).register(ext);
        renderingContext.chooserRegistry.constraint({ 'http://www.w3.org/2004/02/skos/core#inScheme': '' }).register(ext);
        defaultRegistered = true;
      }
    },
    chooser: SkosChooser,
  };

  return ext;
});
