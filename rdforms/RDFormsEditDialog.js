define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/Deferred',
  'rdfjson/Graph',
  'rdforms/model/engine',
  'rdforms/model/validate',
  'rdforms/view/renderingContext',
  'rdforms/view/bootstrap/LevelEditor',
  'rdforms/view/Editor',
  'di18n/NLSMixin',
  'entryscape-commons/dialog/TitleDialog',
  '../defaults',
  './PresentExpandable',
  'i18n!nls/escoRdforms',
], (declare, lang, array, domConstruct, domAttr, domStyle, Deferred, Graph, engine,
    validate, renderingContext, LevelEditor, Editor, NLSMixin, TitleDialog,
    defaults, PresentExpandable) =>

  declare([TitleDialog, NLSMixin.Dijit], {
    nlsHeaderTitle: 'metadataEditDialogHeader',
    nlsFooterButtonLabel: 'metadataEditDialogDoneLabel',
    nlsBundles: ['escoRdforms'],
    discardWarning: null,
    discardOption: null,
    keepOption: null,
    tooFewFields: null,
    tooManyFields: null,
    tooFewOrTooManyFields: null,
    title: null,
    doneLabel: null,
    doneTitle: null,
    explicitNLS: false,
    localizationParams: {},

    postCreate() {
      this.levels = new LevelEditor({ externalEditor: true },
        domConstruct.create('div', null, this.headerExtensionNode));
      this.editor = new Editor({}, domConstruct.create('div', null, this.containerNode));
      this.externalMetadata = new PresentExpandable({}, domConstruct.create('div', null, this.containerNode, 'first'));
      domStyle.set(this.externalMetadata, 'display', 'none');
      this.levels.setExternalEditor(this.editor);
      this.inherited(arguments);
    },

    localeChange() {
      const bundle = this.NLSBundle0;
      renderingContext.setMessages(bundle);
      this.levels.localize(bundle);
      this.discardWarning = bundle.discardMetadataChangesWarning;
      this.discardOption = bundle.discardMetadataChanges;
      this.keepOption = bundle.keepMetadataChanges;
      this.saveChanges = bundle.saveChanges;
      this.tooFewFields = bundle.tooFewFields;
      this.tooManyFields = bundle.tooManyFields;
      this.tooFewOrTooManyFields = bundle.tooFewOrTooManyFields;
      domAttr.set(this.defaultMessage, 'innerHTML',
        `<span class="mandatoryMark">*</span>${bundle.mandatoryMarkExplanation}`);
      this.updateTitleAndButton();
    },
    updateTitleAndButton() {
      if (this.explicitNLS && (this.title !== '' || this.title === null)) {
        this.updateLocaleStringsExplicit(this.title, this.doneLabel, this.doneTitle);
      } else {
        this.updateLocaleStrings(this.NLSBundle0, this.localizationParams);
      }
      this.updateHeaderWidth();
    },

    show(uri, graph, template, level) {
      this.updateEditor(uri, graph, template, level);
      this.inherited(arguments);
    },

    showEntry(entry, template, level) {
      const uri = entry.getResourceURI();
      this.updateExternalMetadata(entry, template);
      this.show(uri, entry.getMetadata(), template, level);
    },

    updateEntry(entry, template, level) {
      const uri = entry.getResourceURI();
      this.updateEditor(uri, entry.getMetadata(), template, level);
      this.updateExternalMetadata(entry, template);
    },

    updateExternalMetadata(entry, template) {
      const graph = entry.getCachedExternalMetadata();
      if ((entry.isLinkReference() || entry.isReference()) && !graph.isEmpty()) {
        domStyle.set(this.externalMetadata, 'display', 'block');
        this.externalMetadata.show(entry.getResourceURI(), graph, template);
      } else {
        domStyle.set(this.externalMetadata, 'display', 'none');
      }
    },

    updateEditor(uri, graph, template, level) {
      this.uri = uri;
      this.graph = new Graph(graph.exportRDFJSON());
      this.template = template;
      const profile = engine.levelProfile(template);
      const detectLevel = engine.detectLevel(profile);
      let givenLevel = level;
      if (profile.itemCount <= 5 || detectLevel.indexOf('mixed') === -1) {
        domStyle.set(this.levels.domNode, 'display', 'none');
        givenLevel = 'optional'; // Show all since there are markings to distinguish them
      } else {
        domStyle.set(this.levels.domNode, 'display', '');
        if (givenLevel == null) {
          if (profile.mandatory === 0) {
            givenLevel = 'recommended';
          } else {
            givenLevel = 'mandatory';
          }
        }
      }
      this.editor.graph = null; // Just to avoid re-rendering old form when changing includelevel.
      this.levels.setIncludeLevel(givenLevel || 'mandatory');
      this.levels.show(uri, this.graph, template);
      this.lockFooterButton();
      this.graph.onChange = () => {
        this.unlockFooterButton();
      };
    },
    conditionalHide() {
      if (!this.graph.isChanged()) {
        this.hide();
        return;
      }
      const dialogs = defaults.get('dialogs');
      /*
       dialogs.confirm(this.discardWarning, this.discardOption, this.keepOption,
       lang.hitch(this, function (discard) {
       if (discard) {
       this.hide();
       }
       }))
       */
      const dialogOptions = [{ name: 'keepEditing', buttonLabel: this.keepOption }, {
        name: 'save',
        buttonLabel: this.saveChanges,
        primary: true,
      }, { name: 'discard', buttonLabel: this.discardOption }];
      dialogs.options(this.discardWarning, dialogOptions).then(lang.hitch(this, (option) => {
        switch (option) {
          case 'keepEditing':
            break;
          case 'save':
            this.footerButtonClick();
            break;
          case 'discard':
            this.hide();
            break;
          default:
        }
      }));
    },

    /**
     * Provided so subclasses can overrride and discard certain errors, e.g. in a create situation.
     * @returns {*}
     */
    getReport() {
      return validate.bindingReport(this.editor.binding);
    },

    isFormOk() {
      const report = this.getReport();
      if (report.errors.length > 0) {
        let hasFew = false;
        let hasMany = false;
        array.forEach(report.errors, (err) => {
          switch (err.code) {
            case engine.CODES.TOO_FEW_VALUES:
              hasFew = true;
              break;
            case engine.CODES.TOO_MANY_VALUES:
            case engine.CODES.TOO_MANY_VALUES_DISJOINT:
              hasMany = true;
              break;
            default:
          }
        });
        this.editor.report(report);
        if (hasFew && !hasMany) {
          return this.tooFewFields;
        } else if (hasMany && !hasFew) {
          return this.tooManyFields;
        }
        return this.tooFewOrTooManyFields;
      }
      return true;
    },

    doneAction() {
      // Override
    },

    footerButtonAction() {
      const formStatus = this.isFormOk();
      if (formStatus !== true) {
        const d = new Deferred();
        d.reject(formStatus);
        return d;
      }
      return this.doneAction(this.graph);
    },
  }));
