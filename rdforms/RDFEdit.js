/* global define*/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-class',
  'dojo/on',
  'dojo/json',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'rdfjson/Graph',
  'rdfjson/formats/converters',
  'dojo/text!./RDFEditTemplate.html',
], (declare, lang, domClass, on, json, _WidgetBase, _TemplatedMixin, Graph, converters,
    template) =>
  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    graph: '',
    subView: 'rdf/xml',

    /**
     * @param {rdfjson.Graph|String|Object} rdf supports rdf/xml as a string or
     * rdf/json as a Graph, a string or a Object.
     * @return {undefined|Object} if undefined everything went well,
     * if a object something went wrong and the report was returned.
     */
    setRDF(rdf) {
      const report = converters.detect(rdf);
      if (!report.error) {
        this.switchState(report.format, report.graph);
      }
      return report;
    },

    switchState(format, graph) {
      if (this.switchRDF(format, graph)) {
        this.switchTab(format);
      }
    },

    switchTab(syntax) {
      if (syntax === 'rdf/json') {
        domClass.remove(this.rdfxmlTab, 'active');
        domClass.add(this.rdfjsonTab, 'active');
        domClass.remove(this.rdfxmlTabContent, 'active');
        domClass.add(this.rdfjsonTabContent, 'active');
      } else {
        domClass.remove(this.rdfjsonTab, 'active');
        domClass.add(this.rdfxmlTab, 'active');
        domClass.remove(this.rdfjsonTabContent, 'active');
        domClass.add(this.rdfxmlTabContent, 'active');
      }
    },

    rdfjsonClicked() {
      this.switchState('rdf/json');
    },
    rdfxmlClicked() {
      this.switchState('rdf/xml');
    },

    switchRDF(syntax, graph) {
      let _graph = graph;
      try {
        _graph = _graph || this.getGraph();
        this.subView = syntax;
        if (_graph) {
          this.setGraph(_graph);
          return true;
        }
      } catch (err) {
        // TODO better way to handle this?
        return false;
      }
      return false;
    },

    /**
     * @return {Object} a report as an object with a graph and if
     * something has gone wrong an error message.
     */
    getRDF() {
      try {
        const graph = this.getGraph();
        if (graph.validate().valid) {
          return { graph, format: this.subView };
        }
        return { error: 'RDF/JSON is not valid.' };
      } catch (e) {
        switch (this.subView) {
          case 'rdf/xml':
            return { error: 'RDF/XML is invalid' };
          case 'rdf/json':
            return { error: 'RDF/JSON is invalid' };
          default:
            return { error: 'Unsupported format' };
        }
      }
    },

    /**
     * @returns {rdfjson.Graph}
     */
    getGraph() {
      switch (this.subView) {
        case 'rdf/xml':
          return this.getRDFXML() || this.origGraph;
        case 'rdf/json':
          return this.getRDFJSON() || this.origGraph;
        default:
          return null;
      }
    },
    getCurrentSubView() {
      return this.subView;
    },
    /**
     * @param {rdfjson.Graph} graph
     */
    setGraph(graph) {
      this.origGraph = graph;
      switch (this.subView) {
        case 'rdf/xml':
          this.setRDFXML(graph);
          break;
        case 'rdf/json':
          this.setRDFJSON(graph);
          break;
        default:
          break;
      }
    },

    /**
     * Called everytime the RDF is edited (after a delay of 400 milliseconds).
     */
    onRDFChange() {
    },

    //= ==================================================
    // Private methods
    //= ==================================================
    postCreate() {
      this.inherited(arguments);
      let timer;
      const onRDFChange = lang.hitch(this, this.onRDFChange);
      const onRDFChangeFunc = function () {
        clearTimeout(timer);
        timer = setTimeout(onRDFChange, 400);
      };
      on(this._rdfjson, 'keyup', onRDFChangeFunc);
      on(this._rdfxml, 'keyup', onRDFChangeFunc);
    },
    getRDFXML() {
      if (this.rdfxmlValue.length <= 100000) {
        return converters.rdfxml2graph(this.getValueFromNode(this._rdfxml));
      }
      return this.origGraph || new Graph();
    },
    setRDFXML(graph) {
      this.rdfxmlValue = converters.rdfjson2rdfxml(graph);
      if (this.rdfxmlValue.length > 100000) {
        this._rdfxml.setAttribute('readonly', true);
        this.setValueToNode(this._rdfxml, `${this.rdfxmlValue.substring(0, 100000)}\n    ----- \n RDF to large, truncating it. \n   ------`);
      } else {
        this._rdfxml.removeAttribute('readonly');
        this.setValueToNode(this._rdfxml, this.rdfxmlValue);
      }
    },
    setValueToNode(node, value) {
      node.value = value;
    },
    getValueFromNode(node) {
      return node.value;
    },
    getRDFJSON() {
      if (this.rdfjsonValue.length <= 100000) {
        const rdfStr = this.getValueFromNode(this._rdfjson);
        return new Graph(json.parse(rdfStr == null || rdfStr === '' ? '{}' : rdfStr));
      }
      return this.origGraph || new Graph();
    },
    setRDFJSON(graph) {
      this.rdfjsonValue = json.stringify(graph.exportRDFJSON(), 0, 2);
      if (this.rdfjsonValue.length > 100000) {
        this._rdfjson.setAttribute('readonly', true);
        this.setValueToNode(this._rdfjson, `${this.rdfjsonValue.substring(0, 100000)}\n    ----- \n RDF to large, truncating it. \n   ------`);
      } else {
        this._rdfjson.removeAttribute('readonly');
        this.setValueToNode(this._rdfjson, this.rdfjsonValue);
      }
    },
  }));
