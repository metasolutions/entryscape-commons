/* global define*/
define([
  'mithril',
  './Position',
  './Map',
  'entryscape-commons/defaults',
  'rdforms/model/engine',
], (m, Position, Map, defaults, engine) => {

  return vnode => {
    const {binding, editable, bundle} = vnode.attrs;

    const updateGeoCoordinates = coords => {
      binding.setValue(coords);
      m.redraw();
      return true;
    };

    const state = {
      inputsFocused: false
    };

    const unfocusInputs = () => state.inputsFocused = false;
    const focusInputs = () => state.inputsFocused = true;

    const component = {
      view(vnode) {
        const { value, editable } = vnode.attrs;

        return m('div', null, [
          m(Position, { editable, value: binding.getValue(), bundle, updateGeoCoordinates, focusInputs, inputsFocused: state.inputsFocused }),
          m(Map, { editable, value: binding.getValue(), updateGeoCoordinates, unfocusInputs }),
        ]);
      },
    };

    return component;
  };
});
