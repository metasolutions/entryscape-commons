/* global define*/
define([
  'mithril',
  './components/GeoCoordinates',
  'rdforms/view/renderingContext',
  'i18n',
  'entryscape-commons/defaults',
  'i18n!nls/escoRdforms',
], (m, GeoCoordinates, renderingContext, i18n, defaults) => {
  let defaultRegistered = false;

  const ext = {
    editor(node, binding, context) {
      i18n.getLocalization('nls', 'escoRdforms', defaults.get('locale'), (bundle) => {
        m.mount(node, { view: () => m(GeoCoordinates, {binding, editable: true, bundle}) } );
      });
    },
    presenter(node, binding, context) {
      i18n.getLocalization('nls', 'escoRdforms', defaults.get('locale'), (bundle) => {
        m.mount(node, { view: () => m(GeoCoordinates, {binding, editable: false, bundle}) } );
      });
    },
    registerDefaults() {
      if (!defaultRegistered) {
        defaultRegistered = true;

        renderingContext.editorRegistry
          .datatype('http://www.opengis.net/ont/geosparql#wktLiteral')
          .register(ext.editor);
        renderingContext.presenterRegistry
          .datatype('http://www.opengis.net/ont/geosparql#wktLiteral')
          .register(ext.presenter);
      }
    },
  };
  return ext;
});
