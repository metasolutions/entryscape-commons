/* global define*/
define([
  'mithril',
  './components/Spatial',
  'rdforms/view/renderingContext',
  'i18n',
  'entryscape-commons/defaults',
  'i18n!nls/escoRdforms',
], (m, Spatial, renderingContext, i18n, defaults) => {
  let defaultRegistered = false;

  const ext = {
    editor(node, binding, context) {
      i18n.getLocalization('nls', 'escoRdforms', defaults.get('locale'), (bundle) => {
        m.mount(node, { view: () => m(Spatial, {binding, editable: true, bundle, context }) });
      });
    },
    presenter(node, binding, context) {
      i18n.getLocalization('nls', 'escoRdforms', defaults.get('locale'), (bundle) => {
        m.mount(node, { view: () => m(Spatial, {binding, editable: false, bundle, context }) });
      });
    },
    registerDefaults() {
      if (!defaultRegistered) {
        defaultRegistered = true;
        renderingContext.editorRegistry
          .item({ getId() { return 'dcatde:spatial'; } })
          .register(ext.editor);
        renderingContext.presenterRegistry
          .item({ getId() { return 'dcatde:spatial'; } })
          .register(ext.presenter);
      }
    },
  };
  return ext;
});
