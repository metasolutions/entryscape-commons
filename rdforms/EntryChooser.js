/* global define*/
define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/event',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  'dijit/focus',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'di18n/localize',
  '../defaults', // Provides namespaces, context, entrystore and rdfutils in registry.
  '../dialog/TitleDialog', // In template
  'rdforms/view/renderingContext',
  'dojo/text!./EntryChooserTemplate.html',
  'entryscape-commons/create/EntryType',
  'entryscape-commons/create/typeIndex',
  'rdforms/view/bootstrap/LevelEditor',
  'rdforms/view/Editor',
  'rdforms/model/validate',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/list/EntryRow',
  'i18n!nls/escoRdforms',
  'i18n!nls/escoEntryChooser',
  'i18n!nls/escoList',
], (declare, lang, array, event, on, domConstruct, domAttr, domClass, domStyle, focusUtil,
    _WidgetsInTemplateMixin, NLSMixin, localize, defaults, TitleDialog, renderingContext,
    template, EntryType, typeIndex, LevelEditor, Editor, validate, BaseList, EntryRow) => {
  const rdfType = defaults.get('namespaces').expand('rdf:type');
  const chooserScope = {};

  const getChoice = (entry, obj) => {
    const o = obj || {};
    const rdfutils = defaults.get('rdfutils');
    o.value = entry.getResourceURI();
    o.label = rdfutils.getLabel(entry);
    /* utils.getLocalizedMap(entry.getMetadata(), entry.getResourceURI(), [
     "http://xmlns.com/foaf/0.1/name",
     "http://purl.org/dc/terms/title",
     "http://purl.org/dc/elements/1.1/title",
     "http://www.w3.org/2006/vcard/ns#fn"
     ]);*/
    o.description = rdfutils.getDescription(entry);
    /* utils.getLocalizedMap(entry.getMetadata(), entry.getResourceURI(), [
     "http://purl.org/dc/terms/description"
     ]);*/
    return o;
  };

  const restrictToContext = (item, qo) => {
    const prop = item.getProperty();
    const restr = typeof chooserScope[prop] === 'undefined' || chooserScope[prop] === 'context';
    const conf = typeIndex.getConfFromConstraints(item.getConstraints());

    if (conf && conf.context) {
      qo.context(defaults.get('entrystore').getContextById(conf.context));
      // eslint-disable-next-line
    } else if ((conf && conf.allContexts != true) || restr) {
      qo.context(defaults.get('context'));
    }
  };

  const EntryChooserRowAction = declare([], {
    open(params) {
      const choice = getChoice(params.row.entry);
      params.row.list.onSelect(choice);
      params.row.list.entrychooserDialog.hide();
    },
  });

  const EntryChooserRow = declare([EntryRow], {
    postCreate() {
      this.inherited('postCreate', arguments);
    },
    getRenderName() {
      const rdfutils = defaults.get('rdfutils');
      let titleExtra = '';
      let name = rdfutils.getLabel(this.entry);
      const choice = getChoice(this.entry);
      if (choice.value === this.list.binding.getValue()) {
        titleExtra = ` (${this.nlsSpecificBundle.currentValueMark})`;
        if (name !== null) {
          name += titleExtra;
        }
      }
      return name;
    },
  });

  const EntryChooserList = declare([BaseList], {
    nlsBundles: ['escoList', 'escoEntryChooser'],
    nlsCreateEntryMessage: null,
    includeRefreshButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeCreateButton: false,
    includeVersionsButton: false,
    rowClickDialog: 'entryChooserRowAction',
    rowClass: EntryChooserRow,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('entryChooserRowAction', EntryChooserRowAction);
      this.listView.includeResultSize = !!this.includeResultSize; // make this boolean
    },
    getIconClass() {
      if (this.conf && this.conf.faclass) {
        return this.conf.faClass;
      }

      return '';
    },
    getEmptyListWarning() {
      if (this.binding != null) {
        const createLabel = this.binding.getItem().getLabel();
        return localize(this.NLSBundle1, 'errorMessage', { 1: createLabel });
      }

      return '';
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem(this.conf.template);
      }
      return this.template;
    },

    search(params) {
      const _params = params || {};
      const term = _params.term != null && _params.term.length > 0 ? _params.term : '';
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      const qo = typeIndex.query(es.newSolrQuery(),
        { constraints: this.binding.getItem().getConstraints() }, term);
      restrictToContext(this.binding.getItem(), qo);

      if (_params.sortOrder === 'title') {
        const l = locale.get();
        qo.sort(`title.${l}+desc`);
      }

      this.listView.showEntryList(qo.list());
    },
  });

  const EntryChooser = declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: template,
    binding: null,
    onSelect: null,
    nlsBundles: ['escoRdforms', 'escoEntryChooser'],

    postCreate() {
      this.inherited(arguments);
      this.entryChooserList = new EntryChooserList({ entrychooserDialog: this.dialog }, domConstruct.create('div', null, this.searchNode));
      domConstruct.place(this.moveInput, this.dialog.headerExtensionNode);
      this.levels = new LevelEditor({ externalEditor: true },
        domConstruct.create('div', null, this.levelEditorNode));
      this.editor = new Editor({}, domConstruct.create('div', null, this.editorNode));
      this.levels.setExternalEditor(this.editor);
      this.fileOrLink = new EntryType({
        valueChange: lang.hitch(this, (value) => {
          if (value != null) {
            this.dialog.unlockFooterButton();
          } else {
            this.dialog.lockFooterButton();
          }
        }),
      }, domConstruct.create('div', null, this.entityTypeNode, 'first'));
    },
    localeChange() {
      if (this.binding != null) {
        const createLabel = this.binding.getItem().getLabel();
        this.dialog.updateLocaleStringsExplicit(
          localize(this.NLSBundle1, 'searchForHeader', { 1: createLabel }),
          localize(this.NLSBundle1, 'createEntryType', { 1: createLabel }));
      }
    },
    searchOption() {
      domClass.add(this.__searchOptionLabel, 'active');
      domClass.remove(this.__createOptionLabel, 'active');
      domStyle.set(this.searchNode, 'display', 'block');
      domStyle.set(this.createNode, 'display', 'none');
      domStyle.set(this.dialog.footerButtonNode, 'display', 'none');
    },
    createOption() {
      domClass.remove(this.__searchOptionLabel, 'active');
      domClass.add(this.__createOptionLabel, 'active');
      domStyle.set(this.searchNode, 'display', 'none');
      domStyle.set(this.createNode, 'display', 'block');
      domStyle.set(this.dialog.footerButtonNode, 'display', 'block');

      let context;
      if (this.conf && this.conf.context) {
        context = defaults.get('entrystore').getContextById(this.conf.context);
      }
      if (!context) {
        context = defaults.get('context');
      }
      this._newEntry = defaults.get('createEntry')(context);
      const nds = this._newEntry;
      const constraints = this.binding.getItem().getConstraints();
      this._graph = nds.getMetadata();
      const uri = nds.getResourceURI();

      const conf = typeIndex.getConfFromConstraints(constraints);
      const template_ = conf != null ? conf.template : null;

      if (constraints) {
        const subj = this._newEntry.getResourceURI();
        Object.keys(constraints).forEach((prop) => {
          const obj = constraints[prop];
          if (Array.isArray(obj)) {
            this._graph.add(subj, prop, obj[0]);
          } else {
            this._graph.add(subj, prop, obj);
          }
        });
      }

      if (template_) {
        this.editor.graph = null; // Just to avoid re-rendering old form when changing includelevel.
        this.levels.setIncludeLevel(conf.templateLevel || 'mandatory');
        this.editor.hideAddress = true;
        this.editor.show({
          resource: uri,
          graph: nds.getMetadata(),
          template: defaults.get('itemstore').getItem(template_),
        });
      } else {
        // domAttr.set(this.createRdf,"innerHTML", "template not defined");
      }
      this.localeChange();
    },
    show(binding, onSelect) {
      this.binding = binding;
      this.onSelect = onSelect;
      this.entryChooserList.binding = binding;
      this.entryChooserList.onSelect = onSelect;
      this.conf = typeIndex.getConfFromConstraints(this.binding.getItem().getConstraints());
      this.entryChooserList.conf = this.conf;
      if (this.conf && this.conf.inlineCreation) {
        // show  Creation tab
        domStyle.set(this.moveInput, 'display', 'block');
        this.fileOrLink.showConfig(this.conf);
      } else {
        // hide Creation tab
        domStyle.set(this.moveInput, 'display', 'none');
      }
      this.inherited(arguments);
      domStyle.set(this.dialog.footerButtonNode, 'display', 'none');
      this.entryChooserList.render();
      this.dialog.show();
    },
    footerButtonAction() {
      const report = validate.bindingReport(this.editor.binding);
      if (report.errors.length > 0) {
        this.editor.report(report);
        return this.NLSBundles.escoRdforms.missingMandatoryFields;
      }
      if (!this._graph.isChanged()) {
        return undefined;
      }

      return this.fileOrLink.newEntry(this._newEntry).then(lang.hitch(this, (entry) => {
        const choice = getChoice(entry);
        this.onSelect(choice);
      }));
    },
  });

  const async = defaults.get('asynchandler');
  let asyncCounter = 0;
  const ignoreCallType = (callType) => {
    const ct = callType || `ignoreEC${asyncCounter}`;
    asyncCounter += 1;
    async.addIgnore(ct, async.codes.GENERIC_PROBLEM, true);
    return ct;
  };


  let defaultRegistered = false;
  const ext = {
    getChoice(item, value) {
      const obj = {
        value,
        load(onSuccess) {
          const store = defaults.get('entrystore');
          const storeutil = defaults.get('entrystoreutil');
          const onError = () => {
            obj.upgrade = (binding, callback) => {
              const eChooser = new EntryChooser();
              eChooser.startup();
              eChooser.show(binding, callback);
              eChooser.fileOrLink.setLink(binding.getValue());
              eChooser.localeReady.then(() => {
                eChooser.createOption();
              });
            };
            obj.label = value;
            obj.mismatch = true; // TODO replace with something else
            onSuccess();
          };
          const entryToObj = (entry) => {
            getChoice(entry, obj);
            delete obj.load;
            return obj;
          };
          if (value.indexOf(store.getBaseURI()) === 0) {
            const euri = store.getEntryURI(store.getContextId(value), store.getEntryId(value));
            return store.getEntry(euri, { asyncContext: ignoreCallType() })
              .then(entryToObj).then(onSuccess, onError);
          } else if (item.hasStyle('internalLink')) {
            let ct;
            if (store.getCache().getByResourceURI(value).length === 0) {
              ct = ignoreCallType('search');
            }
            return storeutil.getEntryByResourceURI(value, null, ct).then(entryToObj).then(
              onSuccess, onError);
          }
          const storeUtil = defaults.get('entrystoreutil');
          async.addIgnore('search', async.codes.GENERIC_PROBLEM, true);
          return storeUtil.getEntryByResourceURI(value).then((entry) => {
            getChoice(entry, obj);
            delete obj.load;
            return obj;
          }).then(onSuccess, onError);
        },
      };
      return obj;
    },
    show(binding, onSelect) {
      const eChooser = new EntryChooser();
      eChooser.startup();
      eChooser.show(binding, onSelect);
    },
    supportsInlineCreate(binding) {
      const conf = typeIndex.getConfFromConstraints(binding.getItem().getConstraints());
      return conf && conf.inlineCreation;
    },
    search(item, term) {
      const es = defaults.get('entrystore');
      const qo = es.newSolrQuery();
      restrictToContext(item, qo);
      const rdfutils = defaults.get('rdfutils');
      return typeIndex.query(qo, {constraints: item.getConstraints()}, term)
        .limit(10).list().getEntries().then((entries) => {
          return entries.map((e) => {
            return {
              value: e.getResourceURI(),
              label: rdfutils.getLabelMap(e),
              description: rdfutils.getDescriptionMap(e)
            };
          });
        });
    },
    registerDefaults() {
      if (!defaultRegistered) {
        renderingContext.chooserRegistry.itemtype('choice').register(ext);
        defaultRegistered = true;
      }
    },
    chooser: EntryChooser,
    setChooserScope(scope) {
      Object.keys(scope).forEach((key) => {
        chooserScope[key] = scope[key];
      });
    },
  };

  const chooserScopeConfig = defaults.get('entrychooser');
  if (chooserScopeConfig) {
    ext.setChooserScope(chooserScopeConfig);
  }

  return ext;
});
