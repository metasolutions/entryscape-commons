define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'dojo/_base/array',
  'dojo/dom-construct',
  'dojo/dom-class',
  'entryscape-commons/list/EntryRow',
  './util',
], (declare, defaults, array, domConstruct, domClass, EntryRow, util) =>
  declare([EntryRow], {
    postCreate() {
      this.divPath = domConstruct.create('div', { class: 'escoConceptRow__path' }, this.col2Node, this.nameNode);
      $(this.domNode).addClass('termsList');
      domClass.add(this.nameNode, 'escoConceptRow__name');
      this.inherited('postCreate', arguments);
      this.renderPath();
    },
    renderPath() {
      const self = this;
      const path = [];
      const rdfutils = defaults.get('rdfutils');
      util.getConceptPath(path, this.entry).then((entries) => {
        const length = entries.length;
        entries.reverse();

        domConstruct.create('i', {
          class: 'fa fa-sitemap escoConceptRow__icon',
        }, self.divPath);

        array.forEach(entries, (entry, i) => {
          let spanEle;
          if (i === 0) {
            spanEle = domConstruct.create('span', {
              class: 'escoConceptRow__terminology',
              innerHTML: `${rdfutils.getLabel(entry)} `,
            }, self.divPath);
          } else {
            spanEle = domConstruct.create('span', { innerHTML: ` ${rdfutils.getLabel(entry)} ` }, self.divPath);
          }
          if (i === 0 && length > 1) {
            domConstruct.create('i', { class: 'fa fa-angle-double-right' }, spanEle);
          }
          if (i > 0 && i < length - 1) {
            domConstruct.create('i', { class: 'fa fa-angle-right' }, spanEle);
          }
        });
      });
    },
  }));
