define([
  'mithril',
  '../Group'
], (m, Group) => {

  /**
   * @see ./Fieldset.md
   */
  const Fieldset = {
    /**
     */
    view(vnode) {
      const { legend, components } = vnode.attrs;
      return m(Group, { element: 'fieldset', components: [m('legend', legend), ...components] });
    },
  };

  return Fieldset;
});
