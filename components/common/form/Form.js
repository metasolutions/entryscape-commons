define([
  'mithril',
], (m) => {
  /**
   * @type {{view: ((vnode))}}
   */
  const Form = {
    enabled: true,
    view(vnode) {
      const {
        id,
        oninput,
        onchange,
        onsubmit = 'return false;',
        classNames = [],
      } = vnode.attrs;

      return m('form', { id, onsubmit, oninput, onchange, class: classNames.join(' ') }, vnode.children);
    },
  };

  return Form;
});
