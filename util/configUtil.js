define([
  'config',
], (config) => {
  const getBaseUrl = () => {
    return config.baseAppPath ? `${window.location.origin}/${baseAppPath}` : window.location.origin;
  };
  const getThemeDefaults = () => {
    let defaults = {};
    try {
      defaults.appName = config.theme.default.appName;
      defaults.logo = config.theme.default.logo;
      defaults.themePath = config.theme.default.themePath;
    } catch (e) {
      throw Error('App theme default is not configured correctly!');
    }

    return defaults;
  };
  const getThemeToRender = () => {
    let { appName, themePath, logo } = getThemeDefaults();
    if (config.theme && config.theme.localTheme) {
      themePath = 'theme/';
    }

    if (config.theme && (config.theme.appName ||
        (config.theme.logo && config.theme.logo.text))) {
      appName = (config.theme.appName || config.theme.logo.text);
    }

    return { appName, themePath, logo };
  };

  const getLogoType = () => {
    if (config.theme.logo) {
      if (config.theme.logo.icon && config.theme.logo.full) { // icon + full logo
        return 'all';
      } else if (config.theme.logo.full) { // full logo only
        return 'full';
      }
    }

    return 'icon'
  };

  const getLogoInfo = (defaultType = null) => {
    const { appName, themePath, logo } = defaultType ? getThemeDefaults() : getThemeToRender();
    const type = defaultType || getLogoType();

    let logoInfo = { type };
    switch (type) {
      case 'all':
        logoInfo.src = {
          icon: require.toUrl(themePath + config.theme.logo.icon),
          full: require.toUrl(themePath + config.theme.logo.full),
        };
        break;
      case 'full':
        logoInfo.src = {
          full: require.toUrl(themePath + config.theme.logo.full)
        };
        break;
      default: // icon
        if (defaultType) { // mainly for footer
          logoInfo.src = { icon: logo }
          logoInfo.text = appName;
        } else {
          const hasCustomIcon = 'logo' in config.theme && 'icon' && config.theme.logo;
          logoInfo.src = {
            icon: hasCustomIcon ? require.toUrl(themePath + config.theme.logo.icon) : logo,
          };
          logoInfo.text = appName;
        }
    }
    ;

    return logoInfo;
  };

  const getAppName = (native = true) => {
    const { appName } = native ? getThemeDefaults() : getThemeToRender();
    return appName;
  };

  return {
    getBaseUrl,
    getThemeDefaults,
    getThemeToRender,
    getLogoInfo,
    getAppName,
  }
});
