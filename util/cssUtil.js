define([
  'dojo/dom-style',
  'dojo/dom-class',
], (domStyle, domClass) => {

  const toggledDisplayNoneBlock = {
    property: 'display',
    value1: 'none',
    value2: 'block',
  };

  return {
    toggleDisplayNoneParams: {
      property: 'display',
      value1: 'none',
      value2: '',
    },

    toggleClass(nodes, cssClass) {
      nodes.forEach(node => domClass.toggle(node, cssClass));
    },
    /**
     * E.g
     *
     * For a params = { property: 'display', value1: 'none', value2: '' }, all the nodes that have a
     * display:none will have that removed.
     *
     * @param nodes
     * @param params {Object}
     */
    togglePropertyValue(nodes, params) {
      const { property, value1, value2 } = params;

      nodes.forEach((node) => {
        const currentPropertyValue = domStyle.get(node, property);

        if (currentPropertyValue === value1) {
          domStyle.set(node, property, value2);
        } else if (currentPropertyValue === value2) {
          domStyle.set(node, property, value1);
        }
      });
    },
    toggleDisplayNoneBlock(nodes) {
      this.togglePropertyValue(nodes, toggledDisplayNoneBlock);
    }
  };
});
