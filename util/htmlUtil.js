define([], () => {
  return {
  toggleAttribute(nodes, attr, value) {
    nodes.forEach(n => n.hasAttribute(attr) ? n.removeAttribute(attr) : n.setAttribute(attr, value));
  },

  toggleEnabledDisabled(domNodes) {
    this.toggleAttribute(domNodes, 'disabled', 'disabled');
  },  
  }
  
});
