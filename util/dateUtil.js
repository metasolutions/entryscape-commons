define([
  'config',
  'di18n/locale',
  'dojo/date/locale',
], (config, il8nlocale, locale) => ({
  getMultipleDateFormats(date) {
    let period = 'older';
    const lang = il8nlocale.get();
    const shortDateFormat = this.getDateFormat(lang);
    const modDateMedium = locale.format(date, { selector: 'date', formatLength: 'medium' });
    let short = modDateMedium;
    const cd = new Date();
    const currentDateMedium = locale.format(cd, { selector: 'date', formatLength: 'medium' });
    if (currentDateMedium === modDateMedium) {
      period = 'today';
      short = locale.format(date, { selector: 'time', formatLength: 'short' });
    } else if (date.getYear() === cd.getYear()) {
      period = 'thisyear';
      short = locale.format(date, { selector: 'date', datePattern: shortDateFormat });
    }
    return {
      dateMedium: modDateMedium,
      timeMedium: locale.format(date, { selector: 'time', formatLength: 'medium' }),
      full: locale.format(date, { formatLength: 'full' }),
      short,
      period };
  },
  getDateFormat(localLang) {
    for (let i = 0; i < config.locale.supported.length; i++) {
      const supportedLang = config.locale.supported[i].lang;
      if (localLang === supportedLang) {
        return config.locale.supported[i].shortDatePattern;
      }
    }

    return '';
  },
}));
