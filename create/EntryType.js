define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/on',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'dojo/text!./EntryTypeTemplate.html',
  'bootstrap/button',
  'i18n!nls/escoEntryType',
], (declare, lang, domAttr, domStyle, domConstruct, domClass, on, _WidgetBase, _TemplatedMixin,
    NLSMixin, template) =>
  // noinspection JSUnusedGlobalSymbols
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    nlsBundles: ['escoEntryType'],
    bid: 'escoEntryType',
    templateString: template,
    _i18nState: 1,
    __fileBlock: null,
    __linkBlock: null,
    __linkInput: null,
    __filePathInput: null,
    __filePathInputWrapper: null,
    __fileInputWrapper: null,
    __fileInput: null,

    postCreate() {
      this.inherited(arguments);
      on(this.__filePathInputWrapper, 'click', lang.hitch(this, function () {
        this.__fileInput.click();
      }));

      let t = null;
      const f = lang.hitch(this, this.linkTyped);
      on(this.__linkInput, 'keyup', () => {
        if (t) {
          clearTimeout(t);
        }
        t = setTimeout(f, 400);
      });
    },

    showConfig(conf) {
      this.show(conf.includeFile, conf.includeLink, conf.includeInternal);
    },

    /**
     *
     * @param includeFile
     * @param includeLink
     * @param includeInternal
     */
    show(includeFile, includeLink, includeInternal) {
      // DefaultValues
      isIncludeFile = includeFile === true;
      isIncludeLink = includeLink === true;
      isIncludeInternal = includeInternal === true;

      if (!isIncludeFile && !isIncludeLink && !isIncludeInternal) {
        console.error('Cannot create entity that is nothing (not file, not link, not internal URI), ' +
          'something is wrong in the configuration');
      }

      this.newFileInputElement();

      domClass.remove(this.domNode, `${this.bid}--linkLabelVisible`);
      domClass.remove(this.domNode, `${this.bid}--fileLabelVisible`);
      domClass.remove(this.domNode, `${this.bid}--internalLink`);

      // No internal option allowed with link
      if (isIncludeLink && !isIncludeInternal) {
        this.setI18nState(2);
      } else {
        this.setI18nState(1);
      }

      if (!isIncludeFile) { // No Filoption
        this.linkOption();
        this.noOption();
        domClass.add(this.domNode, `${this.bid}--linkLabelVisible`);
      } else if (isIncludeFile && !isIncludeLink && !isIncludeInternal) { // No linkoption
        this.fileOption();
        this.noOption();
        domClass.add(this.domNode, `${this.bid}--fileLabelVisible`);
      } else {
        this.linkOption();
      }

      // Only internal option, no link
      if (!isIncludeLink && isIncludeInternal) {
        domClass.add(this.domNode, `${this.bid}--internalLink`);
      }

      this.updateErrorMessage(null);
      domAttr.set(this.__linkInput, 'value', '');
      domAttr.set(this.__filePathInput, 'value', '');
    },
    updateErrorMessage(message) {
      if (message === '' || message === null) {
        domStyle.set(this.__errorMessage, 'display', 'none');
      } else {
        domStyle.set(this.__errorMessage, 'display', '');
        domAttr.set(this.__errorMessage, 'innerHTML', message);
      }
    },

    linkTyped() {
      const value = domAttr.get(this.__linkInput, 'value');
      let inValid = false;
      if (lang.isFunction(this.__linkInput.checkValidity)) {
        inValid = !this.__linkInput.checkValidity();
      } else {
        const re = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/;
        inValid = value === '' ? false : value.match(re) === null; // Needs improvement
      }

      if (inValid) {
        this.updateErrorMessage(this.NLSBundle0.linkNotValid);
      } else {
        this.updateErrorMessage(null);
      }

      if (this._i18nState === 2 && value === '') { // Empty value is not allowed, only real URI
        this.setValue(null, false);
      } else if (value === '') {
        this.setValue('', false);
      } else if (inValid === true) {
        this.setValue(null, false);
      } else {
        this.setValue(value, false);
      }
    },
    fileSelected() {
      const label = domAttr.get(this.__fileInput, 'value').replace(/\\/g, '/').replace(/.*\//, '');
      domAttr.set(this.__filePathInput, 'value', label);
      this.setValue(label === '' ? null : label, true);
    },
    noOption() {
      domClass.remove(this.domNode, `${this.bid}--linkOption`);
      domClass.remove(this.domNode, `${this.bid}--fileOption`);
    },
    linkOption() {
      domClass.add(this.__linkOptionLabel, 'active');
      domClass.remove(this.__fileOptionLabel, 'active');
      domClass.add(this.domNode, `${this.bid}--linkOption`);
      domClass.remove(this.domNode, `${this.bid}--fileOption`);
      this.__isFile = false;
      this.linkTyped();
      this.optionChange(false);
    },
    fileOption() {
      domClass.add(this.__linkOptionLabel, 'active');
      domClass.remove(this.__fileOptionLabel, 'active');
      domClass.add(this.domNode, `${this.bid}--fileOption`);
      domClass.remove(this.domNode, `${this.bid}--linkOption`);
      this.__isFile = true;
      this.updateErrorMessage(null);
      this.fileSelected();
      this.optionChange(true);
    },
    optionChange() {
      // Override or listen on this.
    },
    getValue() {
      return this.__value;
    },
    setLink(uri) {
      domAttr.set(this.__linkInput, 'value', uri);
      this.setValue(uri, false);
    },
    setValue(value, isFile) {
      this.__isFile = isFile;
      this.__value = value;
      this.valueChange(value, isFile);
    },
    valueChange() {
      // Override or listen on this.
    },
    isFile() {
      return this.__isFile;
    },
    newFileInputElement() {
      if (this.__fileInput) {
        domConstruct.destroy(this.__fileInput);
        this.fileInputListener.remove();
      }
      this.__fileInput = domConstruct.create('input',
        { type: 'file', name: `${this.id}_fileDialog` }, this.__fileInputWrapper);
      this.fileInputListener = on(this.__fileInput, 'change', lang.hitch(this, this.fileSelected));
    },
    getFileInputElement() {
      return this.__fileInput;
    },
    newEntry(pe) {
      const context = pe.getContext();
      const graph = pe.getMetadata();
      if (this.isFile()) {
        const newpe = context.newEntry();
        // Just in case the inherited open method created a link with a default external URI.
        const uri1 = pe.getResourceURI();
        const uri2 = newpe.getResourceURI();
        if (uri1 !== uri2) {
          graph.replaceURI(uri1, uri2);
        }
        const inpEl = this.getFileInputElement();
        console.log("input element is null " + inpEl == null);
        return newpe.setMetadata(graph).commit()
          .then((entry) => {
            if (inpEl.value) { // check if file was given
              return entry.getResource(true).putFile(inpEl).then(() => entry);
            }
            return entry; // Just make sure we return the entry.
          });
      }
      const uri = this.getValue();
      if (uri === '' || uri === null) {
        return pe.commit();
      }
      graph.replaceURI(pe.getResourceURI(), uri);
      return context.newLink(uri).setMetadata(graph).commit();
    },
  }));
