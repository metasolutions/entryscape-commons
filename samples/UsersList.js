define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  '../defaults',
  '../rdforms/GeoChooser',
  '../list/common/BaseList',
], (declare, lang, defaults, GeoChooser, BaseList) => {
  GeoChooser.registerDefaults();

  const CDialog = declare(null, {
    constructor(params) {
      this.list = params.list;
    },
    open() {
      const es = defaults.get('entrystore');
            /** @type {store/PrototypeEntry}*/
      const pe = es.getContextById('1').newNamedEntry();
      const md = pe.getMetadata();
      const ns = defaults.get('namespaces');
      md.add(pe.getResourceURI(), ns.expand('rdf:type'), ns.expand('foaf:Person'));
      pe.commit().then((entry) => {
        this.list.getView().addRowForEntry(entry);
      });
    },
  });

  const ns = defaults.get('namespaces');

  return declare([BaseList], {
    includeCreateButton: true,
    entryType: ns.expand('foaf:Person'),
    postCreate() {
      this.inherited('postCreate', arguments);
      this.nlsSpecificBundle = {
        listHeader: 'List of Persons',
        createPopoverTitle: 'Register a new person.',
        createPopoverMessage: 'The person will be described using properties from the vocabulary FOAF (Friend Of A Friend).',
        createButtonLabel: 'Register person',
      };
      this.registerDialog('create', CDialog);
    },

    getTemplate() {
      if (!this.template) {
        const is = defaults.get('itemstore');
        is.createItem({
          type: 'text',
          nodetype: 'DATATYPE_LITERAL',
          id: 'locn:geometry',
          datatype: 'http://www.opengis.net/ont/geosparql#wktLiteral',
          property: 'http://www.w3.org/ns/locn#geometry',
          label: { en: 'Geographic boundingbox' },
          cardinality: { min: 1, pref: 1, max: 1 },
        });

        this.template = is.createTemplateFromChildren([
          'foaf:name',
          'locn:geometry',
          'dcterms:description',
        ]);
      }
      return this.template;
    },
  });
});
