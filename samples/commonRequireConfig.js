require.config({
  baseUrl: '../libs', // Path relative to bootstrapping html file.
  // Paths relative baseUrl, only those that deviate from baseUrl/{modulename}
  // are explicitly listed.
  paths: {
    'entryscape-commons': '..',
    nls: '../nls',
    theme: '../theme',
    text: 'requirejs-text/text',
    i18n: 'di18n/i18n',
    fuelux: 'fuelux/js',
    bootstrap: 'bootstrap-amd/lib',
    select2: 'select2/src/js',
    moment: 'moment/moment',
    jquery: 'jquery/src',
    sizzle: 'sizzle/dist/sizzle',
    'jquery.mousewheel': 'select2/src/js/jquery.mousewheel.shim',
    jstree: 'jstree/src/jstree',
    'jstree.dnd': 'jstree/src/jstree.dnd',
    'jstree.wholerow': 'jstree/src/jstree.wholerow',
    requireLib: 'requirejs/require',
    vis: 'vis/dist/vis',
    leaflet: 'leaflet/dist/leaflet',
    typeahead: 'typeahead.js/dist/typeahead.jquery',
    config: 'empty:', // Because we define it inline below.
    md5: 'md5/js/md5.min',
  },
  shim: {
    typeahead: { deps: ['jquery'] },
  },

  map: { // Standard part, do not change unless you know exactly what you are doing.
    '*': {
      jquery: 'jquery/jquery',  // In general, use the main module (for all unqualified jquery dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      has: 'dojo/has', // Use dojos has module since it is more clever.
      'dojo/text': 'text', // Use require.js text module
      // Make sure i18n, dojo/i18n and di18n/i18n are all treated as a SINGLE module named i18n.
      // (We have mapped i18n to be the module provided in di18n/i18n, see paths above.)
      'dojo/i18n': 'i18n',
      'di18n/i18n': 'i18n',
    },
    jquery: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (jquerys has dependencies to specific modules).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      'external/sizzle/dist/sizzle': 'sizzle',
    },
    bootstrap: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (bootstraps has dependencies to specific dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
    },
    'store/rest': {
      'dojo/request': 'dojo/request/xhr', // Force using xhr since we know we are in the browser
      'dojo/request/iframe': 'dojo/request/iframe', // Override above line for iframe path.
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/xhr',  // Force using xhr since we know we are in the browser
    },
  },
});
