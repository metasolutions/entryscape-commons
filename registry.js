/* eslint no-param-reassign: "off" */
define([
  'exports',
  'dojo/Deferred',
  'dojo/topic',
], (exports, Deferred, topic) => {
  // App generic registry methods.
  const registry = {};
  const initDefs = {};
  const initDefsResolved = {};

  exports.set = (key, obj) => {
    registry[key] = obj;
    if (!initDefs.hasOwnProperty(key)) {
      initDefs[key] = new Deferred();
    }
    if (initDefsResolved[key] !== true) {
      initDefs[key].resolve(obj);
      initDefsResolved[key] = true;
    }
    topic.publish(`registry/${key}`, obj);
  };
  exports.onInit = (key, callback) => {
    if (!initDefs.hasOwnProperty(key)) {
      initDefs[key] = new Deferred();
    }
    if (callback) {
      return initDefs[key].then(callback);
    }
    return initDefs[key];
  };
  exports.onChange = (key, callback, lastChange) => {
    const handler = topic.subscribe(`registry/${key}`, callback);
    if (lastChange === true && typeof registry[key] !== 'undefined') {
      callback(registry[key]);
    }

    return handler;
  };
  exports.onChangeOnce = (key, callback, lastChange) => {
    const handle = exports.onChange(key, (data) => {
      callback(data);
      handle.remove();
    }, lastChange);
  };

  exports.get = (key, callback) => {
    const val = registry[key];
    if (typeof callback === 'function') {
      if (typeof val !== 'undefined') {
        callback(val);
      } else {
        exports.onInit(key, callback);
      }
    }
    return val;
  };

  return exports;
});
