define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  'dojo/html',
  'di18n/locale',
  'dojo/dom-construct',
  'config',
  'entryscape-commons/dialog/AcknowledgeTextDialog',
  'di18n/NLSMixin',
  'i18n!nls/escoErrors',
], (declare, lang, Deferred, html, locale, domConstruct, config, AcknowledgeTextDialog, NLSMixin) =>
  declare([AcknowledgeTextDialog, NLSMixin.Dijit], {
    nlsBundles: ['escoErrors'],
    restrictionPath: '',
    postCreate() {
      this.inherited('postCreate', arguments);
      this.restrictionNode = domConstruct.create('div', null, this.containerNode);
      if (config.theme.commonRestrictionTextPath) {
        this.restrictionPath = config.theme.commonRestrictionTextPath;
      }
    },
    show(path, titleParam) {
      if (!titleParam) {
        title = this.NLSBundle0.restrictionHeader;
      }

      this.inherited('show', [path, title]);
      if (this.restrictionPath !== '') {
        const restrictionPath = this.restrictionPath;
        const restrictionNode = this.restrictionNode;
        const language = locale.get();
        if (this.currentLanguage === language) {
          return;
        }
        this.currentLanguage = language;
        require([`dojo/text!${restrictionPath}_${language}.html`], (content) => {
          html.set(restrictionNode, content);
        }, () => {
          require([`dojo/text!${restrictionPath}.html`], (content) => {
            html.set(restrictionNode, `<hr>${content}`);
          });
        });
      }
    },
  }));
