define([
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/_base/fx',
  'dojo/dom-style',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'fuelux/loader', // In template
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'dojo/string',
  'jquery',
  '../registry',
  'dojo/text!./AsyncHandlerTemplate.html',
  'i18n!nls/escoErrors',
], (lang, declare, basefx, domStyle, domAttr, domConstruct, domClass, loader, _WidgetBase,
    _TemplatedMixin, NLSMixin, string, jquery, registry, template) => {
    // Case Generic problem: Something went wrong... send error report / continue anyway / ok,
    //  proceed anyway.
    // Case signed out: Sign in again - > close dialog and open sign in dialog
    // Case lost connection: No contact with server, retry

  const INPROGRESS = 0;
  const GENERIC_PROBLEM = 1;
  const UNAUTHORIZED = 2;
  const SIGNED_OUT = 3;
  const LOST_CONNECTION = 4;

  const extractProblem = function (err) {
    if (typeof err === 'object' && err.response && typeof err.response.status === 'number') {
      const status = err.response.status;
      const major = Math.floor(status / 100);
      switch (major) {
        case 0:
          return LOST_CONNECTION;
        case 1:
        case 3:
          return GENERIC_PROBLEM;
        case 4:
          return status === 401 ? UNAUTHORIZED : GENERIC_PROBLEM;
        default:
      }
    }
    return GENERIC_PROBLEM;
  };

  return declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    progressDelay: 400,
    nlsBundles: ['escoErrors'],
    codes: {
      GENERIC_PROBLEM,
      UNAUTHORIZED,
      LOST_CONNECTION,
    },

    checkCountIntervalls: [2, 5, 10, 30, 60, 120, 300, 600],
    postCreate() {
      this.inherited('postCreate', arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
      this.promises = [];
      this.fadeIn = basefx.fadeIn({ node: this.domNode });
      this._ignoreNext = {};
      this._ignore = {};
    },

    show(promise, callType) {
      if (this.checkIgnoreTrue(callType)) {
        return;
      }

      this.closeDetails();
      const obj = {
        promise,
        callType,
        time: new Date().getTime(),
      };
      this.promises.push(obj);
      promise.then(
                lang.hitch(this, this.resolved, promise),
                lang.hitch(this, this.rejected, promise));
      this.setNewTimer();
    },

    addIgnore(callType, status, forNextRequest) {
      const ignoreObj = forNextRequest === true ? this._ignoreNext : this._ignore;
      if (typeof status === 'number') {
        ignoreObj[callType] = status;
      } else {
        ignoreObj[callType] = true;
      }
    },

    removeIgnore(callType) {
      delete this._ignoreNext[callType];
      delete this._ignore[callType];
    },

    checkIgnoreTrue(callType) {
      if (this._ignoreNext[callType] === true) {
        delete this._ignoreNext[callType];
        return true;
      }

      return this._ignore[callType] === true;
    },

    checkIgnore(obj) {
      const ct = obj.callType;
      let status = -1;
      if (obj.err) {
        status = extractProblem(obj.err);
      }

      const ignoreNextStatus = this._ignoreNext[ct];
      if (typeof ignoreNextStatus !== 'undefined') {
        delete this._ignoreNext[ct];
        return ignoreNextStatus === true ? true : ignoreNextStatus === status;
      }

      const ignoreStatus = this._ignore[ct];
      if (typeof ignoreStatus !== 'undefined') {
        return ignoreStatus === true ? true : ignoreStatus === status;
      }
      return false;
    },

    removeTimer(obj) {
      clearTimeout(obj.timer);
      delete obj.timer;
      delete this.timer;
    },
    setNewTimer() {
      if (this.dialogOpen) {
        return;
      }
      if (!this.timer) {
        for (let i = 0; i < this.promises.length; i++) {
          const obj = this.promises[i];
          if (!obj.resolved) {
            const remaining = this.progressDelay - (new Date().getTime() - obj.time);
            if (remaining < 0) {
              this.openDialog();
            } else {
              obj.timer = setTimeout(lang.hitch(this, function () {
                delete obj.timer;
                delete this.timer;
                this.openDialog();
              }), remaining);
              this.timer = true;
            }
            break;
          }
        }
      }
    },

    openDialog() {
      this.dialogOpen = true;
      domStyle.set(this.domNode, 'display', 'block');
      this.fadeIn.play();
      this.updateDialog();
    },

    updateOrCloseDialog() {
      let resolved = true;
      for (let i = 0; i < this.promises.length; i++) {
        const obj = this.promises[i];
        if (obj.resolved !== true) {
          resolved = false;
        }
      }
      if (resolved) {
        this.closeDialog();
      } else {
        this.updateDialog();
      }
    },

    updateDialog() {
      domAttr.set(this.messages, 'innerHTML', '');
      let rejectionReason = INPROGRESS;
      for (let i = 0; i < this.promises.length; i++) {
        const obj = this.promises[i];
        if (obj.resolved === false) {
          const reason = extractProblem(obj.err);
          if (reason > rejectionReason) {
            rejectionReason = reason;
          }
        }
      }
      if (rejectionReason === UNAUTHORIZED) {
        registry.get('entrystore').getAuth()
                    .getUserInfo(true).then(lang.hitch(this, function (userinfo) {
                      this.renderDialog(userinfo.id === '_guest' ? SIGNED_OUT : GENERIC_PROBLEM);
                    }));
      } else {
//                this.renderDialog(SIGNED_OUT);
        this.renderDialog(rejectionReason);
      }
    },

    closeDialog() {
      if (this.dialogOpen) {
        this.fadeIn.stop();
        domStyle.set(this.domNode, 'display', 'none');
        if (this._loader.firstChild) {
          jquery(this._loader.firstChild).loader('destroy');
        }
        this.promises = [];
        delete this.dialogOpen;
        this.checkCountIdx = -1;
        clearTimeout(this.checkCountdownTimeout);
      }
      domStyle.set(this.messages, 'display', '');
    },

    closeDialogSignedOut() {
      registry.get('verifyUser')();
      this.closeDialog();
    },
    renderDialog(state) {
      if (state === INPROGRESS) {
        jquery(this._loader.firstChild).loader('destroy');
        jquery(domConstruct.create('div', { class: 'loader' }, this._loader)).loader();
      } else {
        jquery(this._loader.firstChild).loader('destroy');
      }

      switch (state) {
        case INPROGRESS:
          domClass.remove(this.domNode, 'reject1 reject2 reject3');
          domClass.add(this.domNode, 'inprogress');
          break;
        case GENERIC_PROBLEM:
          domClass.remove(this.domNode, 'inprogress reject2 reject3');
          domClass.add(this.domNode, 'reject1');
          this.renderDetails();
          break;
        case SIGNED_OUT:
          domClass.remove(this.domNode, 'inprogress reject1 reject3');
          domClass.add(this.domNode, 'reject2');
          break;
        case LOST_CONNECTION:
          domClass.remove(this.domNode, 'inprogress reject1 reject2');
          domClass.add(this.domNode, 'reject3');
          this.checkConnection(true);
          break;
        default:
      }
    },
    renderDetails() {
      domAttr.set(this.messages, 'innerHTML', '');
      for (let i = 0; i < this.promises.length; i++) {
        const obj = this.promises[i];
        let progressClass = 'info';
        if (typeof obj.resolved !== 'undefined') {
          progressClass = obj.resolved === true ? 'success' : 'danger';
        }
        let message;
        if (obj.err && obj.err.response && obj.err.response.status === 412) {
          message = this.NLSBundle0.conflictProblem;
        } else {
          message = typeof obj.err === 'object' && typeof obj.err.message === 'string' ?
                        obj.err.message : obj.err;
        }
        if (obj.err) {
          console.error(`${obj.err}\n${obj.stack}`);
        }
        domConstruct.create('div', {
          class: `alert alert-${progressClass}`, innerHTML: `${obj.callType}: ${message}` }, this.messages);
      }
    },

    closeDetails() {
      this.setI18nState(1);
      domStyle.set(this.messages, 'display', 'none');
    },
    toggleDetails() {
      if (this._i18nState === 1) {
        this.setI18nState(2);
        domStyle.set(this.messages, 'display', 'block');
      } else {
        this.setI18nState(1);
        domStyle.set(this.messages, 'display', 'none');
      }
    },

    signIn() {
      const signInDialog = registry.get('signInDialog');
      this.closeDialog();
      domStyle.set(this.domNode, 'display', 'none');
      signInDialog.dialog.conditionalHide = function () {
        delete signInDialog.mainDialog.conditionalHide;
        signInDialog.dialog.hide();
        registry.get('verifyUser')();
      };
      signInDialog.show();
    },
    checkCountIdx: -1,
    checkCountdownTimeout: null,
    checkConnection(fromSystem) {
      if (fromSystem === true && this.checkCountIdx >= 0) {
        return;
      }
      this.checkCountIdx = 0;
      clearTimeout(this.checkCountdownTimeout);

      const es = registry.get('entrystore');
      const drawCounter = lang.hitch(this, function (seconds) {
        // Not connected. Connecting in 4s... Try Now
        domAttr.set(this.timeToCheck, 'innerHTML',
          string.substitute(this.NLSBundle0.notConnected, { time: seconds }));
      });
      let check;
      const countdown = lang.hitch(this, function (seconds) {
        this.checkCountdownTimeout = setInterval(lang.hitch(this, function () {
          const seconds_ = seconds - 1;
          if (seconds_ === 0) {
            clearTimeout(this.checkCountdownTimeout);
            check();
          } else {
            drawCounter(seconds_);
          }
        }), 1000);
        drawCounter(seconds);
      });
      const continueCountdown = lang.hitch(this, function () {
        countdown(this.checkCountIntervalls[this.checkCountIdx]);
        if (this.checkCountIdx < (this.checkCountIntervalls.length - 1)) {
          this.checkCountIdx += 1;
        }
      });
      check = function () {
        es.getREST().get(`${es.getBaseURI()}management/status`, 'text/plain').then((res) => {
          if (res === 'UP') {
            this.closeDialog();
          } else if (typeof res === 'object' && res.repositoryStatus === 'online') {
            continueCountdown();
          }
        }, continueCountdown);
      };
      check();
    },

    resolved(promise) {
      const obj = this.getPromiseObject(promise);
      obj.resolved = true;
      if (this.dialogOpen) {
        this.updateOrCloseDialog();
      } else {
        this.promises.splice(this.promises.indexOf(obj), 1);
      }
      if (obj.timer) {
        this.removeTimer(obj);
        this.setNewTimer();
      }
            // Only needed to remove from ignoreNext when callType and status is a match
      this.checkIgnore(obj);
    },
    rejected(promise, value) {
      const obj = this.getPromiseObject(promise);
      obj.resolved = false;
      obj.err = value;
      if (this.checkIgnore(obj)) {
        this.promises.splice(this.promises.indexOf(obj), 1);
        if (obj.timer) {
          this.removeTimer(obj);
        }
        this.updateOrCloseDialog();
        return;
      }
      if (this.dialogOpen) {
        this.updateDialog();
      } else {
        for (let i = 0; i < this.promises.length; i++) {
          const o = this.promises[i];
          if (o.timer) {
            this.removeTimer(o);
          }
        }
        this.openDialog();
      }
    },

    getPromiseObject(promise) {
      for (let i = 0; i < this.promises.length; i++) {
        const obj = this.promises[i];
        if (obj.promise === promise) {
          return obj;
        }
      }

      return null;
    },
  });
});
