define([
  // Common dependencies for booting the EntryScape platform
  'jquery',
  'entryscape-commons/nav/Site',
  'moment',
  'rdforms/view/bmd/all', // RDForms bootstrap dependency.
  'dojo/selector/_loader',
  'dojo/request/iframe',
  'entryscape-commons/nav/Layout',
  'entryscape-commons/nav/Start',
  // All translations in EntryScape-common
  'i18n!rdforms/view/nls/rdforms', // Translations for RDForms library.
  'i18n!dojo/cldr/nls/gregorian', // Translations for formating of dates.
  // No require-nls support for the following, explicit dependency to each language
], {});
