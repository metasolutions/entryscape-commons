define([
  'dojo/_base/array',
  'dojo/Deferred',
  'dojo/string',
  './registry',
  'store/EntryStore',
  'store/EntryStoreUtil',
  'rdfjson/namespaces',
  'rdfjson/Graph',
  'rdforms/template/ItemStore',
  'rdforms/template/bundleLoader',
  'rdforms/view/renderingContext',
  'config',
  'spa/init',
  './dialog/ConfirmDialog',
  './dialog/AcknowledgeDialog',
  './dialog/AcknowledgeTextDialog',
  './dialog/Progress',
  './dialog/OptionsDialog',
  './errors/RestrictionDialog',
  './errors/AsyncHandler',
  './util/configUtil',
  'rdforms/utils',
  'di18n/NLSMixin',
  'di18n/locale',
  'dojo/aspect',
  'dojo/_base/window',
  'dojo/dom-construct',
  'dojo/domReady',
  'i18n',
], (array, Deferred, string, registry, EntryStore, EntryStoreUtil, namespaces, Graph, ItemStore,
    bundleLoader, renderingContext, config, init, ConfirmDialog, AcknowledgeDialog,
    AcknowledgeTextDialog, Progress, OptionsDialog, RestrictionDialog, AsyncHandler, configUtil,
    utils, NLSMixin, locale, aspect, win, domConstruct, domReady, i18n) => {
  /**
   * Adds the folloging to the registry:
   * entrystore      - an instance of store/EntryStore.
   * entry           - an instance of store/Entry, refers to the current entry in use
   * itemstore       - an instance of rdforms/template/ItemStore.
   * createEntry     - creates a named or link entry depending on config.entrystore.resourceBase
   * entrychooser    - specifying per property if entries are choosen in repository or per context
   * dialogs         - a module for showing confirm, progress and acknowledge dialogs.
   * namespaces      - the rdfjson/namespaces module.
   * localize        - returns a string based on the locale from e.g. {en: "Hello", sv: "Hej"}.
   * rdfutils        - a module for getting labels from entry instances.
   * defaultLocale   - the language (ISO-code) specified by the browser environment on page load.
   * locale          - the language chosen, one of the supported languages of EntryScape.
   * userInfo        - the current user information, see store/Auth#getUserInformation.
   * userEntry       - the entry for the current user (_guest entry when unauthorized)
   * userEntryInfo   - user information including also a firstName, lastName and a displayName.
   * authorizedUser  - the username (store/User#getName) for the current user, null for _guest.
   * siteManager     - the current spa/Site instance handling the various views.
   * context         - the current context (instance of store/Context).
   * entry           - the current entry (instance of store/Entry).
   * isAdmin         - true if the current user is the site administrator
   * inAdminGroup    - true if the user is in the site admin group
   * hasAdminRights  - true if the user is either site administrator or
   *                 - in the site administrator group
   * baseUrl         - The base URL of the application
   *
   *
   * In addition, if the config contains a googleAnalyticsID the google analytics is activated.
   *
   * @exports store/rest
   * @namespace
   */

  // If there are NLS overrides, register them early.
  if (config.locale.NLSOverridePath) {
    i18n.setOverridesPath(config.locale.NLSOverridePath);
  } else if (typeof config.locale.NLSOverride === 'object') {
    Object.keys(config.locale.NLSOverride).forEach((key) => {
      i18n.addOverride(key, config.locale.NLSOverride[key]);
    });
  }

  // Default registration: entrystore
  const es = new EntryStore(
    (config.entrystore && config.entrystore.repository ? config.entrystore.repository : null));
  registry.set('entrystore', es);
  registry.set('entry', undefined);
  registry.set('entrystoreutil', new EntryStoreUtil(es));

  // Default registration: itemstore
  const items = new ItemStore();
  // registry.set("itemstore", items);
  if (config.itemstore) {
    if (config.itemstore.bundles) {
      bundleLoader(items, config.itemstore.bundles, () => {
        registry.set('itemstore', items);
      });
    }

    if (config.itemstore.appendLanguages) {
      const langs = renderingContext.getLanguageList();
      renderingContext.setLanguageList(langs.concat(config.itemstore.appendLanguages));
    }
    if (config.itemstore.languages) {
      renderingContext.setLanguageList(config.itemstore.languages);
    }
    if (config.itemstore.choosers) {
      require(config.itemstore.choosers, (...args) => {
        args.forEach(chooser => chooser.registerDefaults());
      });
    }
  }
  if (config.entrychooser) {
    registry.set('entrychooser', config.entrychooser);
  }

  const getResourceBase = (scope) => {
    if (!config.entrystore || !config.entrystore.resourceBase) {
      return null;
    }
    const o = config.entrystore.resourceBase;
    return typeof o === 'object' ? o[scope] || o.default : o;
  };

  registry.set('createEntry', (context, scope) => {
    const c = context || registry.get('context');
    const resourceBase = getResourceBase(scope);
    if (resourceBase) {
      const resURI = string.substitute(resourceBase,
        { contextId: c.getId(), entryId: '_newId', uuid: utils.generateUUID() });
      return c.newLink(resURI);
    }
    return c.newNamedEntry();
  });

  // Default nls settings from config
  if (config.nls && config.nls.globalbase) {
    NLSMixin.setGlobalNlsBundleBase(config.nls.globalbase);
  } else {
    NLSMixin.setGlobalNlsBundleBase('nls');
  }

  const async = new AsyncHandler();
  es.addAsyncListener((promise, callType) => {
    async.show(promise, callType);
  });
  registry.set('asynchandler', async);

  const container = domConstruct.create('div', { id: 'entryscape_dialogs', 'class': 'entryscape' },
    win.body());
  renderingContext.setPopoverContainer(container);

  // Default registration of support for global "dialogs"
  const confirmDialog = new ConfirmDialog();
  const acknowledgeDialog = new AcknowledgeDialog();
  const progress = new Progress();
  const acknowledgeTextDialog = new AcknowledgeTextDialog();
  const restrictionDialog = new RestrictionDialog();
  const optionsDialog = new OptionsDialog();
  registry.set('dialogs', {
    progress(promise) {
      return progress.show(promise);
    },
    acknowledge(message, okLabel, callback) {
      return acknowledgeDialog.show(message, okLabel, callback);
    },
    confirm(message, confirmLabel, rejectLabel, callback) {
      return confirmDialog.show(message, confirmLabel, rejectLabel, callback);
    },
    acknowledgeText(path, title, callback) {
      return acknowledgeTextDialog.show(path, title, callback);
    },
    restriction(path) {
      restrictionDialog.show(path);
    },
    options(message, options) {
      return optionsDialog.show(message, options);
    },
  });

  // config.rdf section
  config.rdf = config.rdf || {};
  // Default registration of namespaces
  registry.set('namespaces', namespaces);
  if (config.rdf.namespaces) {
    namespaces.add(config.rdf.namespaces);
  }
  namespaces.add('store', 'http://entrystore.org/terms/');

  registry.set('localize', lang2val => utils.getLocalizedValue(lang2val).value);

  const fixEoG = (eog, uri) => {
    if (!(eog instanceof Graph)) {
      return { g: eog.getMetadata(), r: eog.getResourceURI() };
    }
    return { g: eog, r: uri };
  };

  let rdfutils;
  let labelProperties;
  let descriptionProperties;
  registry.set('rdfutils', rdfutils = {
    setLabelProperties(arr) {
      labelProperties = arr;
    },
    setDescriptionProperties(arr) {
      descriptionProperties = arr;
    },
    getLabel(eog, uri) {
      const { g, r } = fixEoG(eog, uri);
      return utils.getLocalizedValue(
        utils.getLocalizedMap(g, r, labelProperties)).value;
    },
    getLabelMap(eog, uri) {
      const { g, r } = fixEoG(eog, uri);
      return utils.getLocalizedMap(g, r, labelProperties);
    },
    getDescription(eog, uri) {
      const { g, r } = fixEoG(eog, uri);
      return utils.getLocalizedValue(
        utils.getLocalizedMap(g, r, descriptionProperties)).value;
    },
    getDescriptionMap(eog, uri) {
      const { g, r } = fixEoG(eog, uri);
      return utils.getLocalizedMap(g, r, descriptionProperties);
    },
  });

  if (config.rdf.labelProperties) {
    rdfutils.setLabelProperties(config.rdf.labelProperties);
  } else {
    rdfutils.setLabelProperties([
      'foaf:name',
      ['foaf:firstName', 'foaf:lastName'],
      ['foaf:givenName', 'foaf:familyName'],
      'vcard:fn',
      'skos:prefLabel',
      'dcterms:title',
      'dc:title',
      'rdfs:label',
      'skos:altLabel',
      'vcard:hasEmail',
    ]);
  }

  if (config.rdf.descriptionProperties) {
    rdfutils.setDescriptionProperties(config.rdf.descriptionProperties);
  } else {
    rdfutils.setDescriptionProperties([
      'http://purl.org/dc/terms/description',
    ]);
  }

  // Fix defaultLocale and locale
  registry.set('defaultLocale', locale.get());
  registry.onChange('locale', (l) => {
    if (locale.get() !== l) {
      locale.set(l);
    }
  });

  registry.onChange('locale', () => {
    // Combining the current language, with accepted languages from the browser and finally the
    // languages supported by entryscape. The given order should be respected and no duplicates
    // allowed.
    const primary = new Set();
    primary.add(registry.get('locale'));
    const alo = registry.get('clientAcceptLanguages');
    /* eslint-disable no-confusing-arrow */
    if (alo) {
      Object.keys(alo).sort((l1, l2) => alo[l1] < alo[l2] ? 1 : -1).forEach(l => primary.add(l));
    }
    config.locale.supported.map(o => o.lang).forEach(l => primary.add(l));
    renderingContext.setPrimaryLanguageCodes(Array.from(primary));
  });

  registry.onChange('userInfo', (userInfo) => {
    let bestlang;
    const newlang = userInfo.language == null ? registry.get('defaultLocale') : userInfo.language;
    for (let i = 0; i < config.locale.supported.length; i++) {
      const l = config.locale.supported[i].lang;
      if (newlang.indexOf(l) === 0) {
        if (bestlang == null || bestlang.length < l.length) {
          bestlang = l;
        }
      }
    }
    if (userInfo.clientAcceptLanguage) {
      registry.set('clientAcceptLanguages', userInfo.clientAcceptLanguage);
    }
    registry.set('locale', bestlang || config.locale.fallback);
  }, true);

  if (typeof config.reCaptchaSiteKey !== 'undefined') {
    window.rcCallback = () => registry.set('recaptcha', grecaptcha);
    registry.set('addRecaptcha', (id, callback, expiredCallback) => {
      registry.onInit('recaptcha').then((rec) => {
        rec.render(id, {
          sitekey: config.reCaptchaSiteKey,
          callback,
          'expired-callback': expiredCallback,
        });
      });
    });
    require(['https://www.google.com/recaptcha/api.js?onload=rcCallback&render=explicit']);
  } else {
    registry.set('addRecaptcha', () => {
      console.error('Cannot add recaptcha since no site key is provided in config.reCaptchaSiteKey.');
    });
  }

  // Load userInfo from the start and listen to authorization changes.
  let f;
  const auth = es.getAuth();
  const updateUserInfo = (userInfo) => {
    registry.set('userInfo', userInfo);
    if (userInfo.id !== '_guest') {
      registry.set('authorizedUser', userInfo.user);
    } else {
      registry.set('authorizedUser', null);
    }
  };
  const updateUserEntry = userEntry => registry.set('userEntry', userEntry);
  if (config.entrystore && config.entrystore.authentification !== false) {
    auth.getUserInfo().then(f = (userInfo) => {
      updateUserInfo(userInfo);
      auth.getUserEntry().then(updateUserEntry);
    });
    auth.addAuthListener((topic, userinfo) => {
      f(userinfo);
    });
  }

  registry.set('verifyUser', () => {
    // Forces both userinfor and userEntry to be refreshed.
    auth.getUserEntry(true).then((userEntry) => {
      // Should return immediately since it is needed for loading the userEntry
      auth.getUserInfo(updateUserInfo);
      updateUserEntry(userEntry);
    });
  });

  registry.onChange('userEntry', (userEntry) => {
    const metadata = userEntry.getMetadata();
    const user = userEntry.getResource(true);
    const res = {
      lastName: metadata.findFirstValue(null, 'http://xmlns.com/foaf/0.1/familyName'),
      firstName: metadata.findFirstValue(null, 'http://xmlns.com/foaf/0.1/givenName'),
      displayName: user.getName(),
      user: user.getName(),
      homeContext: user.getHomeContext(),
      entryId: userEntry.getId(),
      language: user.getLanguage(),
    };
    if (res.firstName) {
      res.displayName = res.firstName;
      if (res.lastName) {
        res.displayName += ` ${res.lastName}`;
      }
    }

    const isAdmin = userEntry.getId() === '_admin';
    const inAdminGroup = userEntry.getParentGroups().indexOf(userEntry.getEntryStore().getEntryURI('_principals', '_admins')) >= 0;
    registry.set('isAdmin', isAdmin);
    registry.set('inAdminGroup', inAdminGroup);
    registry.set('hasAdminRights', isAdmin || inAdminGroup);
    registry.set('userEntryInfo', res);
    if (registry.get('locale') !== res.language && res.language != null) {
      registry.set('locale', res.language);
    }
  }, true);

  registry.set('getGroupWithHomeContext', (context) => {
    const d = new Deferred();
    const store = registry.get('entrystore');
    let foundOne = false;

    context.getEntry().then((contextEntry) => {
      const contextACL = contextEntry.getEntryInfo().getACL(true);
      array.forEach(contextACL.rwrite, (principalId) => {
        if (foundOne) {
          return;
        }
        const puri = store.getEntryURI('_principals', principalId);
        store.getEntry(puri, null)
          .then(e => (e.isGroup() ? e.getResource() : null))
          .then((groupResource) => {
            if (!foundOne && groupResource != null &&
              groupResource.getHomeContext() === context.getId()) {
              groupResource.getEntry().then((groupEntry) => {
                d.resolve(groupEntry);
              });
              foundOne = true;
            }
          });
      });
    });
    return d.promise;
  });

  const objToArray = (oOrArr) => {
    if (typeof oOrArr === 'object') {
      const arr = [];
      Object.keys(oOrArr).forEach((key) => {
        const o = oOrArr[key];
        o.name = o.name || key;
        arr.push(o);
      });
      return arr;
    }
    return oOrArr;
  };

  config.entitytypes = objToArray(config.entitytypes);
  config.contexttypes = objToArray(config.contexttypes);
  config.contentviewers = objToArray(config.contentviewers);

  domReady(() => {
    if (config.site) {
      config.site.modules = objToArray(config.site.modules);
      config.site.views = objToArray(config.site.views);
      if (config.site.moduleList) {
        const name2module = {};
        array.forEach(config.site.modules, (m) => {
          name2module[m.name] = m;
        });
        config.site.modules = array.map(config.site.moduleList, mname => name2module[mname]);
      }
      registry.onInit('locale', () => {
        const baseUrl = configUtil.getBaseUrl();
        registry.set('baseUrl', baseUrl);
        const siteConfig = Object.assign({}, config.site, { baseUrl });
        init(siteConfig, (site) => {
          registry.set('siteManager', site);
          // Make sure the current context and entry are set in the registry
          aspect.after(site, 'beforeViewChange', (res, args) => {
            const params = args[1];
            if (params.context != null) {
              registry.set('context', es.getContextById(params.context));
              if (params.entry != null) {
                const uri = es.getEntryURI(params.context, params.entry);
                es.getEntry(uri).then((entry) => {
                  registry.set('entry', entry);
                });
              }
            } else {
              registry.set('context', null);
            }
          });
          registry.onChange('locale', () => {
            site.reRenderCurrentView();
          });
        });
      });
    }

    if (typeof config.googleAnalyticsID !== 'undefined') {
      window.GoogleAnalyticsObject = 'ga';
      window.ga = function () {
        (window.ga.q = window.ga.q || []).push(arguments);
      };
      window.ga.l = 1 * new Date();
      window.ga('create', config.googleAnalyticsID, 'auto');
      window.ga('send', 'pageview', { page: location.pathname + location.search + location.hash });
      window.addEventListener('error', (err) => {
        const lineAndColumnInfo = err.colno ? ` line:${err.lineno}, column:${err.colno}` : ` line:${err.lineno}`;
        window.ga('send', 'event', 'JavaScript Error', err.message, `${err.filename + lineAndColumnInfo} -> ${navigator.userAgent}`, 0, true);
      });
      require(['https://www.google-analytics.com/analytics.js']);
    }
  });
  return registry;
});
