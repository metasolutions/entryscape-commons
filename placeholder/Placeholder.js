define([
  'dojo/_base/declare',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/dom-style',
  'dojo/text!./PlaceholderTemplate.html',
  'dojo/dom-attr',
  'dojo/dom-class',
  'di18n/NLSMixin',
  'di18n/localize',
  'dojo/dom-style',
  'i18n!nls/escoPlaceholder',
], (declare, _WidgetBase, _TemplatedMixin, domStyle, template, domAttr, domClass, NLSMixin, localize) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    missingImageClass: 'question',
    searchImageClass: 'search',
    nlsBundles: ['escoPlaceholder'],
    bid: 'escoPlaceholder',
    placeholderText: '',
    searchMode: false,
    includeCreateButton: false,

    show(searchMode) {
      domStyle.set(this.domNode, 'display', 'block');
      this.render(searchMode);
    },
    hide() {
      domStyle.set(this.domNode, 'display', 'none');
    },
    render(searchMode) {
      this.searchMode = searchMode;
      if (searchMode) {
        this.setImageClass(this.searchImageClass);
      } else {
        this.setImageClass(this.missingImageClass);
      }
      if (this.includeCreateButton) {
        domStyle.set(this.__placeholderButton, 'display', 'block');
      }
      if (this.NLSBundle0) {
        this.localeChange();
      }
    },
    localeChange() {
      const text = this.getText();
      let nlsObj = this.getNlsForCButton();
      if (text) {
        this.setText(text);
        this.setNlsForCButton(nlsObj);
      } else if (this.searchMode) {
        this.setText(this.NLSBundle0.emptySearchMessage);
        this.setNlsForCButton({});
      } else {
        const name = this.getName();
        if (name) {
          nlsObj = [];
          this.setText(localize(this.NLSBundle0,
            'emptyMessageWithName', { 1: name }));
          const buttonLabel = localize(this.NLSBundle0, 'createButtonWithName', { 1: name });
          const buttonTitle = localize(this.NLSBundle0, 'createButtonTitleWithName', { 1: name });
          nlsObj.nlsKey = buttonLabel;
          nlsObj.nlsKeyTitle = buttonTitle;
          this.setNlsForCButton(nlsObj);
        } else {
          this.setText(this.NLSBundle0.emptyMessage);
        }
      }
    },
    getText() {
      if (this.placeholderText && this.placeholderText !== '') {
        return this.placeholderText;
      }
      return '';
    },
    getNlsForCButton() {
      return {};
    },
    getName() {
    },
    setImageClass(cls) {
      domClass.remove(this.__placeholderImage);
      domClass.add(this.__placeholderImage, `fa fa-5x center-block fa-${cls}`);
    },
    setText(text) { // pass localized message
      domAttr.set(this.__placeholdertxt, 'innerHTML', text);
    },
    setNlsForCButton(nlsObj) {
      // {nlsKey: this.nlsCreateEntryLabel,nlsKeyTitle: this.nlsCreateEntryTitle, }
      if (Object.keys(nlsObj).length === 0) {
        domStyle.set(this.__placeholderButton, 'display', 'none');
      } else {
        domAttr.set(this.__placeholderCButton, 'innerHTML', ` ${nlsObj.nlsKey}`);
        domAttr.set(this.__placeholderCButton, 'title', nlsObj.nlsKeyTitle);
      }
    },
    createAction() {
    },
  }));
