define([
  'dojo/_base/declare',
  './Placeholder',
  'di18n/localize',
  '../defaults',
], (declare, Placeholder, localize, defaults) =>
    /**
     * This implementation tries to get icon and name of entries by looking
     * at the surrounding view.
     */
   declare([Placeholder], {
     list: null,
     postCreate() {
       const site = defaults.get('siteManager');
       this.viewDef = site.getViewDef(site.getUpcomingOrCurrentView());
       if (this.viewDef && this.viewDef.faClass) {
         this.missingImageClass = this.viewDef.faClass;
       }
       this.inherited(arguments);
     },
     render() {
       this.includeCreateButton = this.list ? this.list.includeCreateButton === true : false;
       this.inherited(arguments);
     },
     getName() {
       const site = defaults.get('siteManager');
       const viewDef = site.getViewDef(site.getUpcomingOrCurrentView());
       if (viewDef && viewDef.title) {
         return defaults.get('localize')(viewDef.title);
       }

       return '';
     },
     getText() {
       if (!this.searchMode && this.list) {
         return this.list.getEmptyListWarning();
       }
       return '';
     },
     getNlsForCButton() {
       if (!this.searchMode && this.list) {
         return this.list.getNlsForCButton();
       }
       return {};
     },
     createAction() {
       this.list.openDialog('create', {});
     },
   }));
