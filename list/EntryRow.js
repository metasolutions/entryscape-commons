define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/on',
  'dojo/string',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom',
  '../registry',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./EntryRowTemplate.html',
  '../menu/DropdownMenu',
  'entryscape-commons/util/dateUtil',
  'dojo/dom-prop',
], (declare, lang, array, on, string, domAttr, domConstruct, domClass, dom, registry, _WidgetBase,
    _TemplatedMixin, template, DropdownMenu, dateUtil, domProp) =>

  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    entry: null,
    showCol0: false,
    showCol1: false,
    showCol3: true,
    showCol4: true,
    // true forces menu, false disallows menu, otherwise nr. of buttons will decide (> 1 -> menu)
    rowButtonMenu: null,
    nlsDateTitle: 'modifiedDateTitle',

    constructor() {
      this.buttons = {};
    },
    postCreate() {
      this.inherited('postCreate', arguments);
      if (this.showCol0) {
        domClass.remove(this.rowNode, 'col0hidden');
      }
      if (this.showCol1) {
        domClass.remove(this.rowNode, 'col1hidden');
      }
      if (this.showCol3) {
        domClass.remove(this.rowNode, 'col3hidden');
      }
      if (this.nlsGenericBundle) {
        this.updateLocaleStrings(this.nlsGenericBundle, this.nlsSpecificBundle);
      }
      if (!this.showCol4) {
        domClass.add(this.rowNode, 'col4hidden');
      } else {
        this.updateActions();
      }
    },

    updateLocaleStrings(generic, specific) {
      this.nlsGenericBundle = generic;
      this.nlsSpecificBundle = specific;
      // 18thMar2016 if dropdown menu enabled,build localized strings for dropdown menu items
      if (this.dropdownMenu) {
        this.dropdownMenu.updateLocaleStrings(generic, specific);
      }

      Object.keys(this.buttons).forEach((name) => {
        const params = this.buttons[name];
        if (params.params.nlsKeyTitle) {
          domAttr.set(params.element, 'title', (specific && specific[params.params.nlsKeyTitle])
            || generic[params.params.nlsKeyTitle] || '');
        }
      }, this);

      this.render();
    },

    isRowClick(ev) {
      return dom.isDescendant(ev.target, this.rowNode);
    },

    reRender() {
      if (this.showCol4) {
        this.updateActions();
      }
      if (this.nlsGenericBundle) {
        this.updateLocaleStrings(this.nlsGenericBundle, this.nlsSpecificBundle);
      }
    },

    updateActions() {
      // delete any existing actions
      domConstruct.empty(this.buttonsNode);

      // 18thMar2016 if dropdown menu enabled,construct dropdown menu with info,edit,removebuttons
      const rowButtons = this.list.getRowActions();
      let buttonPotentiallyInMenu = 0;
      for (let i = 0; i < rowButtons.length; i++) {
        if (rowButtons[i].noMenu !== true) {
          buttonPotentiallyInMenu += 1;
        }
      }
      if (this.rowButtonMenu === true ||
        (buttonPotentiallyInMenu > 1 && this.rowButtonMenu !== false)) {
        this.dropdownMenu = new DropdownMenu({}, domConstruct.create('button', null, this.buttonsNode));
        domClass.add(this.dropdownMenu.domNode, 'pull-right');
        rowButtons.forEach(lang.hitch(this, this.installMenuItem));
      } else {
        rowButtons.forEach(lang.hitch(this, this.installButton));
      }
    },

    /**
     * @deprecated use corresponding method installActionOrNot.
     */
    installButtonOrNot(params) {
      return this.list.installActionOrNot(params, this);
    },
    installActionOrNot(params) {
      return this.list.installActionOrNot(params, this);
    },
    // 18thMar2016 creating dropdown menu items
    installMenuItem(paramsParams) {
      // 18thMar2016 check button is enabled as dropdown menu item or not
      const params = lang.clone(paramsParams);
      if (params.noMenu) {
        this.installButton(params);
      } else {
        const access = this.installActionOrNot(params);
        if (access === false) {
          return;
        }
        if (params.method && typeof this[params.method] === 'function') {
          params.method = lang.hitch(this, params.method);
        } else if (typeof this[`action_${params.name}`] === 'function') {
          params.method = lang.hitch(this, `action_${params.name}`);
        } else {
          params.method = lang.hitch(this.list, this.list.openDialog, params.name, { row: this });
        }
        if (access !== 'disabled') this.dropdownMenu.addItem(params);
      }
    },
    installButton(params) {
      const access = this.installButtonOrNot(params);
      if (access === false) {
        return;
      }
      const el = domConstruct.create('button', {
        type: 'button',
        class: `btn btn-sm btn-${params.button}`,
        title: params.title || '',
      }, this.buttonsNode,
        params.first === true ? 'first' : 'last');
      let cls;
      if (params.iconType === 'fa') {
        cls = `fa fa-fw fa-${params.icon}`;
      } else { // Default is glyphicons
        cls = `glyphicon glyphicon-${params.icon}`;
      }
      domConstruct.create('span', {
        class: cls,
        'aria-hidden': true,
      }, el);
      this.buttons[params.name] = { params, element: el };
      if (access === 'disabled') {
        domClass.add(el, 'disabled');
      } else {
        on(el, 'click', lang.hitch(this, function (ev) {
          ev.stopPropagation();
          if (params.method && typeof this[params.method] === 'function') {
            this[params.method]();
          } else if (typeof this[`action_${params.name}`] === 'function') {
            this[`action_${params.name}`]();
          } else {
            this.list.openDialog(params.name, { row: this });
          }
        }));
      }
    },
    render() {
      if (this.showCol1) {
        this.renderCol1();
      }

      const name = this.getRenderNameHTML();
      if (typeof name === 'string') {
        domAttr.set(this.nameNode, 'innerHTML', name);
      } else if (typeof name === 'object' && typeof name.then === 'function') {
        name.then(lang.hitch(this, function (nameStr) {
          domAttr.set(this.nameNode, 'innerHTML', nameStr);
        }));
      }
      if (this.showCol3) {
        this.renderCol3();
      }
    },

    getRowClickLink() {
      // Override
    },

    getRenderNameHTML() {
      const name = this.getRenderName();
      const href = this.getRowClickLink() || this.list.getRowClickLink(this);
      if (href) {
        return `<a href="${href}">${name}</a>`;
      }
      return name;
    },

    getRenderName() {
      const rdfutils = registry.get('rdfutils');
      return rdfutils.getLabel(this.entry);
    },

    renderCol1() {
    },

    renderCol3() {
      this.renderDate();
    },
    renderDate() {
      if (this.nlsGenericBundle) { // Localization strings are loaded.
        try {
          const modDate = this.getDate();
          const mDateFormats = dateUtil.getMultipleDateFormats(modDate);
          const dateTitle = this.nlsSpecificBundle[this.nlsDateTitle] ||
            this.nlsGenericBundle[this.nlsDateTitle] || '';
          const tStr = string.substitute(dateTitle, { date: mDateFormats.full });
          domAttr.set(this.col3Node, { innerHTML: mDateFormats.short, title: tStr });
        } catch (e) {
          // TODO strange case when gregorian has not been loaded in time.
        }
      }
    },
    getDate() {
      return this.entry.getEntryInfo().getModificationDate();
    },
    updateCheckBox(select) {
      select ? domProp.set(this.checkboxNode, 'checked', true) : domProp.set(this.checkboxNode, 'checked', false);
    },
    showCheckboxColumn() {
      this.showCol0 = true;
      if (this.showCol0) {
        domClass.remove(this.rowNode, 'col0hidden');
      }
    },
    isChecked() {
      return domProp.get(this.checkboxNode, 'checked');
    },
    getCheckbox() {
      return this.checkboxNode;
    },
  }));
