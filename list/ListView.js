define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/Deferred',
  'dojo/fx',
  'dojo/_base/fx',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/dom-attr',
  '../menu/DropdownMenu',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./ListViewTemplate.html',
  'jquery',
  'dojo/aspect',
  'entryscape-commons/util/uiUtil',
  './components/ResultSize',
  'di18n/localize',
  'mithril',
  'dojo/promise/all',
  '../defaults',
  'dojo/dom-prop',
], (declare, lang, array, Deferred, fx, _fx, on, domConstruct, domStyle, domClass,
    domAttr, DropdownMenu, _WidgetBase, _TemplatedMixin, template, jquery,
    aspect, uiUtil, ResultSize, localize, m, all, defaults, domProp) =>
  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    list: null,
    includeSortOptions: true,
    includeHead: true,
    includeHeadBlock: true,
    includeHeader: false,
    includeResultSize: true,
    searchVisibleFromStart: true,
    includeExpandButton: true,
    searchInList: true,
    rowClickDialog: null,
    sortOrder: 'mod',
    nlsGenericBundle: null,
    nlsListHeaderKey: 'listHeader',
    nlsListHeaderTitleKey: 'listHeaderTitle',
    nlsListResultSizeKey: 'listResultSizeText',
    nlsTypeaheadPlaceholderKey: 'typeaheadPlaceholder',
    listSearchPlaceholderKey: 'listSearchPlaceholder',
    placeHolderClass: null,
    buttonMenu: null, // true forces menu, false disallows menu, otherwise nr.of buttons will decide

    postCreate() {
      this.inherited('postCreate', arguments);
      this.localePromise = new Deferred();
      this.listSize = 0;
      this.rows = [];
      this.newRows = [];
      this.buttons = {};
      const buttons = this.list.getListActions();

      if (this.includeHead) {
        domStyle.set(this.head, 'display', '');
      }

      if (this.includeHeadBlock) {
        domStyle.set(this.headBlock, 'display', '');
      }

      if (!this.includeHeader) {
        domStyle.set(this.headerContainerInner, 'display', 'none');
        domConstruct.place(this.searchBlockInner, this.headerContainerInner, 'before');
        //  domConstruct.place(this.sortBlockInner, this.buttonContainer, 'last');
      }
      if (this.searchVisibleFromStart && this.searchInList === true) {
        // domStyle.set(this.searchBlock, 'display', '');
        domClass.toggle(this.expandButtonIcon, 'fa-chevron-right');
        domClass.toggle(this.expandButtonIcon, 'fa-chevron-down');
      }
      if (this.includeExpandButton && this.searchInList === true) {
        domStyle.set(this.expandButton, 'display', '');
      }

      if (buttons.length > 0) {
        // If forced or more than two buttons allowed in menu and button menu
        // not explicitly disallowed, construct dropdown menu with buttons
        let buttonPotentiallyInMenu = 0;
        for (let i = 0; i < buttons.length; i++) {
          if (buttons[i].noMenu !== true) {
            buttonPotentiallyInMenu += 1;
          }
        }
        if (this.buttonMenu === true ||
          (buttonPotentiallyInMenu > 2 && this.buttonMenu !== false)) {
          this.dropdownMenu = new DropdownMenu({
            labelHeight: true,
            inHeader: true,
          }, domConstruct.create('button', null, this.buttonContainer));
          domClass.add(this.dropdownMenu.domNode, 'pull-right');
          buttons.forEach(lang.hitch(this, this.installMenuItem));
        } else {
          buttons.forEach(lang.hitch(this, this.installButton));
        }
        domClass.remove(this.headerContainer, 'col-md-12');
        domClass.add(this.buttonContainer, 'col-md-12');
        domStyle.set(this.buttonContainer, 'display', '');
        if (buttons.length > 1) {
          if (this.includeResultSize) {
            domClass.add(this.headerContainer, 'col-md-5 col-sm-6');

            domClass.add(this.resultSizeContainer, 'col-md-1 col-sm-1');
          } else {
            domClass.add(this.headerContainer, 'col-md-5 col-sm-7');
          }
        } else if (this.includeResultSize) {
          domClass.add(this.headerContainer, 'col-md-5 col-sm-6');
          domClass.add(this.resultSizeContainer, 'col-md-1 col-sm-1');
        } else {
          domClass.add(this.headerContainer, 'col-md-5 col-sm-8');
        }
      }

      if (this.includeSortOptions === true) {
        domStyle.set(this.sortBlock, 'display', '');
        on(this.sortOrderModNode, 'click', lang.hitch(this, function () {
          if (this.sortOrder !== 'mod') {
            this.sortOrder = 'mod';
            this.action_refresh();
            domClass.toggle(this.sortOrderModNode, 'active');
            domClass.toggle(this.sortOrderTitleNode, 'active');
          }
        }));
        on(this.sortOrderTitleNode, 'click', lang.hitch(this, function () {
          if (this.sortOrder !== 'title') {
            this.sortOrder = 'title';
            this.action_refresh();
            domClass.toggle(this.sortOrderModNode, 'active');
            domClass.toggle(this.sortOrderTitleNode, 'active');
          }
        }));
      }

      if (this.searchInList) {
        let t;
        const searchTriggered = lang.hitch(this, this.action_refresh);
        on(this.searchTermNode, 'keyup', () => {
          if (t != null) {
            clearTimeout(t);
          }
          t = setTimeout(searchTriggered, 300);
        });
      }

      on(this.expandButton, 'click', lang.hitch(this, function () {
        if (domStyle.get(this.searchBlock, 'display') === 'none') {
          fx.wipeIn({
            node: this.searchBlock,
            duration: 300,
          }).play();
        } else {
          fx.wipeOut({
            node: this.searchBlock,
            duration: 300,
          }).play();
        }
        domClass.toggle(this.expandButtonIcon, 'fa-chevron-right');
        domClass.toggle(this.expandButtonIcon, 'fa-chevron-down');
      }));
      if (this.rowClickDialog != null) {
        domClass.add(this.domNode, 'rowClickEnabled');
      }
      if (this.includeMassOperations === true) {
        domClass.add(this.domNode, 'escoMassOperations');
        on(this.selectallCheck, 'click', lang.hitch(this, function (event) {
          const target = event.target || event.srcElement;
          if (target.checked) {
            domStyle.set(this.btnAll, 'display', 'inline');
          } else {
            domStyle.set(this.btnAll, 'display', 'none');
          }
          this.rows.forEach((row) => {
            row.updateCheckBox(target.checked);
          });
          this.newRows.forEach((row) => {
            row.updateCheckBox(target.checked);
          });
        }));
      } else {
        domStyle.set(this.massOperationsNode, 'display', 'none');
      }
    },

    getSize() {
      return this.listSize;
    },
    getActualSize() {
      return this.actualListSize;
    },
    /**
     * Return actualListSize if actualListSize  !== =1 or listSize
     */
    getResultSize() {
      if (this.getActualSize() === -1) {
        return this.getSize();
      }

      return this.getActualSize();
    },

    getPageCount() {
      return this.pageCount;
    },

    getCurrentPage() {
      return this.currentPage;
    },

    /**
     * Bundle assumed to have the following localized strings:
     * listHeader - the header of the list.
     * createPopoverTitle - the title of a popover shown when hovering over the create button
     * createPopoverMessage - the content of a popover shown when hovering over the create button
     * createButtonLabel - the label of the create button
     *
     * @param bundle
     */
    updateLocaleStrings(generic, specific) {
      this.nlsGenericBundle = generic;
      this.nlsSpecificBundle = specific;
      if (this.localePromise.isResolved()) {
        this.localePromise = new Deferred();
      }
      this.localePromise.resolve({ g: generic, s: specific });

      // List header
      this.updateHeader();
      domAttr.set(this.searchTermNode, 'placeholder', (specific && specific[this.listSearchPlaceholderKey]) || generic[this.listSearchPlaceholderKey] || '');
      domAttr.set(this.searchTermNode, 'title', generic.listSearchTitle || '');
      domAttr.set(this.tooShortSearch, 'innerHTML', generic.listSearchTitle || '');
      domAttr.set(this.invalidSearch, 'innerHTML', generic.invalidSearchMessage || '');
      domAttr.set(this.sortOptionsLabel, 'innerHTML', generic.sortOptionsLabel);
      domAttr.set(this.sortOrderTitleCheck, 'innerHTML', generic.titleSortLabel);
      domAttr.set(this.sortOrderDateCheck, 'innerHTML', generic.dateSortLabel);
      domAttr.set(this.selectallLabel, 'innerHTML', generic.selectAllLabel);
      domAttr.set(this.typeaheadInput, 'placeholder', (specific && specific[this.nlsTypeaheadPlaceholderKey]) || generic[this.nlsTypeaheadPlaceholderKey] || '');

      this.rows.forEach((row) => {
        row.updateLocaleStrings(generic, specific);
      });
      this.newRows.forEach((row) => {
        row.updateLocaleStrings(generic, specific);
      });

      // if dropdown menu enabled,build localized strings for dropdown menu items
      if (this.buttonMenu) {
        this.dropdownMenu.updateLocaleStrings(generic, specific);
      }
      let mesg;
      Object.keys(this.buttons).forEach((name) => {
        const params = this.buttons[name];
        if (params.params.nlsKey) {
          domAttr.set(params.label, 'innerHTML', `&nbsp;${
          (specific && specific[params.params.nlsKey])
          || generic[params.params.nlsKey] || ''}`);
        }
        const popoverOptions = uiUtil.getPopoverOptions();
        mesg = null;
        if (params.params.nlsKeyMessage && params.params.nlsKeyMessage !== '') {
          mesg = (specific && specific[params.params.nlsKeyMessage])
            || generic[params.params.nlsKeyMessage];
        }

        if (mesg != null) {
          popoverOptions.content = mesg;
          jquery(params.element).popover(popoverOptions);
        } else {
          jquery(params.element).popover('destroy');
          if (params.params.nlsKeyTitle) {
            domAttr.set(params.element, 'title',
              (specific && specific[params.params.nlsKeyTitle]) ||
              generic[params.params.nlsKeyTitle] || '');
          }
        }
      });
    },

    updateHeader() {
      const lhk = this.nlsListHeaderKey;
      const lhtk = this.nlsListHeaderTitleKey;
      domAttr.set(this.listHeader, 'innerHTML',
        (this.nlsSpecificBundle && this.nlsSpecificBundle[lhk])
        || this.nlsGenericBundle[lhk] || '');
      domAttr.set(this.listHeader, 'title',
        (this.nlsSpecificBundle && this.nlsSpecificBundle[lhtk])
        || this.nlsGenericBundle[lhtk] || '');
    },

    installButton(params) {
      const el = domConstruct.create('button', {
        type: 'button',
        class: `pull-right btn btn-raised btn-${params.button}`,
      }, this.buttonContainer, params.first === true ? 'first' : 'last');
      domConstruct.create('span', { class: `fa fa-${params.icon}`, 'aria-hidden': true }, el);

      const label = domConstruct.create('span', { class: 'escoList__buttonLabel' }, el);
      this.buttons[params.name] = { params, element: el, label };
      if (params.method && typeof this[params.method] === 'function') {
        on(el, 'click', lang.hitch(this, params.method));
      } else if (typeof this[`action_${params.name}`] === 'function') {
        on(el, 'click', lang.hitch(this, `action_${params.name}`));
      } else {
        on(el, 'click', lang.hitch(this.list, this.list.openDialog, params.name, {}));
      }
    },
    installMenuItem(params) {
      if (params.noMenu) {
        this.installButton(params);
      } else {
        if (params.method && typeof this[params.method] === 'function') {
          params.method = lang.hitch(this, params.method);
        } else if (typeof this[`action_${params.name}`] === 'function') {
          params.method = lang.hitch(this, `action_${params.name}`);
        } else {
          params.method = lang.hitch(this.list, this.list.openDialog, params.name, {});
        }
        this.dropdownMenu.addItem(params);
      }
    },
    showPlaceholder(searchMode) {
      // hide hearder part
      // TODO find more elegant way to check if list header should be shown than
      // the very specific check specific to catalog/responsibles sharedControlLabel
      // This is a hotfix.
      if (searchMode || !this.searchInList || this.list.sharedControlLabel != null) {
        domStyle.set(this.listHeaderS, 'display', '');
      } else {
        domStyle.set(this.listHeaderS, 'display', 'none');
      }
      if (!this.dPlaceholder) {
        if (!this.dPlaceholderInProgress) {
          this.dPlaceholderInProgress = true;
          const Cls = this.placeholderClass;
          this.localePromise.then(lang.hitch(this, function () {
            delete this.dPlaceholderInProgress;
            this.dPlaceholder = new Cls({
              search: false,
              list: this.list,
            }, domConstruct.create('div', null, this.__placeholder));
            this.dPlaceholder.show(searchMode);
          }));
        }
      } else {
        this.dPlaceholder.show(searchMode)
      }
    },
    hidePlaceholder() {
      domStyle.set(this.listHeaderS, 'display', '');
      if (this.dPlaceholder) {
        this.dPlaceholder.hide();
      }
    },

    /**
     * @param {store/List} entryList
     */
    showEntryList(entryList) {
      this.entryList = entryList;
      this.pageCount = -1;
      this.actualListSize = -1;
      this.showPage(1);
    },
    setTableHead(rowHTML) {
      domAttr.set(this.tableHeading, 'innerHTML', rowHTML);
    },

    /**
     * Renders the ResultSize component in the footer of the list table.
     */
    showResultSize() {
      this.localePromise.then(() => {
        const bundle = this.nlsSpecificBundle.nlsListResultSizeKey ?
          this.nlsSpecificBundle : this.nlsGenericBundle;
        const tStr = localize(bundle, this.nlsListResultSizeKey, this.getResultSize());

        m.render(this.resultSizeContainer,
          m(ResultSize, { text: tStr }));
      });
    },

    showPage(page) {
      if (page < 1 || (page !== 1 && this.pageCount !== -1 && page > this.pageCount)) {
        return undefined;
      }
      this.currentPage = page;
      return this.entryList.getEntries(page - 1).then(lang.hitch(this, function (entryArr) {
        if (entryArr.length === 0 && page > 1) {
          this.emptyPage = true;
          this.showPage(page - 1);
          return;
        }
        this._clearRows(this.rows);
        this._clearRows(this.newRows);
        if (entryArr.length === 0 && this.placeholderClass != null) {
          this.showPlaceholder(this.searchTerm != null && this.searchTerm !== '');
          if (this.includeMassOperations === true) {
            domAttr.set(this.selectallCheck, 'disabled', 'disabled');
            domStyle.set(this.selectAll, 'cursor', 'not-allowed');
          }
        } else {
          this.hidePlaceholder();
        }
        this._checkSize(entryArr);
        this._calculatePageCount(entryArr);
        entryArr.forEach(this._renderRow, this);
        this._updatePagination();
        if (this.includeResultSize && (this.searchTerm != null && this.searchTerm.length !== 0)) {
          domStyle.set(this.resultSizeContainer, 'display', 'block');
          this.showResultSize();
        } else {
          domStyle.set(this.resultSizeContainer, 'display', 'none');
        }
        this.doneRenderingPage();
      }));
    },
    doneRenderingPage() {
    },
    removeRow(row) {
      this.listSize -= 1;
      this.actualListSize = -1;
      let idx = this.rows.indexOf(row);
      if (idx >= 0) {
        this.rows.splice(idx, 1);
      } else {
        idx = this.newRows.indexOf(row);
        if (idx >= 0) {
          this.newRows.splice(idx, 1);
        }
      }
      if (this.listSize === 0) {
        this.showPlaceholder(false);
        if (this.includeMassOperations === true) {
          domAttr.set(this.selectallCheck, 'disabled', 'disabled');
          domStyle.set(this.selectAll, 'cursor', 'not-allowed');
        }
      }
      this._enforceLimits();
      if (this.includeResultSize) {
        this.showResultSize();
      }
    },
    addRowForEntry(entry) {
      this.listSize += 1;
      this.actualListSize = -1;
      this._enforceLimits();
      if (this.listSize > 0) {
        this.hidePlaceholder();
        if (this.includeMassOperations === true) {
          domAttr.remove(this.selectallCheck, 'disabled');
          domStyle.set(this.selectAll, 'cursor', 'pointer');
        }
      }
      domProp.set(this.selectallCheck, 'checked', false);
      const row = this._renderRow(entry, true);
      if (this.includeResultSize) {
        this.showResultSize();
      }
      return row;
    },
    _clearRows(rows) {
      if (rows != null && rows.length > 0) {
        rows.forEach((row) => {
          row.destroy();
        });
        rows.splice(0, rows.length);
      }
    },
    _checkSize(arr) {
      this.listSize = this.entryList.getSize();
      if (arr.length < this.entryList.getLimit()
        || (arr.length === this.entryList.getLimit() && this.emptyPage)) {
        this.actualListSize = ((this.currentPage - 1) * this.entryList.getLimit()) + arr.length;
      }
      this.emptyPage = false;
      if (this.actualListSize >= 0) {
        this.listSize = this.actualListSize;
      }
      this._enforceLimits();
      this.updateListCount();
    },
    _enforceLimits() {
      if (this.buttonMenu) {
        this.dropdownMenu.enforceLimits(this.listSize, this.searchTerm);
      } else {
        Object.keys(this.buttons).forEach((name) => {
          const bconf = this.buttons[name];
          const maxLimit = bconf.params.max;
          if (bconf.params.disableOnSearch && this.searchTerm != null
            && this.searchTerm.length > 0) {
            domAttr.set(bconf.element, 'disabled', 'disabled');
          } else if (parseInt(maxLimit, 10) === maxLimit && maxLimit !== -1 &&
            maxLimit <= this.listSize) {
            domAttr.set(bconf.element, 'disabled', 'disabled');
          } else {
            domAttr.remove(bconf.element, 'disabled');
          }
        });
      }
    },
    _calculatePageCount(arr) {
      this.pageCount = Math.ceil(this.listSize / this.entryList.getLimit());
      domClass.toggle(this.domNode, 'multiplePages', this.pageCount > 1
        || (arr.length === 0 && this.currentPage > 1));
      return this.pageCount;
    },

    _renderRow(entry, newRow) {
      let node;
      if (newRow === true) {
        node = domConstruct.create('div', null, this.tableHeading, 'after');
      } else {
        node = domConstruct.create('div', null, this.rowListNode);
      }

      const Cls = this.rowClass;
      const row = new Cls({ list: this.list, entry }, node);
      if (newRow === true) {
        _fx.animateProperty({
          node: row.domNode,
          duration: 2500,
          properties: {
            backgroundColor: { start: 'yellow', end: domStyle.get(row.domNode, 'background') },
          },
          onEnd: () => {
            domStyle.set(row.domNode, 'background', '');
          },
        }).play();
      }
      if (this.nlsGenericBundle) {
        row.updateLocaleStrings(this.nlsGenericBundle, this.nlsSpecificBundle);
      }
      if (newRow === true) {
        this.newRows.push(row);
      } else {
        this.rows.push(row);
      }
      if (this.rowClickDialog != null) {
        on(row.domNode, 'click', lang.hitch(this, function (ev) {
          // Check if click should not trigger rowclick
          if (typeof row.isRowClick === 'function' && !row.isRowClick(ev)) {
            return;
          }
          // Check so click is not to disable a menu...
          if (domClass.contains(ev.target, 'dropdown-backdrop') || domClass.contains(ev.target, 'check')) {
            return;
          }
          ev.preventDefault();
          ev.stopPropagation();
          if (typeof row[`action_${this.rowClickDialog}`] === 'function') {
            row[`action_${this.rowClickDialog}`]({ row });
          } else {
            this.list.openDialog(this.rowClickDialog, { row });
          }
        }));
      }
      if (this.includeMassOperations === true) {
        row.showCheckboxColumn();
        on(row.getCheckbox(), 'click', lang.hitch(this, function (ev) {
          ev.stopPropagation();
          const totalRows = this.rows.concat(this.newRows);
          const checkedRows = [];
          totalRows.forEach((r) => {
            if (r.isChecked()) {
              checkedRows.push(r);
            }
          });
          if (checkedRows.length === totalRows.length) {
            domProp.set(this.selectallCheck, 'checked', true);
          } else {
            domProp.set(this.selectallCheck, 'checked', false);
          }
          if (checkedRows.length > 0) {
            domStyle.set(this.btnAll, 'display', 'inline');
          } else {
            domStyle.set(this.btnAll, 'display', 'none');
          }
        }));
      }
      return row;
    },
    _updatePagination() {
      let li;
      let a;
      if (this._pageNodes) {
        this._pageNodes.forEach((pageNode) => {
          domConstruct.destroy(pageNode);
        });
      }
      this._pageNodes = [];

      let i = 1;
      const end = this.pageCount > this.currentPage + 3 ? this.currentPage + 3 : this.pageCount;
      if (this.currentPage > 4) {
        i = this.currentPage - 3;
        li = domConstruct.create('li', null, this.paginationNextLi, 'before');
        this._pageNodes.push(li);
        a = domConstruct.create('span', { innerHTML: '&hellip;' }, li);
      }
      for (; i <= end; i++) {
        li = domConstruct.create('li', null, this.paginationNextLi, 'before');
        this._pageNodes.push(li);
        a = domConstruct.create('a', { innerHTML: `${i}` }, li);
        if (i === this.currentPage) {
          domClass.add(li, 'active');
        } else {
          on(a, 'click', lang.hitch(this, function (page, evt) {
            evt.preventDefault();
            this.showPage(page);
          }, i));
        }
      }
      if (this.pageCount > this.currentPage + 3) {
        li = domConstruct.create('li', null, this.paginationNextLi, 'before');
        this._pageNodes.push(li);
        a = domConstruct.create('span', { innerHTML: '&hellip;' }, li);
      }
      domClass.toggle(this.paginationPreviousLi, 'disabled', this.currentPage === 1);
      domClass.toggle(this.paginationNextLi, 'disabled', this.currentPage === this.pageCount);
      if (this.nlsGenericBundle) {
        domAttr.set(this.paginationPreviousA, 'title', this.nlsGenericBundle.listPreviousPage);
        domAttr.set(this.paginationNextA, 'title', this.nlsGenericBundle.listNextPage);
      }
    },
    _clickPrevious(evt) {
      evt.preventDefault();
      this.showPage(this.currentPage - 1);
    },

    _clickNext(evt) {
      evt.preventDefault();
      this.showPage(this.currentPage + 1);
    },
    action_refresh() {
      this.searchTerm = domAttr.get(this.searchTermNode, 'value') || '';
      domStyle.set(this.resultSizeContainer, 'display', 'none');
      domClass.remove(this.searchBlockInner, 'has-error');
      domClass.remove(this.searchBlockInner, 'has-warning');
      domClass.remove(this.searchIconFeedback, 'fa-exclamation-triangle');
      domStyle.set(this.tooShortSearch, 'display', 'none');
      domStyle.set(this.invalidSearch, 'display', 'none');
      if (/["+~#()]/.test(this.searchTerm)) {
        domClass.add(this.searchBlockInner, 'has-error');
        domClass.add(this.searchIconFeedback, 'fa-exclamation-triangle');
        domStyle.set(this.invalidSearch, 'display', '');
      } else if (this.searchTerm.length < 3 && this.searchTerm.length !== 0) {
        domClass.add(this.searchBlockInner, 'has-warning');
        domClass.add(this.searchIconFeedback, 'fa-exclamation-triangle');
        domStyle.set(this.tooShortSearch, 'display', '');
      } else {
        this._enforceLimits();
        this.list.search({ sortOrder: this.sortOrder, term: this.searchTerm });
      }
    },
    selectRow(row) {
      this.selectedRow = row;
      domClass.add(row.domNode, 'selectedRow');
    },
    clearSelection() {
      if (this.selectedRow && this.selectedRow.domNode) {
        domClass.remove(this.selectedRow.domNode, 'selectedRow');
      }
      delete this.selectedRow;
    },
    clearSearch() {
      if (this.searchTerm) {
        domAttr.set(this.searchTermNode, 'value', '');
        this.action_refresh();
      }
    },
    clearView() {
      domAttr.set(this.searchTermNode, 'value', '');
      this.searchTerm = '';
      domClass.remove(this.searchBlockInner, 'has-error');
      domClass.remove(this.searchBlockInner, 'has-warning');
      domClass.remove(this.searchIconFeedback, 'fa-exclamation-triangle');
      domStyle.set(this.tooShortSearch, 'display', 'none');
      domStyle.set(this.invalidSearch, 'display', 'none');
    },
    /**
     * to get updated list size
     */
    updateListCount() {
    },
    removeAll() {
      const gb = this.list.nlsGenericBundle;
      const sb = this.list.nlsSpecificBundle;
      const removeConfirmMessage = sb[this.list.nlsRemoveAllEntires] ?
        sb[this.list.nlsRemoveAllEntires] : gb[this.list.nlsRemoveAllEntires];
      const removeFailedMessage = sb[this.list.nlsRemoveFailedKey] ?
        sb[this.list.nlsRemoveFailedKey] : gb[this.list.nlsRemoveFailedKey];
      const dialogs = defaults.get('dialogs');
      dialogs.confirm(removeConfirmMessage, null, null,
        lang.hitch(this, function (confirm) {
          if (confirm) {
            // filter only checked rows
            const totalRows = this.rows.concat(this.newRows);
            const checkedRows = [];
            totalRows.forEach((row) => {
              if (row.isChecked()) {
                checkedRows.push(row);
              }
            });
            all(array.map(checkedRows, row => row.entry.del())).then(() => {
              this.action_refresh();
              domStyle.set(this.btnAll, 'display', 'none');
              domProp.set(this.selectallCheck, 'checked', false);
            }, () => {
              dialogs.acknowledge(removeFailedMessage);
            }).then(() => {
              this.action_refresh();
            });
          }
        }));
    },
  }));
