define([
  'dojo/_base/declare',
  './BaseList',
  'entryscape-commons/create/typeIndex',
], (declare, BaseList, typeIndex) => declare([BaseList], {
  entitytype: '',
  emptyListWarningNLS: 'emptyListWarning',
  conf: null,

  loadEntityTypeConf() {
    if (this.entitytype != null && this.entitytype !== '') {
      this.conf = typeIndex.getConfByName(this.entitytype);
    }
  },

  getIconClass() {
    this.loadEntityTypeConf();
    if (this.conf && this.conf.faClass) {
      return this.conf.faClass;
    }

    return '';
  },

  getEmptyListWarning() {
    return this.NLSBundle1[this.emptyListWarningNLS];
  },
}));
