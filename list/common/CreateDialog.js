define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  './ListDialogMixin',
  '../../defaults',
  '../../rdforms/RDFormsEditDialog',
  'di18n/localize',
], (declare, lang, ListDialogMixin, defaults, RDFormsEditDialog, localize) =>
    /**
     * Dialog for creating new entries. Uses the lists entryType (if present) to set a type.
     * Uses the list specific bundle to get a title and button label via the keys "createHeader" and
     * "createButton" respectively.
     */
   declare([RDFormsEditDialog, ListDialogMixin], {
     explicitNLS: true,
     open() {
       this.inherited(arguments);
       this.list.getView().clearSearch();
       this.updateGenericCreateNLS();
       this._newEntry = defaults.get('createEntry')();
       const nds = this._newEntry;
       let et = this.list.entryType;
       et = lang.isArray(et) ? et[0] : et;
       if (et) {
         nds.getMetadata().add(nds.getResourceURI(), 'rdf:type', et, true);
       }
       this.editor.hideAddress = true;
       this.show(nds.getResourceURI(), nds.getMetadata(),
         this.list.getTemplate(), this.list.getTemplateLevel());
     },
     updateGenericCreateNLS() {
       const bundle = this.list.nlsSpecificBundle.createHeader ?
                this.list.nlsSpecificBundle : this.list.nlsGenericBundle;
       const name = this.list.getName();
       this.title = localize(bundle, 'createHeader', name);
       this.doneLabel = localize(bundle, 'createButton', name);
       this.updateTitleAndButton();
     },
     doneAction(graph) {
       return this._newEntry.setMetadata(graph).commit().then(lang.hitch(this, function (newEntry) {
         this.list.addRowForEntry(newEntry);
       }));
     },
   }));
