define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-attr',
  'dojo/fx',
  'entryscape-commons/list/EntryRow',
], (declare, domConstruct, domClass, domStyle, domAttr, fx, EntryRow) =>
    /** To use this EntryRow you must register a row action with the name expand, e.g.:
     *       this.registerRowAction({
     *           name: "expand",
     *           button: "link",
     *           iconType: "fa",
     *           icon: "chevron-right"
     *       });
     *
     *  You also need to implement the initExpandArea member function to initialize
     *  the expand area.
     */
   declare([EntryRow], {
     expandIcon: 'fa-chevron-down',
     unexpandIcon: 'fa-chevron-up',
     expandTitle: '',
     unexpandTitle: '',
     postCreate() {
       this.inherited(arguments);
       this.initExpandTitles();
       domAttr.set(this.buttons.expand.element, 'title', this.expandTitle);
     },
     initExpandTitles: function() {
     },
     action_expand() {
       const el = this.buttons.expand.element.firstChild;
       domClass.toggle(el, this.expandIcon);
       domClass.toggle(el, this.unexpandIcon);
       if (!this.details) {
         this.details = domConstruct.create('tr', null, this.rowNode, 'after');
         const td = domConstruct.create('td', {
           colspan: 4,
           style: {
             padding: '0px',
           },
         }, this.details);
         this.detailsContainer = domConstruct.create('div', { style: { display: 'none' } }, td);
         this.initExpandArea(this.detailsContainer);
       }
       if (domStyle.get(this.detailsContainer, 'display') === 'none') {
         domAttr.set(this.buttons.expand.element, 'title', this.unexpandTitle);
         fx.wipeIn({
           node: this.detailsContainer,
           duration: 300,
         }).play();
       } else {
         domAttr.set(this.buttons.expand.element, 'title', this.expandTitle);
         fx.wipeOut({
           node: this.detailsContainer,
           duration: 300,
         }).play();
       }
     },
     initExpandArea(/* node */) {
       // Override
     },
   }));
