define([
  'dojo/_base/declare',
  './ListDialogMixin',
  '../../contentview/ContentViewSideDialog',
], (declare, ListDialogMixin, ContentViewSideDialog) =>
  declare([ContentViewSideDialog, ListDialogMixin], {
    open(params) {
      this.inherited(arguments);
      const entry = params.row.entry;
      this.show(entry, this.list.getTemplate(entry), this.list.getEntityConfig(entry));
    },
  }));
