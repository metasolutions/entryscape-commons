define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/text!./DropdownRowTemplate.html',
  'jquery',
  'dojo/_base/array',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  '../EntryRow',
], (declare, lang, on, domConstruct, template, jquery,
    array, domAttr, domClass, domStyle, EntryRow) =>
  declare([EntryRow], {
    bid: 'escoDropdownRow',
    templateString: template,
    items: null,
    dropdownItems: [],
    // items: {},
    showCol1: false,

    buildRendering() {
      this.dropdownItems = [];
      this.inherited(arguments);
    },
    postCreate() {
      this.items = {};
      if (this.dropdownItems.length > 0) {
        this.showCol1 = true;
        jquery(this.dropdownRowMenu).dropdown();
        array.forEach(this.dropdownItems, lang.hitch(this, this.addItem));
        jquery(this.dropdownTest).on('shown.bs.dropdown', lang.hitch(this, function () {
          this.updateDropdown();
        }));
      }
      this.inherited(arguments);
    },
    updateDropdown() {

    },
    isDisabled(name) {
      const cssclass = domAttr.get(this.items[name].elementItem, 'class');
      return (cssclass === 'disabled');
    },
    disableDropdown() {
      domClass.add(this.dropdownRowMenu, 'disabled');
      domStyle.set(this.dropdownIcon, 'visibility', 'hidden');
      domClass.add(this.dropdownIcon, 'escoDropdownRow__menuDisabled');// set css
    },
    /* start*/
    setDropdownStatus(menuItem) {
      if (this.items[menuItem]) {
        const menuObj = this.items[menuItem].param;
        let icon = menuObj.icon;
        if (menuObj.iconType === 'fa') {
          icon = `fa fa-${icon}`;
        } else { // Default is glyphicons
          icon = `glyphicon glyphicon-${icon}`;
        }
        this.setDropdownStatusIcon(icon);
      }
    },
    setDropdownTitle(menuItem) {
      if (this.items[menuItem]) {
        const menuObj = this.items[menuItem].param;
        const title = this.list.nlsSpecificBundle[menuObj.nlsKeyTitle];
        this.setDropdownStatusTitle(title);
      }
    },
    /* end */
    setDropdownStatusIcon(icon) {
      // statusIcon
      const oldClass = domAttr.get(this.statusIcon, 'class');
      if (oldClass) {
        domClass.replace(this.statusIcon, icon, oldClass);
      } else {
        domClass.add(this.statusIcon, icon);
      }
    },
    setDropdownStatusTitle(title) {
      // statusIcon
      domAttr.set(this.dropdownRowMenu, 'title', title);
    },
    /**
     * this is useful when you change from one role to other
     * it disables new role and enables all other roles
     * sets title and icon to new role
     */
    changeDropdownStatus(name, title) {
      Object.keys(this.items).forEach((item) => {
        const obj = this.items[item];
        if ((!obj.param.disabled) || (obj.param.disabled && !obj.param.disabled)) {
          domClass.remove(obj.elementItem, 'disabled');
        }
      }, this);
      domClass.add(this.items[name].elementItem, 'disabled');
      // statusIcon
      this.setDropdownStatus(name);
      domAttr.set(this.dropdownRowMenu, 'title', title);
    },
    disableCurrentMenuItem(name) {
      domClass.add(this.items[name].elementItem, 'disabled');
    },
    enableCurrentMenuItem(name) {
      domClass.remove(this.items[name].elementItem, 'disabled');
    },
    addItem(paramParams) {
      const param = paramParams;
      const li = domConstruct.create('li', null, this.dropdownRowMenuList);
      const a = domConstruct.create('a', null, li);
      let cls;
      if (param.iconType === 'fa') {
        cls = `fa fa-${param.icon}`;
      } else { // Default is glyphicons
        cls = `glyphicon glyphicon-${param.icon}`;
      }
      cls = `${cls} pull-left escoDropdownRow__menuItemIcon`;
      domConstruct.create('i', { class: cls }, a);
      const label = domConstruct.create('span', { class: 'escoDropdownRow__menuItemText' }, a);
      if (param.method !== 'undefined') {
        on(a, 'click', lang.hitch(this, function (ev) {
          ev.stopPropagation();
          jquery(this.dropdownRowMenu).dropdown('toggle');
          param.method();
        }));
        param.method = lang.hitch(this, param.method);// removed undefined "event"
      }
      this.items[param.name] = { param, elementItem: li, elementLink: a, elementLabel: label };
      if (param.disabled) {
        domClass.add(li, 'disabled');
      }
      domClass.add(li, 'disabled');
    },
    updateLocaleStrings(generic, specific) {
      this.inherited('updateLocaleStrings', arguments);
      Object.keys(this.items).forEach((name) => {
        const obj = this.items[name];
        const labelKey = obj.param.nlsKey;
        const label = specific != null && specific[labelKey] != null ? specific[labelKey] : generic[labelKey] || '';
        domAttr.set(obj.elementLabel, 'innerHTML', `&nbsp;&nbsp;${label}`);
        const titleKey = obj.param.nlsKeyTitle;
        const title = specific != null && specific[titleKey] != null ? specific[titleKey] : generic[titleKey] || '';
        domAttr.set(obj.elementItem, 'title', title);
      }, this);
    },
    removeItems() {
      domConstruct.empty(this.dropdownRowMenu);
      this.items = {};
      this.dropdownItems = [];
    },
    registerDropdownItem(params) {
      this.dropdownItems.push(params);
    },
    removeItem(name) {
      const obj = this.items[name];
      domConstruct.destroy(obj.elementItem);
      delete this.items[name];
    },

  }));
