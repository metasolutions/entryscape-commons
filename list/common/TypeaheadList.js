define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-style',
  './BaseList',
  '../../query/Typeahead',
], (declare, lang, domStyle, BaseList, Typeahead) =>
  declare([BaseList], {
    includeCreateButton: false,
    includeRefreshButton: true,
    searchInList: false,
    searchVisibleFromStart: false,
    typeaheadClass: Typeahead,

    postCreate() {
      this.inherited('postCreate', arguments);
      domStyle.set(this.listView.headerContainerInner, 'display', 'none');
      domStyle.set(this.listView.searchBlockInner, 'display', 'none');
      domStyle.set(this.listView.typeaheadInput, 'display', '');
      domStyle.set(this.listView.lowerBlock, 'display', '');

      const Cls = this.typeaheadClass;
      this.typeahead = new Cls({}, this.getView().typeaheadInput);
      const methods = ['select', 'getLabel', 'getQuery', 'render', 'processEntries'];
      for (let i = 0; i < methods.length; i++) {
        const method = methods[i];
        if (this[`typeahead_${method}`]) {
          this.typeahead[method] = lang.hitch(this, this[`typeahead_${method}`]);
        }
      }
    },
    typeahead_select(entry) {
      this.typeahead.clear();
      this.addRowForEntry(entry);
    },
  }));
