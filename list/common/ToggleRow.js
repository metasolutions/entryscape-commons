define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'entryscape-commons/defaults',
  '../EntryRow',
  'dojo/text!./ToggleRowTemplate.html',
], (declare, lang, domAttr, domStyle, domClass, defaults, EntryRow, template) =>
  declare([EntryRow], {
    templateString: template,
    nlsPublicTitle: '',
    nlsProtectedTitle: '',
    showCol1: true,
    /**
     * Needs to be set in postCreate to correspond to correct value
     */
    isPublicToggle: false,
    /**
     * Weather it is allowed to toggle this row to public or not.
     */
    allowTogglePublic: true,

    setToggled(isEnabled, isPublic) {
      this.toggleEnabled = isEnabled;
      if (isEnabled) {
        domClass.remove(this.publicToggleNode, 'disabled');
        this.isPublicToggle = isPublic;
        if (isPublic) {
          domStyle.set(this.publicNode, 'display', '');
          domStyle.set(this.protectedNode, 'display', 'none');
        } else {
          domStyle.set(this.publicNode, 'display', 'none');
          domStyle.set(this.protectedNode, 'display', '');
        }
      }
    },

    updateLocaleStrings(generic, specific) {
      this.inherited('updateLocaleStrings', arguments);
      domAttr.set(this.publicNode, 'title', specific[this.nlsPublicTitle]);
      domAttr.set(this.protectedNode, 'title', specific[this.nlsProtectedTitle]);
    },

    toggle(ev) {
      if (!this.toggleEnabled) {
        return;
      }
      ev.stopPropagation();
      if (this.allowToggle() === false) {
        return;
      }
      this.toggleImpl(lang.hitch(this, this.setToggled, true, !this.isPublicToggle));
    },

    allowToggle() {
      return true;
    },
  }));
