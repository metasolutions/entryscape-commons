define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/aspect',
], (declare, lang, aspect) => declare([], {
  row: null,
  _firstOpenDialog: true,
  constructor(params) {
    this.list = params.list;
  },

  connectIfFirstDialogOpen() {
    if (this._firstOpenDialog) {
      this._firstOpenDialog = false;
      const obj = this.dialog || this;
      if (typeof obj.hide === 'function') {
        aspect.after(this.dialog || this, 'hide', lang.hitch(this, this.onDialogHide));
      }
    }
  },

  onDialogHide() {
    if (this.row && this.list) {
      this.list.getView().clearSelection();
    }
  },

  open(params) {
    this.connectIfFirstDialogOpen();
    if (typeof params === 'object' && params.row && this.list) {
      this.row = params.row;
      this.list.getView().selectRow(this.row);
    }
  },
}));
