define([], () => {
  const ResultSize = {
    view(vnode) {
      const { text } = vnode.attrs;
      return m('div.esco__resultSize', { title: `${text}` }, `${text}`);
    },
  };

  return ResultSize;
});
