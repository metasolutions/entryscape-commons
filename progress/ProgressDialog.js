define([
  'dojo/_base/declare',
  'dijit/_WidgetsInTemplateMixin',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./ProgressDialogTemplate.html',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'entryscape-commons/list/common/ListDialogMixin',
  '../defaults',
  'config',
  'dojo/_base/array',
  'rdforms/utils',
  'i18n!nls/escoProgress',
], (declare, _WidgetsInTemplateMixin, TitleDialog, template, on, domAttr, domStyle, domClass, lang,
    domConstruct, ListDialogMixin, defaults, config, array, utils) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin], {
    bid: 'escoProgress',
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['escoProgress'],
    nlsHeaderTitle: 'progressHeader',
    nlsFooterButtonLabel: 'progressFooterButton',
    __progressNode: null,
    __checkboxNode: null,

    postCreate() {
      this.inherited(arguments);
      this.getTaskNames();
      this.createProgress();
      this.createCheckBoxes();
    },
    open(params) {
      this.inherited(arguments);
      this.row = params.row;
      this.entry = params.row.entry;
      this.displayProgressBar();
      this.dialog.show();
    },
    localeChange() {
      this.inherited('localeChange', arguments);
      if (this.NLSBundles.escoProgress) {
        const mandatoryChecklistTitle = this.NLSBundles.escoProgress.mandatoryChecklistTitle;
        array.forEach(this.taskCheckBoxes, (taskCheckBox) => {
          if (taskCheckBox.mandatory) {
            const mandatoryIndicator = taskCheckBox.mandatory;
            domAttr.set(mandatoryIndicator, 'title', mandatoryChecklistTitle);
          }
        });
      }
    },
    displayProgressBar() {
      const self = this;
      const metadataTasks = [];
      this.noOfCompletedMandatoryTasks = 0;
      // var metadata = self.entry.getMetadata();
      const infoEntryGraph = self.entry.getEntryInfo().getGraph();
      // var tasks = metadata.find(self.entry.getResourceURI(), "http://entrystore.org/terms/progress");
      const tasks = infoEntryGraph.find(self.entry.getResourceURI(), 'http://entrystore.org/terms/progress');
      array.forEach(tasks, (task) => {
        metadataTasks.push(task.getObject().value);
      });
      array.forEach(this.configuredTasks, (configuredTask) => {
        if (metadataTasks.indexOf(configuredTask.name) !== -1) {
          if (configuredTask.mandatory) {
            this.noOfCompletedMandatoryTasks += 1;
          }
          self.setProgressBar(configuredTask.name, self.taskWidth, configuredTask.mandatory);
          self.setCheckBoxes(configuredTask.name, true);
        } else {
          self.setProgressBar(configuredTask.name, 0, configuredTask.mandatory);
          self.setCheckBoxes(configuredTask.name, false);
        }
      });
    },
    updateProgress(selectedName, isMandatory, event) {
      const target = event.target || event.srcElement;
      if (target.checked) {
        if (isMandatory) {
          this.noOfCompletedMandatoryTasks += 1;
        }
        this.setProgressBar(selectedName, this.taskWidth, isMandatory);
      } else {
        if (isMandatory) {
          this.noOfCompletedMandatoryTasks -= 1;
        }
        this.setProgressBar(selectedName, 0, isMandatory);
      }
    },
    getTaskNames() {
      this.configuredTasks = [];
      this.manadatoryTasks = [];
      const self = this;
      if (config.catalog && config.catalog.checklist) {
        const tasks = config.catalog.checklist;
        array.forEach(tasks, (task) => {
          if (task.mandatory) {
            self.manadatoryTasks.push({
              name: task.name,
              label: utils.getLocalizedValue(task.label).value,
              shortLabel: utils.getLocalizedValue(task.shortLabel).value,
              description: utils.getLocalizedValue(task.description).value,
              mandatory: task.mandatory,
            });
          }
          self.configuredTasks.push({
            name: task.name,
            label: utils.getLocalizedValue(task.label).value,
            shortLabel: utils.getLocalizedValue(task.shortLabel).value,
            description: utils.getLocalizedValue(task.description).value,
            mandatory: task.mandatory,
          });
        });
      }
    },
    createProgress() {
      const self = this;
      this.tasks = [];
      this.taskWidth = 100 / this.configuredTasks.length;
      array.forEach(this.configuredTasks, (configuredTask) => {
        const div = domConstruct.create('div', { class: 'progress-bar progress-bar-warning escoProgress__taskIndicator escoProgress__progressBar' }, self.__progressNode);
        domStyle.set(div, 'width', `${self.taskWidth}%`);
        domAttr.set(div, 'innerHTML', configuredTask.shortLabel);
        self.tasks.push({
          taskEl: div,
          taskName: configuredTask.name,
          taskLabel: configuredTask.label,
          taskShortLabel: configuredTask.shortLabel,
        });
      });
    },
    createCheckBoxes() {
      this.taskCheckBoxes = [];
      this.configuredTasks.forEach((configuredTask) => {
        const divPanel = domConstruct.create('div', {
          class: 'panel panel-default' +
          ' escoProgress__task',
        }, this.__checkboxNode);
        const heading = domConstruct.create('div', { class: 'checkbox panel-heading' +
        ' escoProgress__taskHeading' }, divPanel);
        const label = domConstruct.create('label', null, heading);
        const input = domConstruct.create('input', { type: 'checkbox' }, label);
        const task = domConstruct.create('span', {
          innerHTML: configuredTask.label,
          class: 'escoProgress__taskLabel',
        }, label);
        if (configuredTask.description) {
          domConstruct.create('div', {
            class: 'panel-body escoProgress__taskDescription',
            innerHTML: configuredTask.description,
          }, divPanel);
        }
        if (configuredTask.mandatory) {
          const mandatoryIndicator = domConstruct.create('i', { class: 'pull-right fa fa-exclamation-circle escoProgress__mandatorytask' }, heading);
          this.taskCheckBoxes.push({
            taskEl: input,
            taskNameEl: task,
            taskName: configuredTask.name,
            mandatory: mandatoryIndicator,
          });
        } else {
          this.taskCheckBoxes.push({
            taskEl: input,
            taskNameEl: task,
            taskName: configuredTask.name,
          });
        }
        const updateProgress = lang.hitch(
          this, this.updateProgress, configuredTask.name, configuredTask.mandatory);
        on(input, 'click', updateProgress);
      });
    },
    setProgressBar(selectedName, taskWidth) {
      this.tasks.forEach((task) => {
        // task is object
        if (task.taskName === selectedName) {
          const div = task.taskEl;
          domStyle.set(div, 'width', `${taskWidth}%`);
          domAttr.set(div, 'innerHTML', task.taskShortLabel);
          domAttr.set(div, 'title', task.taskLabel);
          if (taskWidth === 0) {
            domAttr.set(div, 'innerHTML', '');
          }
        }
      });
      this.tasks.forEach((task) => {
        const div = task.taskEl;
        if (this.noOfCompletedMandatoryTasks === this.manadatoryTasks.length) {
          domClass.remove(div, 'progress-bar progress-bar-warning escoProgress__taskIndicator escoProgress__progressBar');
          domClass.add(div, 'progress-bar progress-bar-success escoProgress__taskIndicator escoProgress__progressBar');
        } else {
          domClass.remove(div, 'progress-bar progress-bar-success escoProgress__taskIndicator escoProgress__progressBar');
          domClass.add(div, 'progress-bar progress-bar-warning escoProgress__taskIndicator escoProgress__progressBar');
        }
      });
    },
    setCheckBoxes(taskName, isChecked) {
      this.taskCheckBoxes.forEach((taskCB) => {
        // task is object
        if (taskCB.taskName === taskName) {
          const input = taskCB.taskEl;
          if (isChecked) {
            domAttr.set(input, 'checked', true);
          } else {
            domAttr.set(input, 'checked', false);
          }
        }
      });
    },
    footerButtonAction() {
      const infoEntryGraph = this.entry.getEntryInfo().getGraph();
      const tasksCheckBoxes = this.taskCheckBoxes;
      tasksCheckBoxes.forEach((taskCB) => {
        const input = taskCB.taskEl;
        if (domAttr.get(input, 'checked')) {
          infoEntryGraph.addL(this.entry.getResourceURI(), 'http://entrystore.org/terms/progress', taskCB.taskName);
        } else {
          infoEntryGraph.findAndRemove(this.entry.getResourceURI(), 'http://entrystore.org/terms/progress', {
            type: 'literal',
            value: taskCB.taskName,
          });
        }
      });
      return this.entry.getEntryInfo().commit()
        .then(lang.hitch(this.list, this.list.rowMetadataUpdated, this.row));
    },
  }));
