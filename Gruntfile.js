module.exports = function (grunt) {
  grunt.task.loadTasks('node_modules/entryscape-js/tasks');

  grunt.config.merge({
    poeditor: {
      projectids: ['94867'],
    },
    nls: {
      langs: ['en', 'sv', 'da', 'de'],
    },
    update: {
      libs: [
        'di18n',
        'spa',
        'rdfjson',
        'rdforms',
        'store',
      ],
    },
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-available-tasks');
};
