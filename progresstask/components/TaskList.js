define([
  './Task',
  'mithril',
], (Task, m) => {
  const TaskList = {
    bid: 'escoProgressTask',
    view(vnode) {
      const { tasks } = vnode.attrs;
      return m('.progress', { class: `${this.bid}__progressbarDiv` }, tasks.map(task => m(Task, { task })));
    },
  };

  return TaskList;
});

