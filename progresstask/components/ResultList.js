define([
  './Result',
  'mithril',
], (Result, m) => {
  const ResultList = {
    view(vnode) {
      const { tasks } = vnode.attrs;

      return m('ul.list-group', tasks.map(result => m(Result, { result })));
    },
  };

  return ResultList;
});
