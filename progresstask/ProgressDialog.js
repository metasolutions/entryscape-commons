define([
  'dojo/_base/declare',
  'dojo/text!./ProgressDialogTemplate.html',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/_base/lang',
  'dojo/dom-construct',
  '../defaults',
  'store/promiseUtil',
  'config',
  'jquery',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'mithril',
  'di18n/localize',
], (declare, template, domAttr, domStyle, lang, domConstruct, defaults, promiseUtil, config, jquery,
    _WidgetBase, _TemplatedMixin, m) =>
  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    maxWidth: 800,
    postCreate() {
      this.inherited(arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
    },
    /**
     * Show a sticky modal and return the modal node
     * @return {*}
     */
    show() {
      jquery(this.domNode).modal({
        keyboard: false,
        backdrop: 'static',
      });
      jquery(this.domNode).modal('show');
      this._showing = true;
      return this.getModalBody();
    },
    getModalFooter() {
      return this._modalFooter;
    },
    getModalBody() {
      return this._modalBody;
    },
    hide() {
      this.clear();
      jquery(this.domNode).modal('hide');
    },
    clear() {
      m.render(this._modalBody, null);
      m.render(this._modalFooter, null);
    },
  }));
