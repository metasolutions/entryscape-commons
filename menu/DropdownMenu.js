define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./DropdownMenuTemplate.html',
  'jquery',
  'dojo/_base/array',
  'dojo/dom-class',
  'bootstrap/dropdown',
], (declare, lang, on, domConstruct, domAttr, _WidgetBase, _TemplatedMixin, template, jquery, array,
    domClass) => {

  jquery(document).on("show.bs.dropdown", ".dropdown", function () {
    // calculate the required sizes, spaces
    var $ul = jquery(this).children(".dropdown-menu");
    var menuHeight = $ul.height()
    var buttonHeight = jquery(this).children(".dropdown-toggle").height();
    var scrollHeight = $ul.parent().offsetParent().height();
    var menuAnchorTop = $ul.parent().position().top + buttonHeight;

    // how much space would be left on the top if the dropdown opened that direction
    var spaceUp = (menuAnchorTop - buttonHeight - menuHeight);
    // how much space is left at the bottom
    var spaceDown = scrollHeight - (menuAnchorTop + menuHeight);
    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
      jquery(this).addClass("dropup");
  }).on("hidden.bs.dropdown", ".dropdown", function() {
    // always reset after close
    jquery(this).removeClass("dropup");
  });

  return declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    items: null,
    labelHeight: false,
    inHeader: false, // where is the dropdown placed
    postCreate() {
      if (this.labelHeight === true) {
        domClass.add(this.domNode, 'labelHeight');
      }
      if (this.inHeader) {
        domClass.add(this.dropdownButtonNode, 'btn-raised');
      }
      jquery(this.dropdownButtonNode).dropdown();
      this.items = {};
      this.inherited(arguments);
    },
    addItems(params) {
      array.forEach(params, this.addItem, this);
    },
    addItem(param) {
      const li = domConstruct.create('li', null, this.dropdownMenuNode);
      const a = domConstruct.create('a', null, li);
      let cls;
      if (param.iconType === 'fa') {
        cls = `fa fa-fw fa-${param.icon}`;
      } else { // Default is glyphicons
        cls = `glyphicon glyphicon-${param.icon}`;
      }
      domConstruct.create('i', { class: cls }, a);
      const label = domConstruct.create('span', null, a);
      on(a, 'click', lang.hitch(this, function (ev) {
        ev.stopPropagation();
        jquery(this.dropdownButtonNode).dropdown('toggle');
        param.method();
      }));
      this.items[param.name] = { param, elementItem: li, elementLink: a, elementLabel: label };
    },
    updateLocaleStrings(generic, specific) {
      Object.keys(this.items).forEach((name) => {
        const obj = this.items[name];
        const labelKey = obj.param.nlsKey;
        const label = specific != null && specific[labelKey] != null ? specific[labelKey] : generic[labelKey] || '';
        domAttr.set(obj.elementLabel, 'innerHTML', `&nbsp;&nbsp;${label}`);
        const titleKey = obj.param.nlsKeyTitle;
        const title = specific != null && specific[titleKey] != null ? specific[titleKey] : generic[titleKey] || '';
        domAttr.set(obj.elementItem, 'title', title);
      });
    },
    removeItems() {
      domConstruct.empty(this.dropdownMenuNode);
    },
    enforceLimits(limit, searchTerm) {
      Object.keys(this.items).forEach((name) => {
        const iconf = this.items[name];
        const maxLimit = iconf.param.max;
        if (iconf.param.disableOnSearch && searchTerm != null && searchTerm.length > 0) {
          domAttr.set(iconf.elementLink, 'disabled', 'disabled');
        } else if (parseInt(maxLimit, 10) === maxLimit && maxLimit <= limit && maxLimit !== -1) {
          domAttr.set(iconf.elementLink, 'disabled', 'disabled');
        } else {
          domAttr.remove(iconf.elementLink, 'disabled');
        }
      });
    },
  });
});
