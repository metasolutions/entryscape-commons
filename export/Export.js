define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  'jquery',
  'dijit/_WidgetsInTemplateMixin',
  'entryscape-commons/list/common/ListDialogMixin',
  'di18n/NLSMixin',
  'dojo/text!./ExportTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
], (declare, domAttr, domClass, domStyle, jquery, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin, template, TitleDialog) =>
  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    format: '',
    includeFooter: false,
    nlsExportText: 'exportText',
    nlsDownloadButton: 'downloadButton',
    // nlsDownloadLabel: null,
    nlsFormatSelect: 'exportFormatSelect',

    localeChange() {
      const bundle = this.NLSBundles[this.nlsBundles[0]];
      domAttr.set(this.exportText, 'innerHTML', bundle[this.nlsExportText]);
      domAttr.set(this.downloadButton, 'innerHTML', bundle[this.nlsDownloadButton]);
      domAttr.set(this.exportFormatSelect, 'innerHTML', bundle[this.nlsFormatSelect]);
      // if (this.downloadLabel) {
      //  domAttr.set(this.downloadLabel, 'innerHTML', bundle[this.nlsDownloadLabel]);
      //  domStyle.set(this.downloadLabel, 'display', '');
      // }
      this.inherited(arguments);
    },

    rdfxmlClick(e) {
      // This is the default from EntryStore, optionally we could set application/rdf+xml
      this.format = '';
      this.update(e);
    },

    turtleClick(e) {
      this.format = 'text/turtle';
      this.update(e);
    },

    ntriplesClick(e) {
      this.format = 'text/n-triples';
      this.update(e);
    },

    jsonldClick(e) {
      this.format = 'application/ld+json';
      this.update(e);
    },

    downloadClick() {
      const mdUri = `${this.entry.getEntryInfo().getMetadataURI()}?recursive=${this.profile}&download`;
      const format = this.format !== '' ? `&format=${this.format}` : '';
      window.open(mdUri + format, '_blank');
    },

    update(e) {
      const mdUri = this.entry.getEntryInfo().getMetadataURI();
      const format = this.format !== '' ? `&format=${this.format}` : '';
      domAttr.set(this.exportUrl, 'value', `${mdUri}?recursive=${this.profile}${format}`);
      if (e) {
        this.updateUI(e);
      }
    },

    updateUI(e) {
      const activeA = jquery('.download-formats a.active');
      if (activeA.length > 0) {
        domClass.remove(activeA[0], 'active');
      }

      domClass.add(e.currentTarget, 'active');
    },

    open(params) {
      this.entry = params.row.entry;
      this.update();
      this.dialog.show();
    },
  }));
