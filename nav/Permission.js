define([
  'config',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  '../defaults',
  'di18n/NLSMixin',
  'dojo/text!./PermissionTemplate.html',
  'bootstrap/collapse',
  'i18n!nls/escoSignin',
], (config, declare, lang, domAttr, _WidgetBase, _TemplatedMixin, defaults, NLSMixin, template) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    bid: 'escoPermission',
    nlsBundles: ['escoPermission'],
    templateString: template,
    maxWidth: 800,
    show() {
      const site = defaults.get('siteManager');
      domAttr.set(this.__signinLink, 'href', site.getViewPath(config.site.signinView));
    },
  }));
