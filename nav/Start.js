define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-style',
  'dojo/on',
  'config',
  'dojo/dom-attr',
  'dojo/dom-construct',
  './utils',
  'rdforms/utils',
  './Cards',
  '../defaults',
  '../view/ViewMixin',
  'di18n/NLSMixin',
  'i18n!nls/escoModules',
  'i18n!nls/escoLayout',
], (declare, lang, domStyle, on, config, domAttr, domConstruct, utils, rdformsUtils, Cards,
    defaults, ViewMixin, NLSMixin) =>

  declare([Cards, ViewMixin, NLSMixin.Dijit], {
    nlsBundles: ['escoModules', 'escoLayout'],

    postCreate() {
      this.inherited(arguments);
      defaults.onChange('hasAdminRights', info => {
        this.show();
      });

      defaults.onChange('userEntryInfo', lang.hitch(this, (info) => {
        if (this.signInNode) {
          this.updateBannerButtonsVisibility(info.entryId === '_guest');
        }
      }));
    },

    updateBannerButtonsVisibility(visible) {
      if (visible) {
        domStyle.set(this.signInNode, 'display', '');
      } else {
        domStyle.set(this.signInNode, 'display', 'none');
      }
    },

    show() {
      this.render();
    },
    localeChange() {
      if (this.signInNode) {
        domAttr.set(this.signInNode, 'innerHTML', this.NLSBundle1.signInFromBanner);
      }
      this.show();
    },

    getBannerNode() {
      let node;
      if (config.theme && config.theme.startBanner) {
        const sb = config.theme.startBanner;
        let txt;
        node = domConstruct.create('div', { class: 'square-service-block entryscape-intro' });
        if (sb.header) {
          txt = rdformsUtils.getLocalizedValue(sb.header).value;
          domConstruct.create('h2', { innerHTML: txt }, node);
        }
        if (sb.icon) {
          domConstruct.create('img', { style: { 'margin-bottom': '30px' }, src: sb.icon }, node);
        }
        if (sb.text) {
          txt = rdformsUtils.getLocalizedValue(sb.text).value;
          domConstruct.create('p', { innerHTML: txt, class: 'font-size-large' }, node);
          if (sb.details) {
            // Filler button for right height.
            domConstruct.create('button', {
              class: 'btn btn-success',
              innerHTML: 'F',
              style: { visibility: 'hidden' },
            }, node);

            const de = sb.details;
            txt = rdformsUtils.getLocalizedValue(de.buttonLabel).value;
            const but = domConstruct.create('button', {
              class: 'btn btn-raised btn-default pull-right',
              innerHTML: txt,
            }, node);

            txt = rdformsUtils.getLocalizedValue(de.header).value;
            on(but, 'click', () => {
              defaults.get('dialogs').acknowledgeText(de.path, txt);
            });
          }
          this.signInNode = domConstruct.create('button',
            { class: 'btn btn-raised btn-success pull-right' }, node);
          if (this.NLSBundle1) {
            domAttr.set(this.signInNode, 'innerHTML',
              this.NLSBundle1.signInFromBanner);
          }
          on(this.signInNode, 'click', () => {
            const spa = defaults.get('siteManager');
            // remove other params, only view should be passed.
            spa.render(config.site.signinView || 'signin', {});
//              defaults.get('signInDialog').show();
          });
          /* on(this.signUpNode, 'click', () => {
               defaults.get('signUpDialog').show();
             });*/
          const uei = defaults.get('userEntryInfo');
          this.updateBannerButtonsVisibility(uei == null || uei.entryId === '_guest');
        }
      } else {
        node = domConstruct.create('div');
      }
      return node;
    },

    getBannerInfo() {

    },

    /**
     * Essentially maps the modules Map to an Array of module definitions
     * @return {Array}
     */
    getCards() {
      const site = defaults.get('siteManager');
      return Array.from(site.modules).map(([, module]) => module);
    },

    async getLabelAndTooltip(card) {
      let tooltip;
      const pt = card.productName;

      // wait for the nls bundle to load
      await this.localeReady;

      const label = utils.getModuleProp(card, this.NLSBundle0, 'title');
      if (label) {
        tooltip = `EntryScape ${pt}`;
      } else {
        tooltip = utils.getModuleProp(card, this.NLSBundle0, 'tooltip');
      }

      return { label, tooltip };
    },

    async setLabelAndTooltip(labelNode, tooltipNode, card) {
      const { label, tooltip } = await this.getLabelAndTooltip(card);

      domAttr.set(labelNode, 'innerHTML', label);
      domAttr.set(tooltipNode, 'title', tooltip);
    },

    getText(card) {
      return utils.getModuleProp(card, null, 'text', true);
    },
    setText(textNode, card) {
      const text = this.getText(card);
      domAttr.set(textNode, 'innerHTML', text || '');
    },
  }));
