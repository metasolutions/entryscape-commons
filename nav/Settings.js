define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/topic',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/promise/all',
  'di18n/NLSMixin',
  'di18n/locale',
  '../defaults',
  'config',
  'entryscape-commons/dialog/TitleDialog', // In template
  'entryscape-commons/auth/components/PasswordForm',
  'entryscape-commons/auth/Password',
  'dojo/text!./SettingsTemplate.html',
  'dojo/dom-style',
  'i18n!nls/escoLayout',
], (declare, lang, topic, on, domAttr, domConstruct, domClass, all, NLSMixin, locale, defaults,
    config, TitleDialog, PasswordForm, Password, template, domStyle) =>

  declare([TitleDialog.ContentNLS, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escoLayout'],
    nlsHeaderTitle: 'settingsHeader',
    nlsFooterButtonTitle: 'saveSettings',
    nlsFooterButtonLabel: 'saveSettings',

    postCreate() {
      this.inherited('postCreate', arguments);
      domConstruct.create('option', { value: '' }, this.settingsLanguage);
      const sortedSuppLangs = this.sortLanguages(config.locale.supported);
      sortedSuppLangs.forEach((l) => {
        domConstruct.create('option', {
          value: l.lang,
          innerHTML: l.label,
        }, this.settingsLanguage);
      });
      const update = this.updateFooterButton.bind(this);
      on(this.settingsFirstname, 'keyup', update);
      on(this.settingsLastname, 'keyup', update);
      on(this.settingsLanguage, 'change', update);
    },

    show() {
      this.dialog.show();
      Password.clear();
      m.mount(this.passwordFormNode, PasswordForm('settings', this.updateFooterButton.bind(this)));
      domAttr.set(this.settingsFormStatus, 'innerHTML', '');
      const info = defaults.get('userEntryInfo');
      const customProperties = defaults.get('userEntry').getResource(true).getCustomProperties();
      if (info) {
        domAttr.set(this.settingsUsername, 'value', info.user);
        domAttr.set(this.settingsLastname, 'value', info.lastName || '');
        domAttr.set(this.settingsFirstname, 'value', info.firstName || '');
        domAttr.set(this.settingsLanguage, 'value', info.language || '');
        if (config && config.site && config.site.nationalIdNumber) {
          domStyle.set(this.nationalNumberNode, 'display', '');
          domAttr.set(this.settingNationalNumber, 'value', customProperties.nationalid || '');
        }
      } else {
        const dialogs = defaults.get('dialogs');
        dialogs.acknowledge(this.nlsBundle0.noSettingsError);
      }
      this.dialog.lockFooterButton();
    },
    formReset() {
      this.settingsForm.reset();
    },
    showSettingsError(message) {
      const oldStatus = domAttr.get(this.settingsFormStatus, 'innerHTML');
      domAttr.set(this.settingsFormStatus, 'innerHTML', `${oldStatus}<div class="alert alert-danger" role="alert">${message}</div>`);
    },

    /**
     * Sorts the languages based on its label.
     * @param supportedLangNames
     * @returns {Array}
     */
    sortLanguages(supportedLangNames) {
      const label2langNames = supportedLangNames.map(supportedLang => ({
        lang: supportedLang,
        label: supportedLang.label.toLowerCase(),
      }));
      // sort by label
      label2langNames.sort((a, b) => {
        if (a.label < b.label) {
          return -1;
        } else if (a.label > b.label) {
          return 1;
        }
        return 0;
      });
      return label2langNames.map(label2langName => label2langName.lang);
    },
    updateFooterButton() {
      let firstName = domAttr.get(this.settingsFirstname, 'value').trim();
      let lastName = domAttr.get(this.settingsLastname, 'value').trim();
      const language = domAttr.get(this.settingsLanguage, 'value');

      if (Password.canSubmit() && firstName.length > 0
        && lastName.length > 0 && language.length > 0) {
        this.dialog.unlockFooterButton();
      } else {
        this.dialog.lockFooterButton();
      }
    },
    footerButtonAction() {
      const bundle = this.NLSBundle0;
      domAttr.set(this.settingsFormStatus, 'innerHTML', '');

      let firstName = domAttr.get(this.settingsFirstname, 'value').trim();
      let lastName = domAttr.get(this.settingsLastname, 'value').trim();
      const language = domAttr.get(this.settingsLanguage, 'value');

      if (firstName.length === 0) {
        return bundle.firstNameMissing;
      }

      if (lastName.length === 0) {
        return bundle.lastNameMissing;
      }

      const auth = defaults.get('entrystore').getAuth();
      const saveOps = [];

      if (Password.provided()) {
        saveOps.push(auth.getUserEntry()
          .then(userEntry => userEntry.getResource(true).setPassword(Password.password), () => {
            failureOnSaveSettings = true;
            return bundle.passwordSaveError;
          }));
      }

      if (language.length === 2) {
        saveOps.push(auth.getUserEntry()
          .then(userEntry => userEntry.getResource(true).setLanguage(language)), () => {
          failureOnSaveSettings = true;
          return bundle.languageSaveError;
        });
        locale.set(language);
      }

      saveOps.push(auth.getUserEntry().then((userEntry) => {
        firstName = domAttr.get(this.settingsFirstname, 'value');
        lastName = domAttr.get(this.settingsLastname, 'value');

        if (firstName && lastName && (firstName.trim().length > 0)
          && (lastName.trim().length > 0)) {
          const graph = userEntry.getMetadata();

          graph.findAndRemove(null, 'http://xmlns.com/foaf/0.1/firstName', null);
          graph.findAndRemove(null, 'http://xmlns.com/foaf/0.1/givenName', null);
          graph.add(userEntry.getResourceURI(),
            'http://xmlns.com/foaf/0.1/givenName',
            { type: 'literal', value: firstName.trim() });

          graph.findAndRemove(null, 'http://xmlns.com/foaf/0.1/lastName', null);
          graph.findAndRemove(null, 'http://xmlns.com/foaf/0.1/familyName', null);
          graph.add(userEntry.getResourceURI(),
            'http://xmlns.com/foaf/0.1/familyName',
            { type: 'literal', value: lastName.trim() });

          userEntry.commitMetadata().then(() => {
            defaults.set('userEntry', userEntry);
          });
        }
      }, () => {
        failureOnSaveSettings = true;
        return bundle.settingsSaveError;
      }));

      return all(saveOps).then(() => {
        this.formReset();
        topic.publish('dcatmanager/user');
      }, () => {
        topic.publish('dcatmanager/user');
        return bundle.settingsSaveError;
      });
    },
  }));
