define([
  'require',
  'config',
  'dojo/on',
  'dojo/string',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/html',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/locale',
  '../defaults',
  'dojo/text!./SignupTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/json',
  'entryscape-commons/nav/signinUtils',
  'entryscape-commons/auth/Password',
  'entryscape-commons/auth/components/PasswordForm',
  'bootstrap/collapse',
  'i18n!nls/escoSignin',
], (require, config, on, string, declare, lang, domAttr, domStyle, domConstruct, html,
    _WidgetsInTemplateMixin, locale, defaults, template, TitleDialog, json, signinUtils,
    Password, PasswordForm) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin], {
    templateString: template,
    nlsBundles: ['escoSignin'],
    nlsHeaderTitle: 'createAccountHeader',
    nlsFooterButtonLabel: 'createAccount',
    maxWidth: 800,

    footerButtonAction() {
      this.setStatus(this.signupStatus);
      const nationalNumber = domAttr.get(this.nationalNumber, 'value');
      /*if (!this.validatePassword()) {
        return undefined;
      } */
      if (!this.validateNationalId()) {
        return undefined;
      }
      if (!Password.canSubmit()) {
        return undefined;
      }
      const es = defaults.get('entrystore');
      const sm = defaults.get('siteManager');
      const params = lang.clone(sm.getUpcomingOrCurrentParams());
      params.view = 'signin'; // TODO define in site config and use from there instead
      params.nextView = 'start'; // TODO define in site config and use from there instead
      let urlsuccess = window.location.href;
      const fragment = urlsuccess.indexOf('#');
      urlsuccess = (fragment >= 0 ? urlsuccess.substr(0, fragment) : urlsuccess) + sm.getViewPath('signin', params);
      const signupInfo = {
        firstname: domAttr.get(this.suFirstname, 'value'),
        lastname: domAttr.get(this.suLastname, 'value'),
        email: domAttr.get(this.suUsername, 'value'),
        password: Password.password,
        urlsuccess,
        grecaptcharesponse: this.recaptchaResponse,
      };
      if (config != null && config.site != null && config.site.nationalIdNumber) {
        signupInfo.custom_nationalid = nationalNumber;
      }
      const b = this.NLSBundle0;
      return es.getREST()
        .post(`${es.getBaseURI()}auth/signup`, json.stringify(signupInfo)).then(() => defaults.get('dialogs').acknowledge(b.signupConfirmationMessage), (err) => {
          if (err.response.status === 417) {
            throw string.substitute(b.signupWrongDomain,
              { domain: signupInfo.email.substr(signupInfo.email.indexOf('@') + 1) });
          } else {
            throw b.signupErrorMessage;
          }
        });
    },
    postCreate() {
      this.inherited('postCreate', arguments);
      if (config != null && config.site != null && config.site.nationalIdNumber) {
        domStyle.set(this.nationalNumberNode, 'display', '');
        domAttr.set(this.nationalNumber, 'required', 'required');
      }
      this.dialog.lockFooterButton();
      const check = lang.hitch(this, this.check);
      on(this.domNode, 'keyup', check);
      on(this.checkboxPUL, 'change', check);
    },

    check() {
      if (this.checkTimer) {
        clearTimeout(this.checkTimer);
        delete this.checkTimer;
      }

      this.checkTimer = setTimeout(lang.hitch(this, function () {
        delete this.checkTimer;
        let valid = true;
        if (lang.isFunction(this.domNode.checkValidity)) {
          valid = this.domNode.checkValidity();
        }
        if (this.recaptchaResponse == null) {
          valid = false;
        }
        if (!Password.canSubmit()) {
          valid = false;
        }
        this.validateNationalId();
        this.validateFirstName();
        this.validateEmail();

        if (valid) {
          this.dialog.unlockFooterButton();
        } else {
          this.dialog.lockFooterButton();
        }
      }), 400);
    },
    setStatus(node, message) {
      if (message) {
        domStyle.set(node, 'display', '');
        html.set(node, message);
      } else {
        domStyle.set(node, 'display', 'none');
      }
    },
    showPULInfoDialog() {
      let themepath = 'entryscape-commons/theme/';
      if (config.theme && config.theme.privacyLink) {
        window.open(config.theme.privacyLink, '_blank');
        return;
      }
      if (config.theme && config.theme.staticHTML) {
        themepath = 'statichtml/';
      } else if (config.theme && config.theme.localTheme && config.theme.localHTML) {
        themepath = 'theme/';
      }
      defaults.get('dialogs').acknowledgeText(`${themepath}privacy_${defaults.get('locale')}`, this.NLSBundle0.aboutPrivacyHeader);
    },
    validateFirstName() {
      if (domAttr.get(this.suFirstname, 'value').length === 1) {
        this.setStatus(this.firstnameStatus, this.NLSBundle0.signupToShortName);
      } else {
        this.setStatus(this.firstnameStatus);
      }
    },
    validateEmail() {
      if (domAttr.get(this.suUsername, 'value').length > 0 &&
        lang.isFunction(this.suUsername.checkValidity) && !this.suUsername.checkValidity()) {
        this.setStatus(this.emailStatus, this.NLSBundle0.signupInvalidEmail);
      } else {
        this.setStatus(this.emailStatus);
      }
    },
    /* validatePassword() {
      this.setStatus(this.passwordConfirmStatus);
      this.setStatus(this.passwordStatus);
      const newPassword = domAttr.get(this.suPassword, 'value');
      const confirmPassword = domAttr.get(this.confirmPassword, 'value');
      if (newPassword === '') {
        return undefined;
      }
      if (newPassword.length > 0) {
        if (newPassword.length < 8) {
          this.setStatus(this.passwordStatus, this.NLSBundle0.signupToShortPassword);
        }
        if (confirmPassword !== '' && newPassword !== confirmPassword) {
          this.setStatus(this.passwordConfirmStatus, this.NLSBundle0.passwordMismatch);
          return false;
        }
        return true;
      }
      return undefined;
    }, */
    validateNationalId() {
      this.setStatus(this.nationalNumberStatus);
      const nationalNumber = domAttr.get(this.nationalNumber, 'value');
      if (nationalNumber.length > 0
        && config != null && config.site != null && config.site.nationalIdNumber) {
        if (typeof config.site.nationalIdNumber === 'string'
          && signinUtils[config.site.nationalIdNumber]) {
          if (!signinUtils[config.site.nationalIdNumber](nationalNumber)) {
            mesg = this.NLSBundle0.nationalNumberError;
            this.setStatus(this.nationalNumberStatus, mesg);
            return false;
          }
          return true;
        }
      }
      return true;
    },
    show() {
      Password.clear();
      m.mount(this.passwordFormNode, PasswordForm('signup', this.check.bind(this)));
      this.check();
      defaults.get('addRecaptcha')(this.recaptcha, lang.hitch(this, (val) => {
        this.recaptchaResponse = val;
        this.check();
      }), lang.hitch(this, () => {
        this.recaptchaResponse = null;
        this.check();
      }));
      this.dialog.show();
    },
  }));
