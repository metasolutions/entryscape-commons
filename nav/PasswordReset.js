define([
  'config',
  '../util/configUtil',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/html',
  'dijit/_WidgetsInTemplateMixin',
  '../defaults',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/auth/Password',
  'entryscape-commons/auth/components/PasswordForm',
  'dojo/text!./PasswordResetTemplate.html',
  'dojo/json',
  'bootstrap/collapse',
  'i18n!nls/escoSignin',
], (config, configUtil, declare, lang, on, domAttr, domStyle, html, _WidgetsInTemplateMixin,
    defaults, TitleDialog, Password, PasswordForm, template, json) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin], {
    templateString: template,
    nlsBundles: ['escoSignin'],
    nlsHeaderTitle: 'resetPasswordHeader',
    nlsFooterButtonLabel: 'resetPassword',
    maxWidth: 800,

    footerButtonAction() {
      const es = defaults.get('entrystore');
      const sm = defaults.get('siteManager');
      const successUrl = configUtil.getBaseUrl() + sm.getViewPath(config.site.signinView);
      const pwResetInfo = {
        email: domAttr.get(this.pwrUsername, 'value'),
        password: Password.password,
        urlsuccess: successUrl,
        grecaptcharesponse: this.recaptchaResponse,
      };
      return es.getREST().post(`${es.getBaseURI()}auth/pwreset`, json.stringify(pwResetInfo))
        .then(() => {
          return defaults.get('dialogs')
            .acknowledge(this.NLSBundle0.passwordResetConfirmationMessage);
        }, () => {
          throw this.NLSBundle0.passwordResetErrorMessage;
        });
    },
    show() {
      Password.clear();
      m.mount(this.passwordFormNode, PasswordForm('reset', this.check.bind(this)));
      this.check();
      defaults.get('addRecaptcha')(this.recaptcha, lang.hitch(this, function (val) {
        this.recaptchaResponse = val;
        this.check();
      }), lang.hitch(this, function () {
        this.recaptchaResponse = null;
        this.check();
      }));
      this.dialog.show();
    },
    postCreate() {
      this.inherited(arguments);
      on(this.domNode, 'keyup', lang.hitch(this, this.check));
    },

    check() {
      if (this.checkTimer) {
        clearTimeout(this.checkTimer);
        delete this.checkTimer;
      }

      this.checkTimer = setTimeout(lang.hitch(this, function () {
        delete this.checkTimer;
        let valid = true;
        if (lang.isFunction(this.domNode.checkValidity)) {
          valid = this.domNode.checkValidity();
        }
        if (this.recaptchaResponse == null) {
          valid = false;
        }

        this.validateEmail();
        if (!Password.canSubmit()) {
          valid = false;
        }

        if (valid) {
          this.dialog.unlockFooterButton();
        } else {
          this.dialog.lockFooterButton();
        }
      }), 400);
    },
    validateEmail() {
      if (domAttr.get(this.pwrUsername, 'value').length > 0 &&
        lang.isFunction(this.pwrUsername.checkValidity) && !this.pwrUsername.checkValidity()) {
        this.setStatus(this.emailStatus, this.NLSBundle0.signupInvalidEmail);
      } else {
        this.setStatus(this.emailStatus);
      }
    },
    setStatus(node, message) {
      if (message) {
        domStyle.set(node, 'display', '');
        html.set(node, message);
      } else {
        domStyle.set(node, 'display', 'none');
      }
    },
  }));
