define([
  'dojo/dom-attr',
  'dojo/Deferred',
  'rdforms/utils',
  '../defaults',
], (domAttr, Deferred, utils, defaults) => {
  const _getSubViewsAsList = (node, arr) => {
    if (node.subViews != null) {
      for (let i = 0; i < node.subViews.length; i++) {
        const sv = node.subViews[i];
        if (typeof sv === 'string') {
          arr.push(sv);
        } else {
          arr.push(sv.view);
          _getSubViewsAsList(sv, arr);
        }
      }
    }
    return arr;
  };

  const _getSubViews = (node, viewName) => {
    if (node.view === viewName) {
      return _getSubViewsAsList(node, []);
    }
    if (node.subViews != null) {
      for (let i = 0; i < node.subViews.length; i++) {
        const res = _getSubViews(node.subViews[i], viewName);
        if (res != null) {
          return res;
        }
      }
    }

    return null;
  };

  const getSubViews = (view) => {
    const sm = defaults.get('siteManager');
    const config = sm.getConfig();
    const views = [];
    for (let i = 0; i < config.views.length; i++) {
      if (config.views[i].parent === view) {
        views.push(config.views[i].name);
      }
    }
    return views;
  };

  const navutils = {
    getModuleProp(module, bundle, prop, allowEmpty) {
      const mn = module.name;
      if (bundle && bundle[`${mn}-${prop}`]) {
        return bundle[`${mn}-${prop}`];
      } else if (module[prop]) {
        return utils.getLocalizedValue(module[prop]).value;
      } else if (allowEmpty !== true) {
        return mn;
      }

      return null;
    },
    getViewProp(view, prop) {
      if (view[prop]) {
        return utils.getLocalizedValue(view[prop]).value;
      }
      return view.name;
    },
    getSubViewDefs(view) {
      const sm = defaults.get('siteManager');
      const views = getSubViews(view);
      return views.map(v => sm.getViewDef(v));
    },
    getSubViews,
    /**
     * Returns a promise which when resolved returns the label for a view
     *
     * @param viewParam
     * @param params
     * @return {Promise|*}
     */
    getViewLabel(viewParam, params) {
      let label;
      const site = defaults.get('siteManager');
      const viewDef = site.getViewDef(viewParam);

      return new Promise((resolve, reject) => {
        if (typeof viewDef.title === 'undefined') {
          return site.getViewObject(viewDef.name).then((view) => {
            if (view.getViewLabel) {
              view.getViewLabel(viewDef.name, params, (lbl) => {
                if (lbl.length > 12) {
                  resolve(`${lbl.substr(0, 12)}…`);
                } else {
                  resolve(lbl);
                }
                resolve(lbl);
              });
            } else {
              label = navutils.getViewProp(viewDef, 'title');
              resolve(label);
            }
          }, () => reject());
        }
        if (typeof viewDef.title === 'string') {
          label = viewDef.title;
        } else if (typeof viewDef.title === 'object') {
          label = utils.getLocalizedValue(viewDef.title).value;
        }
        if (typeof viewDef.tooltip === 'string') {
          label = viewDef.tooltip;
        } else if (typeof viewDef.tooltip === 'object') {
          label = utils.getLocalizedValue(viewDef.tooltip).value;
        }

        return resolve(label || viewDef.name);
      });
    },
    /**
     * TODO rely on getViewLabel
     *
     * @param labelNode
     * @param titleNode
     * @param viewParam
     * @param params
     * @return {*}
     */
    setViewLabelAndTooltip(labelNode, titleNode, viewParam, params) {
      let label;
      const d = new Deferred();
      const site = defaults.get('siteManager');
      const viewDef = site.getViewDef(viewParam);
      if (typeof viewDef.title === 'undefined') {
        site.getViewObject(viewDef.name).then((view) => {
          if (view.getViewLabel) {
            view.getViewLabel(viewDef.name, params, (lbl, tooltip) => {
              if (lbl.length > 12) {
                domAttr.set(labelNode, 'innerHTML', `${lbl.substr(0, 12)}…`);
              } else {
                domAttr.set(labelNode, 'innerHTML', lbl);
              }
              if (tooltip) {
                domAttr.set(labelNode, 'title', tooltip);
              }
              d.resolve(lbl);
            });
          } else {
            label = navutils.getViewProp(viewDef, 'title');
            domAttr.set(labelNode, 'innerHTML', label);
            d.resolve(label);
          }
        }, () => d.reject());
        return d;
      }
      if (typeof viewDef.title === 'string') {
        label = viewDef.title;
      } else if (typeof viewDef.title === 'object') {
        label = utils.getLocalizedValue(viewDef.title).value;
      }
      if (typeof viewDef.tooltip === 'string') {
        label = viewDef.tooltip;
      } else if (typeof viewDef.tooltip === 'object') {
        label = utils.getLocalizedValue(viewDef.tooltip).value;
      }

      domAttr.set(labelNode, 'innerHTML', label || viewDef.name);
      if (viewDef.tooltip != null) {
        domAttr.set(titleNode, 'title', viewDef.tooltip);
      }
      d.resolve(label);
      return d;
    },
    setText(textNode, viewParam) {
      const site = defaults.get('siteManager');
      const viewDef = site.getViewDef(viewParam);
      site.getViewObject(viewDef).then((view) => {
        if (view.getViewText) {
          view.getViewText(viewDef.name, params, (text) => {
            domAttr.set(textNode, 'innerHTML', text);
          });
        } else {
          domAttr.set(textNode, 'innerHTML', navutils.getViewProp(viewDef, 'text'));
        }
      });
    },
    decodeHtml(html) {
      const txt = document.createElement('textarea');
      txt.innerHTML = html;
      return txt.value;
    },
  };
  return navutils;
});
