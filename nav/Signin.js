define([
  'config',
  'dojo/on',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  '../defaults',
  '../util/configUtil',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-commons/nav/Signup',
  'entryscape-commons/nav/PasswordReset',
  'entryscape-commons/view/PublicView',
  './components/Logo',
  'dojo/text!./SigninTemplate.html',
  'entryscape-commons/dialog/TitleDialog',
  'bootstrap/collapse',
  'i18n!nls/escoSignin',
], (config, on, declare, lang, domAttr, domStyle, domConstruct, domClass, html, _WidgetBase,
    _TemplatedMixin, defaults, configUtil, NLSMixin, localize, Signup, PasswordReset, PublicView,
    Logo, template, TitleDialog) => {
  let signup;
  let resetPassword;

  const SigninMixin = declare([PublicView], {
    nlsBundles: ['escoSignin'],
    templateString: template,
    bid: 'escoSignin',

    postCreate() {
      this.inherited(arguments);
      this.renderLogo();
      if (config.entrystore.internalsignin !== false || window.queryParamsMap.internal === true) {
        domClass.add(this.domNode, `${this.bid}--internal`);
      }
    },
    renderLogo() {
      const logoInfo = configUtil.getLogoInfo();
      m.render(this[`${this.bid}__home`], m(Logo, logoInfo));
    },
    updateExternalSignInOption() {
      const ces = config.entrystore.externalsignin;
      if (ces) {
        domClass.add(this.domNode, `${this.bid}--external`);
        if (ces.icon) {
          domAttr.set(this.__externalImage, 'src', ces.icon);
          if (ces.iconAlt) {
            domAttr.set(this.__externalImage, 'src', defaults.get('localize')(ces.iconAlt));
          }
        } else {
          domStyle.set(this.__externalImage, 'display', 'none');
        }
        if (ces.message) {
          domAttr.set(this.__externalMessage, 'innerHTML', defaults.get('localize')(ces.message));
        }
        const es = defaults.get('entrystore');
        const site = defaults.get('siteManager');
        let { nextView, nextViewParams } = site.getState();
        if (!nextView || (nextView && nextView === this.params.view)) {
          nextView = config.site.startView; // Fix to avoid infinite loops
          nextViewParams = this.params.params;
        }
        const successURL = encodeURIComponent(`${config.baseUrl}${site.getViewPath(nextView, nextViewParams)}`);
        const failureURL = encodeURIComponent(`${config.baseUrl}${site.getViewPath(config.site.signinView)}`);
        domAttr.set(this.__externalButton, 'href', `${es.getBaseURI()}auth/cas?redirectOnSuccess=${successURL}&redirectOnFailure=${failureURL}`);
      }
    }
    ,
    footerButtonAction() {
      if (!this.isFormValid(this.signinForm)) {
        return this.NLSBundle0.signinUnauthorized;
      }
      const esUser = domAttr.get(this.username, 'value');
      const esPassword = domAttr.get(this.password, 'value');
      const auth = defaults.get('entrystore').getAuth();
      const async = defaults.get('asynchandler');
      async.addIgnore('login', async.codes.UNAUTHORIZED, true);
      return auth.login(esUser, esPassword).then(
        lang.hitch(this, function () {
          this.signinForm.reset();
        }),
        lang.hitch(this, function (err) {
          if (err.response.status === 401) {
            throw this.NLSBundle0.signinUnauthorized;
          } else {
            throw this.NLSBundle0.signinError;
          }
        }));
    },
    signin() {
    },
    signout() {
    },
    start() {
    },
    showSignupDialog() {
      signup.show();
    },
    showPWResetDialog() {
      resetPassword.show();
    },
    isFormValid(form) {
      if (lang.isFunction(form.checkValidity)) {
        return form.checkValidity();
      }
      return true;
    },

  });

  const Signin = declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit, SigninMixin], {
    nlsHeaderTitle: 'signInHeader',
    nlsFooterButtonLabel: 'signInButton',
    maxWidth: 800,
    nextView: '',
    show(params) {
      this.params = params;
      this.setStatus();
      this.updateExternalSignInOption();
    },
    postCreate() {
      this.inherited('postCreate', arguments);
      on(this.password, 'keypress', lang.hitch(this, (ev) => {
        if (ev.key === 'Enter') {
          ev.preventDefault();
          ev.stopPropagation();
          this.signin();
        }
      }));

      if (config.site && config.site.signup !== false) {
        domStyle.set(this.showSignupButton, 'display', '');
      }
      defaults.onChange('authorizedUser', lang.hitch(this, (user) => {
        this.setStatus();
        domClass.toggle(this.domNode, `${this.bid}--signout`, user != null);
      }), true);
      defaults.onChange('userEntryInfo', lang.hitch(this, (info) => {
        this.localeReady.then(lang.hitch(this, () => {
          const b = this.NLSBundle0;
          if (info.displayName) {
            html.set(this.signedInHeaderNode,
              localize(b, 'signedInHeader', { name: info.displayName }));
          }
        }));
      }), true);
    },
    signin() {
      const p = this.footerButtonAction();
      if (p && typeof p.then === 'function') {
        p.then(lang.hitch(this, () => {
          const site = defaults.get('siteManager');
          let { nextView, nextViewParams } = site.getState();
          if (!nextView || (nextView && nextView === this.params.view)) {
            nextView = config.site.startView; // Fix to avoid infinite loops
            nextViewParams = this.params.params;
          }
          if (nextView) {
            /**
             * Open the next view only when userEntryInfo is set
             */
            defaults.onChangeOnce('userEntryInfo', () => {
              defaults.get('siteManager').render(nextView, nextViewParams);
            });
          }
        }), lang.hitch(this, this.setStatus));
      } else if (typeof p === 'string') {
        this.setStatus(p);
      }
    },
    signout() {
      return defaults.get('entrystore').getAuth().logout();
    },
    start() {
      return defaults.get('siteManager').render(config.site.startView);
    },
    setStatus(message) {
      if (message) {
        domStyle.set(this.signinStatusWrapper, 'display', '');
        html.set(this.signinStatus, message);
      } else {
        domStyle.set(this.signinStatusWrapper, 'display', 'none');
      }
    },
  });

  Signin.Dialog = declare([TitleDialog.ContentNLS, SigninMixin], {
    nlsHeaderTitle: 'signInHeader',
    nlsFooterButtonLabel: 'signInButton',
    maxWidth: 800,
    show() {
      html.set(this.signinStatus, '');
      this.updateExternalSignInOption();
      this.dialog.show();
    },
    postCreate() {
      this.inherited('postCreate', arguments);
      on(this.password, 'keypress', lang.hitch(this, (ev) => {
        if (ev.key === 'Enter') {
          ev.preventDefault();
          ev.stopPropagation();
          this.dialog.footerButtonClick();
        }
      }));
      domClass.add(this.domNode, 'signindialog');
      // TODO remove site.signup in next version, this is for compatability with old configs
      if (config.site.signup !== false && config.entrystore.signup !== false) {
        domStyle.set(this.showSignupButton, 'display', '');
      }

      if (!signup) {
        signup = new Signup({}, null, domConstruct.create('div', null, this.domNode));
        resetPassword = new PasswordReset({}, null, domConstruct.create('div', null, this.domNode));
        defaults.set('signInDialog', this);
        defaults.set('signUpDialog', signup);
      }
    },
  });

  return Signin;
});
