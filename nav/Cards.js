define([
  'dojo/_base/declare',
  'dojo/_base/array',
  '../defaults',
  './utils',
  '../view/ViewMixin',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./CardsTemplate.html',
], (declare, array, defaults, utils, ViewMixin, domClass, domConstruct, domAttr, _WidgetBase,
    _TemplatedMixin, template) =>

  declare([_WidgetBase, _TemplatedMixin, ViewMixin], {
    templateString: template,
    textWithCards: true,

    render() {
      domAttr.set(this.mainNode, 'innerHTML', '');
      const site = defaults.get('siteManager');
      this.renderBanner();
      const cards = this.getCards();
      if (this.textWithCards) {
        domClass.add(this.mainNode, 'textWithCards');
      }

      const context = this.getContextId();
      cards.forEach(async (card) => {
        const cardView = this.getCardView(card);
        const col = domConstruct.create('div', { class: 'col-xs-6 col-sm-6 col-md-4' }, this.mainNode);
        const cardBlock = domConstruct.create('div', { class: 'square-service-block' }, col);
        const a = domConstruct.create('a', {
          class: cardView == null ? 'disabled' : '',
        }, cardBlock);
        if (cardView != null) {
          const params = context ? { context } : {};
          domAttr.set(a, 'href', site.getViewPath(cardView, params));
        }
        let titleNode;
        if (this.textWithCards) {
          const div = domConstruct.create('div', { class: 'ssb-icon' }, a);
          domConstruct.create('i', { class: `fa fa-${card.faClass}`, 'aria-hidden': 'true' }, div);
          titleNode = domConstruct.create('h2', { class: 'ssb-title' }, a);
          const textNode = domConstruct.create('p', { class: 'font-size-large' }, titleNode);
          await this.setLabelAndTooltip(titleNode, a, card);
          this.setText(textNode, card);
        } else {
          domConstruct.create('h2', { innerHTML: `<i class="fa fa-2x fa-${card.faClass}"></i>` }, a);
          titleNode = domConstruct.create('h3', null, a);
          await this.setLabelAndTooltip(titleNode, a, card);
        }
      }, this);
    },

    /**
     * Return context id if context is defined
     * @return {Integer|null}
     */
    getContextId() {
      const context = defaults.get('context');
      if (context) {
        return context.getId();
      }

      return null;
    },

    renderBanner() {
      domAttr.set(this.bannerNode, 'innerHTML', '');
      domConstruct.place(this.getBannerNode(), this.bannerNode);
    },

    getBannerNode() {
      return domConstruct.create('div', { innerHTML: this.getBannerHTML() });
    },

    getBannerHTML() {
      domClass.add(this.domNode, 'entryscapeCards--noBanner');
      return '';
    },

    show(params) {
      this.viewParams = params;
      this.render();
    },

    /**
     * For each card definition the following attributes are considered:
     * name - the name for the card
     * view - the name of the view the card should link to.
     * faClass - the font awesome icon to use.
     *
     * @returns {object[]} array of card definitions
     */
    getCards() {
      const sm = defaults.get('siteManager');
      const views = utils.getSubViews(this.viewParams.view);
      return array.map(views, function (v) {
        if (typeof v.text === 'object') {
          this.textWithCards = true;
        }
        const viewDef = sm.getViewDef(v);
        return {
          def: viewDef,
          name: v,
          view: v,
          faClass: viewDef != null ? viewDef.faClass : '',
        };
      }, this);
    },

    async setLabelAndTooltip(labelNode, tooltipNode, card) {
      await utils.setViewLabelAndTooltip(labelNode, tooltipNode, this.getCardView(card), this.viewParams);
    },

    setText(textNode, card) {
      utils.setText(textNode, this.getCardView(card));
    },

    /**
     * Sometimes cards are modules (startView) and others they are views (view)
     * @param card
     * @return {*}
     */
    getCardView(card) {
      return card.view || card.startView;
    }
  }));
