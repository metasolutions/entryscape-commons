define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/promise/all',
  'dojo/has',
  'dojo/on',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'jquery',
  './Site',
  'spa/SiteController',
  './utils',
  '../util/configUtil',
  'config',
  './Settings', // In template
  './Signin', // In template
  '../defaults',
  './components/Menu',
  './components/Breadcrumb',
  './components/Logo',
  'di18n/NLSMixin',
  'mithril',

  'dojo/text!./LayoutTemplate.html',
  'bootstrap/collapse',
  'bootstrap/dropdown',
  'i18n!nls/escoLayout',
  'i18n!nls/escoModules',
], (declare, lang, all, has, on, domClass, domStyle, domConstruct, domAttr, _WidgetBase,
    _TemplatedMixin, _WidgetsInTemplateMixin, jquery, Site, SiteController, utils, configUtil, config, Settings,
    Signin, defaults, Menu, Breadcrumb, Logo, NLSMixin, mithril, template) => {
  return declare(
    [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, SiteController, NLSMixin.Dijit], {
      templateString: template,
      _entry2label: {},
      nlsBundles: ['escoLayout', 'escoModules'],
      _firstLoad: true,
      bid: 'layout',

      postCreate() {
        this.inherited('postCreate', arguments);

        this.menu = new Menu();
        this.signin = new Signin();
        this.signinDialog = new Signin.Dialog({}, domConstruct.create('div', null, this.domNode));
        domClass.add(document.body, 'spaSite');
        domClass.add(document.body, 'spaFullscreen');

        this.signOutButtonClicked = false;

        this.renderLogo();
        this.renderFooterLogo();

        if (config.theme && config.theme.oneRowNavbar) {
          this.crumbList = this.crumbListSingle;
          this.tabList = this.tabListSingle;
        }

        this.constructFooter();

        if (config.site.modules && config.site.modules.length > 1) {
          this.showNode(this.menuListNode);
        }

        if (config.theme && config.theme.showCurrentContext) {
          this.showNode(this.contextLabelWrapper);
        }

        if (config.theme && config.theme.showHelpInfo) {
          this.showNode(this.helpInfoFooter);
          this.showNode(this.helpInfoDropdown);
        }

        if (config.site.startView) {
          const site = defaults.get('siteManager');
          domAttr.set(this.home, 'href', site.getViewPath(config.site.startView));
        }

        if (config.theme && config.theme.feedbackViaJira) {
          const fb = this.feedbackButton;
          window.ATL_JQ_PAGE_PROPS = {
            triggerFunction(showCollectorDialog) {
              on(fb, 'click', (e) => {
                e.preventDefault();
                showCollectorDialog();
              });
            },
          };
          this.showNode(this.feedback);
          require([config.theme.feedbackViaJira]);
        }
        this.manageUserLayout();
        defaults.onChange('userEntryInfo', (user) => {
          this.manageUserLayout(user);
        });
        defaults.onChange('context', lang.hitch(this, function (context) {
          if (config.theme && config.theme.showCurrentContext) {
            const rdfutils = defaults.get('rdfutils');
            if (context != null) {
              const async = defaults.get('asynchandler');
              const es = context.getEntryStore();
              const ac = 'contextInHeadCheck';
              async.addIgnore(ac, async.codes.GENERIC_PROBLEM, true);
              es.getEntry(context.getEntryURI(), { asyncContext: ac })
                .then(lang.hitch(this, function (contextEntry) {
                  const lbl = rdfutils.getLabel(contextEntry) || context.getId();
                  domAttr.set(this.contextLabel, 'innerHTML', lbl);
                  domAttr.set(this.contextLabel, 'title', lbl);
                }));
            } else {
              domAttr.set(this.contextLabel, 'innerHTML', '');
              domAttr.set(this.contextLabel, 'title', '');
            }
          }
        }), true);

        const locale2flag = {};
        config.locale.supported.forEach((l) => {
          locale2flag[l.lang] = l.flag;
          const li = domConstruct.create('li', { title: l.labelEn }, this.langListNode);
          const a = domConstruct.create('a', null, li);
          domConstruct.create('span', { class: `flag-icon flag-icon-${l.flag}` }, a);
          domConstruct.create('span', { innerHTML: `&nbsp;&nbsp;${l.label}` }, a);
          on(a, 'click', () => {
            defaults.set('locale', l.lang);
          });
        }, this);
        // setting dialog is also shown by clicking user name
        const openSettingsDialog = lang.hitch(this.settingsDialog, 'show');
        on(this.settingsButton, 'click', openSettingsDialog);
        on(this.settingsButtonFooter, 'click', openSettingsDialog);

        on(this.signOutButton, 'click', lang.hitch(this, () => {
          this.signOutButtonClicked = true;
          this.signin.signout().then(() => {
            const site = defaults.get('siteManager');
            this.destroyComponents();
            this.destroyMenu();
            // remove other params, only view should be passed.
            site.render(config.site.startView || config.site.signinView, {});
            this.signOutButtonClicked = false;
          });
        }));

        defaults.onChange('locale', lang.hitch(this, function (l) {
          if (this._currentLocale) {
            domClass.remove(this.flagNode, `flag-icon-${locale2flag[this._currentLocale]}`);
          }
          domClass.add(this.flagNode, `flag-icon-${locale2flag[l]}`);
          this._currentLocale = l;
        }), true);

        on(this.privacyPolicyButton, 'click', lang.hitch(this, () => {
          defaults.get('signUpDialog').showPULInfoDialog();
        }));
        on(this.privacyPolicyButton_footer, 'click', lang.hitch(this, () => {
          defaults.get('signUpDialog').showPULInfoDialog();
        }));
        defaults.onInit('siteManager', () => {
          this.localeReady.then(() => {
            this.hideForPublicView();
          });
          this.name2Def = {};
          config.site.views.forEach((view) => {
            this.name2Def[view.name] = view;
          }, this);
        });
      },
      renderLogo() {
        const logoInfo = configUtil.getLogoInfo();
        m.render(this.home, m(Logo, logoInfo));
      },
      renderFooterLogo() {
        const logoInfo = configUtil.getLogoInfo('icon');
        logoInfo.isFooter = true;
        m.render(this.logoNodeFooter, m(Logo, logoInfo));
      },
      constructFooter() {
        if (config.theme && config.theme.footer && config.theme.footer.text) {
          const footerText = config.theme.footer.text[`${defaults.get('locale')}`];
          domAttr.set(this.footerText, 'innerHTML', footerText);
        }
        if (config.theme && config.theme.footer && config.theme.footer.buttons) {
          const localize = defaults.get('localize');
          config.theme.footer.buttons.forEach((fButton) => {
            const label = localize(fButton.label);
            const title = localize(fButton.title);
            const a = domConstruct.create('a', {
              class: 'bottom_footer_button btn btn-raised btn-link btn-sm',
              type: 'button',
              href: fButton.link,
            }, this.footerButtons);
            if (fButton.faIcon) {
              domConstruct.create('span', { class: `fa fa-${fButton.faIcon}` }, a);
            }
            domAttr.set(a, 'title', title);
            domConstruct.create('span', { innerHTML: label }, a);
            if (fButton.link.indexOf('http') === 0) {
              domClass.add(a, 'spaExplicitLink');
              domAttr.set(a, 'target', '_blank');
            } else {
              on(a, 'click', (event) => {
                event.stopPropagation();
                event.preventDefault();
                defaults.get('dialogs').acknowledgeText(fButton.link, title);
              });
            }
          }, this);
        }
      },
      manageUserLayout(user = null) {
        if (config.site.public) {
          return;
        }
        if (defaults.get('userInfo').id === '_guest' || window.queryParamsMap.publicView) {
          this.hideNode(this.userDetails);
          this.showNode(this.guestDetails);
          this.hideNode(this.menuListNode);
          this.hideNode(this.sidebarFooter);
        } else {
          this.showNode(this.userDetails);
          this.hideNode(this.guestDetails);
          if (user) {
            domAttr.set(this.userName, 'innerHTML', user.displayName);
            domAttr.set(this.userProfile, 'title', user.displayName);
          } else {
            defaults.get('userEntryInfo', (usr) => {
              domAttr.set(this.userName, 'innerHTML', usr.displayName);
            });
          }

          // show in case they were hidden for public view and then user signed-in
          this.showNode(this.menuListNode);
          this.showNode(this.sidebarFooter);
        }
      },
      hideForPublicView() {
        const site = defaults.get('siteManager');
        const module = site.getSelectedModule();
        if (module && module.public) {
          defaults.get('userEntryInfo', (info) => {
            if (info.entryId === '_guest') {
              // hide side menu
              this.hideNode(this.menuListNode);
              // hide side footer
              this.hideNode(this.sidebarFooter);
              // show breadcrumb
              this.showNode(this.allBreadcrumbs);
            }
          });
        }
      },
      getCurrentModuleName() {
        const site = defaults.get('siteManager');
        const module = site.getSelectedModule();

        if (module && 'productName' in module) {
          return module.productName;
        }
        return this.getModuleStr('title', module);
      },
      getModuleStr(prop, module = null) {
        return module ? utils.getModuleProp(module, this.NLSBundle1, prop) : '';
      },
      localeChange() {
        if (this._firstLoad === true) {
          this._firstLoad = false;
          if (!(has('chrome') >= 32 || has('ff') > 26 || has('ie') >= 11 || has('trident') || has('edge') || has('safari') >= 8)) {
            defaults.get('dialogs').acknowledge(this.NLSBundle0.unSupportedBrowser, this.NLSBundle0.continueUnsupportedBrowser);
          }
        }
      },
      show(viewName, params) {
        this.inherited('show', arguments);
        if (viewName === config.site.signinView) {
          this.destroyMenu();
          this.showNode(this.privacyMenu);
        } else {
          this.renderMenu();
          this.hideNode(this.privacyMenu);
        }

        domAttr.set(this.tabList, 'innerHTML', '');
        this.hideNode(this.tabList);
        this.hideNavBar();
        const nvi = this.getNavBarInfo(viewName);
        if (viewName === config.site.startView || nvi.alone ||
          (config.theme && config.theme.oneRowNavbar)) {
          domClass.remove(this.containerNode, 'spaTwoMenu');
          this.destroyComponents();
        } else {
          domClass.add(this.containerNode, 'spaTwoMenu');
        }

        if (nvi.show) {
          this.showNavBar(nvi, viewName, params);
        }

        const module = defaults.get('siteManager').getSelectedModule();
        if (module == null || nvi.alone) {
          document.title = utils.decodeHtml(configUtil.getAppName());
          return;
        }

        const upcomingView = defaults.get('siteManager').getUpcomingOrCurrentView();
        const upcomingViewDef = this.name2Def[upcomingView];
        const viewsPathArr = this.getBreadcrumbViews(upcomingViewDef);
        const breadcrumbItems = [];
        if (module.asCrumb) {
          breadcrumbItems.push(this.createModuleCrumb(params));
        }
        viewsPathArr.forEach((viewN) => {
          const viewDef = this.name2Def[viewN];
          if (viewDef.hidden === true) {
            return;
          }

          if (viewsPathArr.length > 0) {
            const authorizedUser = defaults.get('authorizedUser');
            if (authorizedUser && !this.signOutButtonClicked) {
              breadcrumbItems.push(this.createCrumb(viewDef, viewName, params));
            }
          } else { // TODO not sure if this is relevant any more?
            this.createTab(viewDef, viewName, params);
          }
        }, this);

//        let lastArrIdx = viewsPathArr.length - 1;
        Promise.all(breadcrumbItems).then((items) => {
          const currentView = defaults.get('siteManager').getCurrentView();
          if ((currentView && currentView !== config.site.signinView || upcomingView !== config.site.signinView) && items.length > 0) {
            this.showBreadcrumb(items);
            this.updateWindowTitle(items[items.length - 1].value); // update window title
          }
        });
      },
      hideNode(node) {
        domStyle.set(node, 'display', 'none');
      },
      showNode(node) {
        domStyle.set(node, 'display', '');
      },
      updateWindowTitle(label) {
        const mname = this.getCurrentModuleName();
        const context = defaults.get('context');
        if (context) {
          context.getEntry().then((centry) => {
            const cname = defaults.get('rdfutils').getLabel(centry);
            if (label === cname) {
              document.title = utils.decodeHtml(`${label} - EntryScape ${mname}`);
            } else {
              document.title = utils.decodeHtml(`${label} - ${cname} - EntryScape ${mname}`);
            }
          });
        } else {
          document.title = utils.decodeHtml(`${label} - EntryScape ${mname}`);
        }
      },
      destroyComponents() {
        this.hideBreadcrumb();
      },
      renderMenu() {
        this.showNode(this.menuListNode);
        m.mount(this.menuListNode, this.menu);
      },
      destroyMenu() {
        this.hideNode(this.menuListNode);
        m.mount(this.menuListNode, null);
      },
      showBreadcrumb(items) {
        this.showNode(this.crumbList);
        m.render(this.crumbList, m(Breadcrumb, { items }));
      },
      hideBreadcrumb() {
        this.hideNode(this.crumbList);
        m.render(this.crumbList, null);
      },
      hideNavBar() {
        this.hideNode(this.controllerViewList);
        domClass.add(this.viewsNode, 'col-md-12');
        domClass.remove(this.viewsNode, 'col-md-11');
      },
      showNavBar(nvi, viewName, params) {
        domAttr.set(this.controllerViewList, 'innerHTML', '');
        this.showNode(this.controllerViewList);
        const site = defaults.get('siteManager');
        nvi.views.forEach((viewDef) => {
          const isSelected = viewDef.name === viewName;

          const li = domConstruct.create('li', {
            class: isSelected ? 'selected' : '',
          }, this.controllerViewList);
          const a = domConstruct.create('a', {}, li);
          if (!isSelected) {
            domAttr.set(a, 'href', site.getViewPath(viewDef.name, params));
          }
          const titleNode = domConstruct.create('span', null, a);
          utils.setViewLabelAndTooltip(titleNode, a, viewDef.name, params);
        }, this);
      },
      createTab(viewDef, currentViewName, params) {
        const li = domConstruct.create('li', {}, this.tabList);
        const a = domConstruct.create('a', {
          class: `${viewDef.name} spaTab`,
          href: this.site.getViewPath(viewDef.name, params),
        }, li);
        utils.setViewLabelAndTooltip(a, li, viewDef.name, params);
        if (viewDef.name === currentViewName) {
          domClass.add(li, 'active');
        }
        return li;
      },
      createModuleCrumb(params) {
        const site = defaults.get('siteManager');
        const module = site.getSelectedModule(); // there should be always a module if you are rendering breadcrumbs
        const href = this.site.getViewPath(module.startView, params);
        const className = 'active';
        const label = utils.getViewProp(module, 'title');
        const value = label.replace('&shy;', '\u00AD');
        return Promise.resolve({ value, className, href });
      },
      createCrumb(viewDef, currentViewName, params) {
        let href = '';
        let className = '';

        if (viewDef.name === currentViewName) {
          className = 'active';
        } else if (viewDef.labelCrumb === true) {
          className = 'crumbPath';
        } else {
          href = this.site.getViewPath(viewDef.name, params);
        }

        return utils.getViewLabel(viewDef.name, params)
          .then((label) => {
            // replace HTML code to Unicode for mithril
            const value = label.replace('&shy;', '\u00AD');
            return { value, className, href };
          });
      },
    });
});
