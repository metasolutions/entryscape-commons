define([
  'dojo/_base/declare',
  'di18n/NLSMixin',
  '../../defaults',
  '../utils',
  './menu/MenuList',
  'i18n!nls/escoModules',
], (declare, NLSMixin, defaults, utils, MenuList) => {
  /**
   * Helper function to get the module title
   * @param {Object} module
   * @param {Object} bundle
   * @return {String}
   */
  const getLocalizedModuleTitle = (module, bundle) => {
    if (module.productName && bundle) {
      const productNameTranslated = utils.getModuleProp(module, bundle, 'title');
      if (productNameTranslated) {
        return productNameTranslated;
      }

      return module.productName;
    }

    return '';
  };

  /**
   * Transform the active moudles to component input
   * { name, label, href, icon }
   *
   * @param bundle
   * @return {Array}
   */
  const getModulesData = (bundle) => {
    const modulesData = [];
    if (bundle) {
      const site = defaults.get('siteManager');
      return Array.from(site.modules).map((mapEntry) => {
        module = mapEntry[1];
        const startView = module.startView || module.views[0];
        return {
          name: module.name,
          label: getLocalizedModuleTitle(module, bundle),
          href: site.getViewPath(startView), // module's defined start view or the first view
          icon: module.faClass,
        };
      });
    }

    return [];
  };

  /**
   * The state of the menu in the UI
   * @type {{items: Array, selectedItem: string}}
   */
  const menuState = {
    items: [],
    selectedItem: '',
    /**
     * Change menu state items
     * @param bundle
     */
    setItems(bundle = null) {
      menuState.items = getModulesData(bundle);
    },
    onclick(module) {
      const site = defaults.get('siteManager');
      if (module) {
        menuState.selectedItem = module;
      } else {
        const selectedModule = site.getSelectedModule();
        menuState.selectedItem = selectedModule ? selectedModule.name : '';
      }
    }
  }

  return declare([NLSMixin], {
    nlsBundles: ['escoModules'],
    constructor() {
      this.initNLS();
      defaults.onChange('userEntryInfo', this.updateItems.bind(this))
    },
    localeChange() {
      this.updateItems();
    },
    updateItems() {
      menuState.setItems(this.NLSBundle0);
      m.redraw();
    },
    updateSelectedItem() {
      menuState.onclick();
    },
    view(vnode) {
      this.updateSelectedItem();
      const { items, selectedItem, onclick } = menuState;
      return m('div', m(MenuList, { items, selectedItem, onclick }));
    }
  });
});
