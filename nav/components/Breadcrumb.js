define([
  './BreadcrumbItem',
], (BreadcrumbItem) => {
  const Breadcrumb = {
    view(vnode) {
      const { items } = vnode.attrs;
      return items.map(item => m(BreadcrumbItem, { item }));
    },
  };

  return Breadcrumb;
});
