define([
  'dojo/_base/declare',
  'dojo/_base/array',
  'dojo/_base/lang',
  'dojo/dom-attr',
  '../defaults',
  'jquery',
  'typeahead', // used via plugin on jquery
], (declare, array, lang, domAttr, defaults, jquery, typeahead) => {
  const rdfutils = defaults.get('rdfutils');
  /** @type {store/EntryStore}*/
  const es = defaults.get('entrystore');
  let counter = 0;

  return declare([], {
    delay: 200,
    emptyMessage: 'No matching entries',
    getLabel(entry) {
      return rdfutils.getLabel(entry) || entry.getId();
    },

    getQuery(str) {
      return es.newSolrQuery().title(str).limit(10).list();
    },

    render(entry) {
      return `<div class="entryscapeTypeahead" title="${
      rdfutils.getDescription(entry) || ''}">${
        this.getLabel(entry)}</div>`;
    },

    processEntries(entries, callback) {
      callback(entries.map(entry => ({ id: entry.getURI(), name: this.getLabel(entry) })));
    },

    select() {
    },

    clear() {
      jquery(this.inputTag).typeahead('val', '');
    },

    setDisabled(disabled) {
      if (disabled) {
        domAttr.set(this.inputTag, 'disabled', 'disabled');
      } else {
        domAttr.remove(this.inputTag, 'disabled');
      }
    },

    // -------Below is constructor, private methods and private variables---------
    constructor(params, inputTag) {
      this.inputTag = inputTag;
      const $inputTag = jquery(inputTag);
      $inputTag.typeahead({ hint: false, highlight: true, minLength: 1 },
        {
          name: `entryscape_typeahead_${counter}`,
          display: 'name',
          source: lang.hitch(this, this._find),
          templates: {
            empty: [
              '<div class="empty-message">',
              this.emptyMessage,
              '</div>',
            ].join('\n'),
            suggestion: lang.hitch(this, this._render),
          },
        });
      $inputTag.on('typeahead:select', lang.hitch(this, this._select));
      counter += 1;
    },
    _render(value) {
      return this.render(es.getCache().get(value.id));
    },
    _select(el, value) {
      this.select(es.getCache().get(value.id));
    },

    _find(query, syncResults, asyncResults) {
      clearTimeout(this.timeoutFunc);
      const f = (entryArr) => {
        this.processEntries(entryArr, asyncResults);
      };
      this.timeoutFunc = setTimeout(() => {
        this.getQuery(query).getEntries(0).then(f);
      }, this.delay);
    },
  });
});
