define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/string',
  'dojo/date/locale',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/text!./LinkedEntriesViewTemplate.html',
  '../defaults',
  'dojo/dom-style',
  'dojo/dom-construct',
  'di18n/localize',
  'rdforms/view/Editor',
  'rdforms/view/Presenter',
  './MetadataComponent',
  './ContentView',
  'entryscape-commons/create/typeIndex',
  'i18n!nls/escoContentview',
], (declare, lang, string, locale, domAttr, domClass, template, defaults, domStyle, domConstruct,
    localize, Editor, Presenter, MetadataComponent, ContentView, typeIndex) =>
  declare([ContentView], {
    templateString: template,
    nlsBundles: ['escoContentview'],
    bid: 'escoContentview',
    entityConf: null,
    contentViewConf: null,

    postCreate() {
      if (!this.contentViewConf.linkedEntityType) {
        console.error(`No linkedEntitytype given in config for contentviewer "${this.contentViewConf.name}" on entitytype "${this.entityConf.name}"`);
        return;
      }
      this.splitConf = typeIndex.getConfByName(this.contentViewConf.linkedEntityType);

      this.template = defaults.get('itemstore').getItem(this.splitConf.template);
      this.editor = new MetadataComponent({
        template: this.template,
        parentObj: this,
        presenterMode: false,
      }, domConstruct.create('div', null, this.__editor));
      defaults.get('context').getEntry().then(lang.hitch(this, (contextEntry) => {
        if (contextEntry.canWriteResource()) {
          domStyle.set(this.__addNode, 'display', '');
        }
      }));
      this.renderViewComponents();
      this.inherited(arguments);
    },
    renderViewComponents() {
      domConstruct.empty(this.__viewComponentsList);
      const es = defaults.get('entrystore');
      es.newSolrQuery()
        .uriProperty(this.contentViewConf.relation, this.entry.getResourceURI())
        .rdfType(this.splitConf.rdfType)
        .list()
        .getEntries(0)
        .then((entries) => {
          entries.forEach((entry) => {
            if (entry != null) {
              MetadataComponent({ entry, parent: this, template: this.template },
                domConstruct.create('div', null, this.__viewComponentsList));
            }
          });
          if (entries.length === 0) {
            this.add();
          }
        });
    },
    add() {
      domStyle.set(this.__add, 'display', 'block');
      domStyle.set(this.__addNode, 'display', 'none');
      this.newPEntry = defaults.get('createEntry')();
      // Add constraints explicit in splitConf
      typeIndex.addConstraints(this.splitConf, this.newPEntry);
      // Add the relation back to the entry
      this.newPEntry.getMetadata().add(this.newPEntry.getResourceURI(),
        this.contentViewConf.relation, this.entry.getResourceURI());
      this.editor.show(this.newPEntry.getMetadata(), this.newPEntry.getResourceURI());
    },
    updateUI() {
      domStyle.set(this.__add, 'display', 'none');
      domStyle.set(this.__addNode, 'display', 'block');
    },
    save(graph) {
      this.newPEntry.setMetadata(graph).commit().then((entry) => {
        MetadataComponent({
          entry,
          parent: this,
          template: this.template,
        }, domConstruct.create('div', null, this.__viewComponentsList, 'first'));
        domStyle.set(this.__add, 'display', 'none');
        domStyle.set(this.__addNode, 'display', 'block');
      });
    },
  }));
