define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'entryscape-commons/util/dateUtil',
  'dojo/text!./MetadataComponentTemplate.html',
  '../defaults',
  'dojo/dom-style',
  'dojo/dom-construct',
  'rdforms/view/Editor',
  'rdforms/view/Presenter',
  'rdfjson/Graph',
  'rdforms/model/validate',
  'i18n!nls/escoContentview',
], (declare, lang, domAttr, domClass, _WidgetBase, _TemplatedMixin, NLSMixin, dateUtil,
    template, defaults, domStyle, domConstruct, Editor, Presenter, Graph, validate) =>
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escoContentview', 'escoRdforms'],
    bid: 'escoMetadataComponent',
    entry: null,
    template: null,
    presenterMode: true,

    postCreate() {
      if (this.presenterMode) {
        this.presenter = new Presenter({
          compact: false,
          filterTranslations: true,
        }, domConstruct.create('div', null, this.__presenter));
        this.graph = this.entry.getMetadata();
        this.presenter.show({
          resource: this.entry.getResourceURI(),
          graph: this.graph,
          template: this.template,
        });
        const cDate = this.entry.getEntryInfo().getModificationDate();
        const mDateFormats = dateUtil.getMultipleDateFormats(cDate);// code change
        domAttr.set(this.__dateNode, { innerHTML: mDateFormats.short });// code
        this.viewMode();
      }
      defaults.get('context').getEntry().then((contextEntry) => {
        if (contextEntry.canWriteResource()) {
          domStyle.set(this.__deleteButtonNode, 'display', '');
          domStyle.set(this.__editButtonNode, 'display', '');
          domStyle.set(this.__saveButtonNode, 'display', '');
          domStyle.set(this.__cancelButtonLabelNode, 'display', '');
        }
      });
      this.editor = new Editor({}, domConstruct.create('div', null, this.__editorNode, 'first'));
      this.inherited(arguments);
    },

    localeChange() {
      const bundle = this.NLSBundle1;
      this.discardWarning = bundle.discardMetadataChangesWarning;
      this.discardOption = bundle.discardMetadataChanges;
      this.keepOption = bundle.keepMetadataChanges;
      this.tooFewFields = bundle.tooFewFields;
    },
    show(graph, resourceURI) {
      this.editMode();
      const self = this;
      this.oldGraph = graph;
      this.graph = new Graph(graph.exportRDFJSON());
      this.lockSaveButton();
      this.graph.onChange = () => {
        // check individual field value and lock save accordingly
        self.unlockSaveButton();
      };
      this.editor.show({
        resource: resourceURI,
        graph: this.graph,
        template: this.template,
        compact: true,
      });
    },
    unlockSaveButton() {
      domClass.remove(this.__saveButtonNode, 'disabled');
    },
    lockSaveButton() {
      domClass.add(this.__saveButtonNode, 'disabled');
    },
    editSave(graph) {
      this.graph = graph;
      this.entry.setMetadata(graph);
      this.entry.commitMetadata().then(() => {
        this.presenter.show({
          resource: this.entry.getResourceURI(),
          graph,
          template: this.template,
        });
        this.viewMode();
      });
    },
    edit() {
      this.lockSaveButton();
      this.show(this.graph, this.entry.getResourceURI());
    },
    remove() {
      this.entry.del().then(() => {
        this.parent.renderViewComponents();
      });
    },
    updateUI(graph) {
      if (this.parentObj && !this.presenterMode) {
        this.parentObj.updateUI();
        return;
      }
      this.presenter.show({
        resource: this.entry.getResourceURI(),
        graph,
        template: this.template,
      });
      domStyle.set(this.__presenter, 'display', 'block');
      domStyle.set(this.__editorNode, 'display', 'none');
    },
    save() {
      const report = validate.bindingReport(this.editor.binding);
      if (report.errors.length > 0) {
        this.graph = new Graph(this.oldGraph.exportRDFJSON());
        this.editor.report(report);
        this.showErrorMessage(this.tooFewFields);
        return;
      }
      if (this.parentObj && !this.presenterMode) {
        this.parentObj.save(this.graph);
        return;
      }
      this.editSave(this.graph);
    },
    cancel() {
      if (!this.graph.isChanged()) {
        this.viewMode();
        this.updateUI();
        return;
      }
      const dialogs = defaults.get('dialogs');
      dialogs.confirm(this.discardWarning, this.discardOption, this.keepOption,
        (discard) => {
          if (discard) {
            this.viewMode();
            this.graph = new Graph(this.oldGraph.exportRDFJSON());
            if (this.parentObj && !this.presenterMode) {
              this.parentObj.graph = new Graph(this.oldGraph.exportRDFJSON());
              this.parentObj.updateUI();
            }
          }
        });
    },
    closeErrorMessage() {
      domStyle.set(this.errorMessageAlert, 'display', 'none');
    },

    showErrorMessage(message) {
      domAttr.set(this.errorMessage, 'innerHTML', message);
      domStyle.set(this.errorMessageAlert, 'display', '');
    },
    editMode() {
      domStyle.set(this.__presenter, 'display', 'none');
      domStyle.set(this.__editButtonNode, 'display', 'none');
      domStyle.set(this.__deleteButtonNode, 'display', 'none');

      domStyle.set(this.__editorNode, 'display', 'block');
      domStyle.set(this.__saveButtonNode, 'display', 'block');
      domStyle.set(this.__cancelButtonNode, 'display', 'block');
    },
    viewMode() {
      this.closeErrorMessage();
      domStyle.set(this.__presenter, 'display', 'block');
      domStyle.set(this.__editButtonNode, 'display', 'block');
      domStyle.set(this.__deleteButtonNode, 'display', 'block');

      domStyle.set(this.__editorNode, 'display', 'none');
      domStyle.set(this.__saveButtonNode, 'display', 'none');
      domStyle.set(this.__cancelButtonNode, 'display', 'none');
    },
  }));
