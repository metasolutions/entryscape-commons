define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  '../defaults',
  'rdforms/view/Presenter',
], (declare, domConstruct, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin,
    defaults, Presenter) =>
  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    includeMetadataPresentation: false,
    entry: null,
    entityConf: null,
    contentViewConf: null,
    tabs: null,

    postCreate() {
      this.inherited(arguments);
      if (this.includeMetadataPresentation) {
        this.renderMetadata();
      }
    },
    renderMetadata() {
      let template;
      if (this.tabs.list) {
        template = this.tabs.list.getTemplate();
      } else {
        template = defaults.get('itemstore').getItem(this.entityConf.template);
      }
      const presenter = new Presenter({ compact: true }, domConstruct.create('div', null, this.__metadataViewer));
      const resource = this.entry.getResourceURI();
      const graph = this.entry.getMetadata();
      presenter.show({ resource, graph, template });
    },
  }));
