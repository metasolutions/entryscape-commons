define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/string',
  'dojo/on',
  'dojo/date/locale',
  'dojo/dom-attr',
  'dojo/dom-class',
  '../defaults',
  'dojo/dom-style',
  'dojo/dom-construct',
  'di18n/localize',
  './ContentView',
  'dojo/text!./ImageViewTemplate.html',
  'i18n!nls/escoContentview',
], (declare, lang, string, on, locale, domAttr, domClass, defaults, domStyle,
    domConstruct, localize, ContentView, template) =>
  declare([ContentView], {
    templateString: template,
    nlsBundles: ['escoContentview'],
    bid: 'escoImageView',
    includeMetadataPresentation: true,

    postCreate() {
      this.inherited(arguments);

      on(this.__imgContent, 'load', () => {
        domStyle.set(this.__imgContent, 'display', 'block');
        domStyle.set(this.__message, 'display', 'none');
      });

      on(this.__imgContent, 'error, abort', () => {
        this.localeReady.then(() => {
          domStyle.set(this.__spinner, 'display', 'none');
          domAttr.set(this.__messageText, 'innerHTML', this.NLSBundle0.imageCannotBeLoaded);
          domClass.remove(this.__message, 'alert-info');
          domClass.add(this.__message, 'alert-danger');
        });
      });

      this.entry.refresh().then((entry) => {
        let imageURI;
        if (this.contentViewConf.property) {
          const md = entry.getMetadata();
          imageURI = md.findFirstValue(null, this.contentViewConf.property);
        }
        if (!imageURI) {
          const format = entry.getEntryInfo().getFormat();
          if (entry.isLink() || (format != null && format.indexOf('image/') === 0)) {
            imageURI = entry.getResourceURI();
          }
        }
        if (imageURI) {
          domStyle.set(this.__spinner, 'display', '');
          domStyle.set(this.__message, 'display', '');
          domClass.add(this.__message, 'alert-info');
          this.localeReady.then(() => {
            domAttr.set(this.__messageText, 'innerHTML', this.NLSBundle0.loadingImage);
          });
          domAttr.set(this.__imgContent, 'src', imageURI);
        } else {
          this.localeReady.then(() => {
            domStyle.set(this.__message, 'display', '');
            domAttr.set(this.__messageText, 'innerHTML', this.NLSBundle0.noImageProvided);
            domClass.remove(this.__message, 'alert-info');
            domClass.add(this.__message, 'alert-warning');
          });
        }
      });
    },
  }));
