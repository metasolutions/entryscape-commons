define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-style',
  'rdforms/view/Presenter',
  './ContentViewTabs',
  '../create/typeIndex',
  '../dialog/TitleDialog',
  'entryscape-commons/defaults',
  'i18n!nls/escoContentview',
], (declare, domConstruct, domStyle, Presenter, ContentViewTabs, typeIndex, TitleDialog,
    defaults) =>
  declare([TitleDialog.ContentNLS], {
    templateString: `<div>
    <div data-dojo-attach-point="__contentviewer"></div>
    <div data-dojo-attach-point="__presenter"></div>
</div>`,
    nlsBundles: ['escoContentview'],
    nlsHeaderTitle: 'contentViewDialogHeader',
    nlsFooterButtonLabel: 'contentViewDialogCloseLabel',

    show(entry, tmpl, entityConf) {
      let template = tmpl;
      let conf = entityConf;
      if (!conf) {
        conf = typeIndex.getConf(entry);
      }
      if (!template && conf && conf.template) {
        template = defaults.get('itemstore').getItem(conf.template);
      }
      if (!template) {
        console.warn(`Cannot show uri "${uri}" since there is no suitable template`);
        return;
      }
      if (conf && conf.contentviewers) {
        this.useContentViewer(entry, conf);
      } else {
        this.usePresenter();
        let graph;
        if (entry.isReference()) {
          graph = entry.getCachedExternalMetadata();
        } else if (entry.isLinkReference()) {
          graph = entry.getCachedExternalMetadata().clone().addAll(entry.getMetadata());
        } else {
          graph = entry.getMetadata();
        }
        this.presenter.show({
          resource: entry.getResourceURI(),
          graph,
          template,
        });
      }
      this.dialog.show();
    },
    usePresenter() {
      if (!this.presenter) {
        this.presenter = new Presenter({ compact: true }, this.__presenter);
      }
      domStyle.set(this.presenter.domNode, 'display', '');
      if (this.contentviewer) {
        domStyle.set(this.contentviewer.domNode, 'display', 'none');
      }
    },
    useContentViewer(entry, conf) {
      if (this.contentviewer) {
        this.contentviewer.destroy();
      }
      this.contentviewer = new ContentViewTabs({ entityConf: conf, entry },
        domConstruct.create('div', null, this.__contentviewer));
      if (this.presenter) {
        domStyle.set(this.presenter.domNode, 'display', 'none');
      }
    },
  }));
