define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-class',
  'dojo/Deferred',
  'dojo/promise/all',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./ContentViewTabsTemplate.html',
  '../defaults',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/on',
  'config',
  'entryscape-commons/list/common/ListDialogMixin',
], (declare, lang, domClass, Deferred, all, _WidgetBase, _TemplatedMixin, template,
    defaults, domStyle, domConstruct, on, config, ListDialogMixin) =>
  declare([_WidgetBase, _TemplatedMixin, ListDialogMixin], {
    bid: 'escoContentViewTabs',
    templateString: template,
    entityConf: null, // Must be provided
    entry: null, // Must be provided
    initialTab: null,
    activeTab: null,

    postCreate() {
      this.inherited(arguments);
      this.tabs = {};
      if (this.entityConf.contentviewers.length === 1) {
        domStyle.set(this.__tabList, 'display', 'none');
      }
      const contenttabs = this.entityConf.contentviewers.map(cv => (typeof cv === 'string' ? { name: cv } : cv));
      all(contenttabs.map(contentTab => this.createTab(contentTab), this))
        .then(() => {
          if (this.initialTab) {
            this.switchTab(this.initialTab);
          } else {
            this.switchTab(contenttabs[0].name);
          }
        });
    },

    createTab(contentTabObj) {
      // contentTab object {name, param}
      const d = new Deferred();
      const contentTab = contentTabObj.name;
      const cntViewConf = this.getContentViewConf(contentTab);
      if (!cntViewConf) {
        console.warn(`Contentviewer: ${contentTab} is missing in configuration`);
        d.resolve();
        return d;
      }
      require([cntViewConf.class], lang.hitch(this, (Cls) => {
        const li = domConstruct.create('li', { role: 'presentation' }, this.__tabList);
        on(li, 'click', lang.hitch(this, (ev) => {
          ev.stopPropagation();
          this.switchTab(contentTab);// pass tab name
        }));
        domConstruct.create('a', {
          innerHTML: defaults.get('localize')(cntViewConf.label),
          role: 'tab',
          class: `${this.bid}__tab`,
          'data-toggle': 'tab',
        }, li);

        const view = new Cls({
          tabs: this,
          contentViewConf: lang.mixin({}, cntViewConf, contentTabObj),
          entityConf: this.entityConf,
          entry: this.entry,
        }, domConstruct.create('div', null, this.__tabContent));
        domClass.add(view.domNode, 'tab-pane');
        domStyle.set(view.domNode, 'role', 'tabpanel');

        this.tabs[contentTab] = {
          tabPanel: view.domNode,
          tabList: li,
        };
        d.resolve();
      }));
      return d;
    },
    getContentViewConf(contentTab) {
      let cntViewConf;
      config.contentviewers.forEach((contentViewer) => {
        if (contentViewer.name === contentTab) {
          cntViewConf = contentViewer;
        }
      });
      return cntViewConf;
    },
    switchTab(tabName) {
      Object.keys(this.tabs).forEach((tab) => {
        if (tab !== tabName) {
          domClass.remove(this.tabs[tab].tabList, 'active');
          domClass.remove(this.tabs[tab].tabPanel, 'active');
        }
      });
      this.activeTab = tabName;
      domClass.add(this.tabs[tabName].tabList, 'active');
      domClass.add(this.tabs[tabName].tabPanel, 'active');
    },
  }));
