define([
  'dojo/_base/declare',
  './ContentView',
], (declare, ContentView) => declare([ContentView], {
  templateString: '<div ><div data-dojo-attach-point="__metadataViewer"></div></div>',
  includeMetadataPresentation: true,
}));
