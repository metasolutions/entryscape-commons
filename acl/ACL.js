define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/dom-style',
  'dojo/dom-attr',
  'dojo/dom-prop',
  './ACLList',
  '../defaults',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'dojo/text!./ACLTemplate.html',
  'jquery',
  'i18n!nls/escoAcl',
], (declare, lang, domClass, domConstruct, domStyle, domAttr, domProp, ACLList, defaults,
    _WidgetBase, _TemplatedMixin, NLSMixin, template) =>

  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escoAcl'],
    aclList: null,
    includeOverride: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.aclList = new ACLList({ nlsTypeaheadPlaceholderKey: 'searchPrincipals' }, this.aclListNode);
      this.aclList.onChange = lang.hitch(this, 'onChange');
      this.contextAclList = new ACLList({
        nlsTypeaheadPlaceholderKey: 'searchPrincipals',
        readOnly: true,
        focusOnResource: true,
      }, this.contextAclListNode);
      this.contextAclList.typeahead.setDisabled(true);
    },

    onChange() {
    },

    show(params) {
      const context = defaults.get('context');
      if (context && params.entry) {
        context.getEntryById(params.entry).then(lang.hitch(this, this.showEntry));
      }
    },
    showEntry(entry) {
      this.entry = entry;
      this.aclList.setACLFromEntry(entry);
      entry.getContext().getEntry().then(lang.hitch(this, this.showContextEntry));
      if (entry.getEntryInfo().hasACL()) {
        domProp.set(this.customOption, 'checked', true);
        this.changeToCustomSharing();
      } else {
        domProp.set(this.inheritedOption, 'checked', true);
        this.changeToInheritedSharing();
      }
    },
    localeChange() {
      domAttr.set(this.noContextOwners, 'innerHTML', this.NLSBundle0.noContextOwners);
    },
    showContextEntry(contextEntry) {
      this.contextAclList.setACLFromEntry(contextEntry);
      const acl = contextEntry.getEntryInfo().getACL(true);
      const es = defaults.get('entrystore');
      const rdfutils = defaults.get('rdfutils');
      if (acl.admin.length === 0) {
        domStyle.set(this.noContextOwners, 'display', '');
        domAttr.set(this.ownersList, 'innerHTML', '');
      } else {
        domStyle.set(this.noContextOwners, 'display', 'none');
        domAttr.set(this.ownersList, 'innerHTML', '');
        for (let i = 0; i < acl.admin.length; i++) {
          const span = domConstruct.create('li', {
            class: 'principalName',
            innerHTML: acl.admin[i],
          }, this.ownersList);
          es.getEntry(es.getEntryURI('_principals', acl.admin[i])).then(lang.hitch(this, (spanNode, entry) => {
            domAttr.set(spanNode, 'innerHTML', rdfutils.getLabel(entry) || entry.getId());
          }, span));
        }
      }
    },
    changeToInheritedSharing() {
      this.entryACL = false;
      domClass.add(this.domNode, 'inheritedSharingMode');
      domClass.remove(this.domNode, 'explicitSharingMode');
      this.onChange();
    },
    changeToCustomSharing() {
      this.entryACL = true;
      domClass.remove(this.domNode, 'inheritedSharingMode');
      domClass.add(this.domNode, 'explicitSharingMode');
      this.onChange();
    },
    saveACL() {
      const ei = this.entry.getEntryInfo();
      if (this.entryACL) {
        ei.setACL(this.aclList.getACL());
      } else {
        ei.setACL({});
      }
      return ei.commit();
    },
  }));
