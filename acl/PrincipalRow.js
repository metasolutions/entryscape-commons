define([
  'dojo/_base/declare',
  'dojo/on',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dojo/dom-class',
  '../list/EntryRow',
  'jquery',
  'bootstrap/dropdown',
], (declare, on, domAttr, domConstruct, domClass, EntryRow, jquery) => {
  const rights = [
    'none',
    'read',
    'write',
    'admin',
    'rread',
    'rwrite',
    'mread',
    'mwrite',
    'mwrite_rread',
    'rwrite_mread',
  ];

  const rrights = [
    'none',
    'rread',
    'rwrite',
    'admin',
  ];

  const rmap = {
    read: 'rread',
    write: 'rwrite',
    mwrite_rread: 'rread',
    rwrite_mread: 'rwrite',
  };

  return declare([EntryRow], {
    showCol1: true,

    postCreate() {
      this.inherited(arguments);

      if (this.list.isContextsACL || this.list.isPrincipalsACL) {
        this.focusOnResource = true;
      }
      domClass.add(this.domNode, 'principalRow');
      if (this.list.readOnly) {
        domClass.add(this.domNode, 'readOnly');
      }
    },

    installButtonOrNot(params) {
      const id = this.entry.getId();
      if (params.name === 'remove') {
        if (id === '_guest' || id === '_users') {
          return false;
        }
        return !this.list.readOnly;
      }
      return this.inherited(arguments);
    },

    updateLocaleStrings() {
      this.inherited('updateLocaleStrings', arguments);
    },

    getNLSLabel(keyParam) {
      let key = keyParam;
      if (this.list.isContextsACL) {
        key += 'ContextsOption';
      } else if (this.list.isPrincipalsACL) {
        key += 'PrincipalsOption';
      } else if (this.list.isContextACL) {
        key += 'ContextOption';
      } else {
        key += 'Option';
      }
      return (this.nlsSpecificBundle && this.nlsSpecificBundle[key]) || this.nlsGenericBundle[key] || '';
    },
    getNLSTitle(keyParam) {
      let key = keyParam;
      if (this.list.isContextsACL) {
        key += 'ContextsOptionTitle';
      } else if (this.list.isPrincipalsACL) {
        key += 'PrincipalsOptionTitle';
      } else if (this.list.isContextACL) {
        key += 'ContextOptionTitle';
      } else {
        key += 'OptionTitle';
      }

      return (this.nlsSpecificBundle && this.nlsSpecificBundle[key]) || this.nlsGenericBundle[key] || '';
    },

    renderCol1() {
      if (this.entry.isUser()) {
        domAttr.set(this.col1Node, 'innerHTML', '<i class="fa fa-user fa-lg"></i>');
      } else {
        domAttr.set(this.col1Node, 'innerHTML', '<i class="fa fa-users fa-lg"></i>');
      }
    },

    getRenderName() {
      if (this.nlsSpecificBundle) {
        if (this.entry.getId() === '_guest') {
          return this.nlsSpecificBundle.guestLabel;
        } else if (this.entry.getId() === '_users') {
          return this.nlsSpecificBundle.usersLabel;
        }
      }
      return this.inherited(arguments);
    },

    renderCol3() {
      domAttr.set(this.col3Node, 'innerHTML', '');
      if (this.nlsGenericBundle) { // Localization strings are loaded.
        const list = this.list;
        const entryId = this.entry.getId();
        const wrapperNode = domConstruct.create('span', { class: 'dropdown pull-right' }, this.col3Node);
        let currentRight = list.getRight(entryId);
        if (this.focusOnResource && rmap.hasOwnProperty(currentRight)) {
          currentRight = rmap[currentRight];
        }
        const simpleRight = currentRight === 'none' || currentRight === 'read'
          || currentRight === 'write' || currentRight === 'admin';
        const currentRightLabel = this.getNLSLabel(currentRight);
        const currentRightTitle = this.getNLSTitle(currentRight);
        const textNode = domConstruct.create('span', {
          class: 'principalRight',
          'data-toggle': 'dropdown',
        }, wrapperNode);
        const currentRightNode = domConstruct.create('span', {
          innerHTML: currentRightLabel,
          title: currentRightTitle,
        }, textNode);
        if (this.list.readOnly) {
          return;
        }
        domConstruct.create('span', { class: 'caret' }, textNode);
        const ul = domConstruct.create('ul', { class: 'dropdown-menu dropdown-menu-right' }, wrapperNode);
        let selectedLi;
        const options = this.focusOnResource ? rrights : rights;
        options.forEach((right, i) => {
          let li;
          const label = this.getNLSLabel(right);
          const title = this.getNLSTitle(right);
          const labelA = `<a>${label}</a>`;

          if (i > 3) {
            li = domConstruct.create('li', {
              innerHTML: labelA,
              class: 'extraRights selectable',
              title,
            }, ul);
          } else {
            li = domConstruct.create('li', {
              innerHTML: labelA,
              class: 'selectable',
              title,
            }, ul);
          }
          on(li, 'click', () => {
            if (selectedLi) {
              domClass.remove(selectedLi, 'disabled');
              domClass.add(selectedLi, 'selectable');
            }
            domClass.add(li, 'disabled');
            domClass.remove(li, 'selectable');
            selectedLi = li;
            domAttr.set(currentRightNode, 'innerHTML', label);
            list.setRight(entryId, right);
          });
          if (right === currentRight) {
            domClass.add(li, 'disabled');
            domClass.remove(li, 'selectable');
            selectedLi = li;
          }

          if (right === 'admin' && !this.focusOnResource) {
            domConstruct.create('li', { role: 'separator', class: 'divider' }, ul);
            const moreLabel = `<a>${(this.nlsSpecificBundle && this.nlsSpecificBundle.moreOptions)
            || this.nlsGenericBundle.moreOptions || ''}</a>`;
            const moreLabelTitle =
              (this.nlsSpecificBundle && this.nlsSpecificBundle.moreOptionsTitle)
              || this.nlsGenericBundle.moreOptionsTitle || '';
            const more = domConstruct.create('li', {
              class: 'moreOptions selectable',
              innerHTML: moreLabel,
              title: moreLabelTitle,
            }, ul);
            on(more, 'click', (ev) => {
              domClass.remove(wrapperNode, 'simpleRight');
              ev.stopPropagation();
            });
          }
        });
        if (simpleRight) {
          domClass.add(wrapperNode, 'simpleRight');
        }

        jquery(ul).dropdown();
      }
    },
  });
});
