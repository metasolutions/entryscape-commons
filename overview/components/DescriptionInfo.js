define([
  'mithril',
], (m) => {
  /**
   * HTML: dt - dl
   */
  const DescriptionInfo = {
    view(vnode) {
      return [
        m('dt', vnode.attrs.label),
        m('dd', vnode.attrs.value),
      ];
    },
  };

  return DescriptionInfo;
});
