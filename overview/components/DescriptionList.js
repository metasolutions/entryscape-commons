define([
  './DescriptionInfo',
  'mithril',
], (DescriptionInfo, m) => {
  /**
   * HTML dl
   *
   * @type {{sList: Array, view: ((vnode))}}
   */
  const DescriptionList = {
    view(vnode) {
      const vnodeList = [];
      vnode.attrs.sList.forEach((item) => {
        const { label, value } = item;
        vnodeList.push(m(DescriptionInfo, { label, value }));
      });

      return m('dl', vnodeList);
    },
  };

  return DescriptionList;
});
