/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */

define({
  root: {
    confirm: "Yes",
    reject: "No",
    ok: "OK",
  },
  sv: true,
  da: true,
  de: true,
});
