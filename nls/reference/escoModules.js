/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */

define({
  root: {
    "catalog-title": "Catalog",
    "terms-title": "Terms",
    "import-title": "Import",
    "admin-title": "Admin",
    "workbench-title": "Workbench",
    "search-title": "Search",
  },
  sv: true,
  da: true,
  de: true,
});
