/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */

define({
  root: {
    progressHeader: "Checklist",
    progressFooterButton: "Save",
    mandatoryChecklistTitle: "Mandatory",
  },
  sv: true,
  da: true,
  de: true,
});
