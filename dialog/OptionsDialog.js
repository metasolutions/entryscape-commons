define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'jquery',
  'bootstrap/modal',
  'dojo/text!./OptionsDialogTemplate.html',
  'dojo/_base/array',
  'dojo/on',
], (declare, lang, Deferred, domAttr, domConstruct, _WidgetBase, _TemplatedMixin, jquery,
    modal, template, array, on) =>
  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    postCreate() {
      this.inherited('postCreate', arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
      domConstruct.place(this.domNode, 'entryscape_dialogs');
      jquery(this.domNode).on('hide.bs.modal', lang.hitch(this, function () {
        if (this.lock !== true && this._deferred != null) {
          const d = this._deferred;
          delete this._deferred;
          d.reject(false);
        }
      }));
      jquery(this.domNode).on('hidden.bs.modal', lang.hitch(this, function () {
        this._showing = false;
        if (this._showFunc) {
          this._showFunc();
          delete this._showFunc;
        }
      }));
    },
    show(message, options) {
      this._deferred = new Deferred();
      const f = function () {
        domConstruct.empty(this.buttonsFooter);
        domAttr.set(this.message, 'innerHTML', message);
        array.forEach(options, lang.hitch(this, this.installButton));
        jquery(this.domNode).modal('show');
        this._showing = true;
      };
      if (this._showing) {
        this._showFunc = f;
      } else {
        f.apply(this);
      }
      return this._deferred;
    },
    installButton(params) {
      let el;
      if (params.primary) {
        el = domConstruct.create('button', {
          type: 'button',
          class: 'btn btn-primary',
          innerHTML: params.buttonLabel,
        }, this.buttonsFooter);
      } else {
        el = domConstruct.create('button', {
          type: 'button',
          class: 'btn btn-default',
          innerHTML: params.buttonLabel,
        }, this.buttonsFooter);
      }
      on(el, 'click', lang.hitch(this, function (ev) {
        ev.stopPropagation();
        this.lock = true;
        jquery(this.domNode).modal('hide');
        const d = this._deferred;
        delete this._deferred;
        d.resolve(params.name);
        this.lock = false;
      }));
    },
  }));
