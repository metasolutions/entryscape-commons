define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/on',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'jquery',
  'bootstrap/modal',
  'dojo/text!./FileDialogTemplate.html',
], (declare, lang, domAttr, domStyle, domConstruct, on, _WidgetBase, _TemplatedMixin, jquery, modal, template) =>
  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    validFile: false,
    postCreate() {
      this.inherited('postCreate', arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
      domConstruct.place(this.domNode, 'entryscape_dialogs');
      jquery(this.domNode).on('hide.bs.modal', lang.hitch(this, function () {
        if (this.lock !== true && this._confirmCallback != null) {
          this._confirmCallback(false);
          delete this._confirmCallback;
        }
      }));
      on(this.fileInput, 'change', lang.hitch(this, this.fileSelected));
    },
    clear() {
      domAttr.set(this.uploadNode, 'disabled', true);
      domAttr.set(this.selectedFile, 'value', '');
      this.validFile = false;
    },
    fileSelected() {
      this.numFiles = this.fileInput.files ? this.fileInput.files.length : 1;
      this.label = domAttr.get(this.fileInput, 'value').replace(/\\/g, '/').replace(/.*\//, '');
      this.validFile = true;
      domAttr.set(this.uploadNode, 'disabled', false);
      domAttr.set(this.selectedFile, 'value', this.label);
    },
    show(message, uploadLabel, cancelLabel, callback) {
      this._uploadCallback = callback;
      this.clear();
      this.unlockUpload(); // Just to be sure.
      domAttr.set(this.uploadLabelNode, 'innerHTML', uploadLabel || 'Upload');
      domAttr.set(this.cancelLabelNode, 'innerHTML', cancelLabel || 'Cancel');
      jquery(this.domNode).modal('show');
    },
    cancel() {
      if (this.lock) {
        return;
      }
      this.clear();
      this.unlockUpload();
      jquery(this.domNode).modal('hide');
      delete this._uploadCallback;
    },
    unlockUpload() {
      this.lock = false;
      domStyle.set(this.spinner, 'display', 'none');
      domAttr.set(this.uploadNode, 'disabled', false);
      domAttr.set(this.cancelLabelNode, 'disabled', false);
      domAttr.set(this.messageNode, 'innerHTML', '');
      jquery(this.domNode).modal('hide');
    },
    upload() {
      if (!this.validFile || this.lock) {
        return;
      }
      const unlock = lang.hitch(this, this.unlockUpload);
      this.lock = true;
      domStyle.set(this.spinner, 'display', '');
      domAttr.set(this.uploadNode, 'disabled', true);
      domAttr.set(this.cancelLabelNode, 'disabled', true);
      this._uploadCallback(this.fileInput, this.label).then(() => {
        unlock();
      }, function (errMessage) {
        unlock();
        if (typeof errMessage === 'string') {
          domAttr.set(this.messageNode, 'innerHTML', errMessage);
        }
      });
      delete this._uploadCallback;
    },
  }));
