define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  'dojo/Deferred',
  'dojo/html',
  'di18n/locale',
  './TitleDialog',
], (declare, lang, domConstruct, Deferred, html, locale, TitleDialog) =>

  declare([TitleDialog], {
    includeFooter: false,
    maxWidth: 800,
    postCreate() {
      this.inherited('postCreate', arguments);
      this.mainNode = domConstruct.create('div', null, this.containerNode);
    },
    show(path, title, callback) {
      if (!path || path === '') {
        const errorTxt = 'No path given to acknowledge text';
        console.error(errorTxt);
        return Promise.reject(new Error(errorTxt));
      }
      this.inherited(arguments);
      const p = new Promise((resolve, reject) => {
        const t = setTimeout(() => {
          reject(new Error(`Timeout when loading file: ${path}`));
        }, 500);
        html.set(this.titleNode, title);
        const containerNode = this.mainNode;
        const language = locale.get() !== 'en' ? `_${locale.get()}` : '';
        require([`dojo/text!${path}${language}.html`], (content) => {
          html.set(containerNode, content);
          resolve(true);
          clearTimeout(t);
        }, () => {
          require([`dojo/text!${path}.html`], (content) => {
            html.set(containerNode, content);
            resolve(true);
            clearTimeout(t);
          });
        });
      });
      if (callback) {
        p.then(callback);
      }
      return p;
    },
  }));
