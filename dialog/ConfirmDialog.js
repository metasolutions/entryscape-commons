define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'jquery',
  'di18n/NLSMixin',
  'bootstrap/modal',
  'dojo/text!./ConfirmDialogTemplate.html',
  'i18n!nls/escoDialogs',
], (declare, lang, Deferred, domAttr, domConstruct, _WidgetBase, _TemplatedMixin, jquery,
    NLSMixin, modal, template) =>
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['escoDialogs'],
    postCreate() {
      this.inherited('postCreate', arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
      domConstruct.place(this.domNode, 'entryscape_dialogs');
      jquery(this.domNode).on('hide.bs.modal', lang.hitch(this, function () {
        if (this.lock !== true && this._deferred != null) {
          const d = this._deferred;
          delete this._deferred;
          d.reject(false);
        }
      }));
      jquery(this.domNode).on('hidden.bs.modal', lang.hitch(this, function () {
        this._showing = false;
        if (this._showFunc) {
          this._showFunc();
          delete this._showFunc;
        }
      }));
    },
    show(message, confirmLabel, rejectLabel, callback) {
      this._deferred = new Deferred();
      if (callback) {
        this._deferred.then(callback, callback);
      }
      const f = function () {
        domAttr.set(this.confirmLabelNode, 'innerHTML', confirmLabel || this.NLSBundle0.confirm);
        domAttr.set(this.rejectLabelNode, 'innerHTML', rejectLabel || this.NLSBundle0.reject);
        domAttr.set(this.confirmMessage, 'innerHTML', message);
        jquery(this.domNode).modal('show');
        this._showing = true;
      };
      if (this._showing) {
        this._showFunc = f;
      } else {
        f.apply(this);
      }
      return this._deferred;
    },
    reject() {
      this.lock = true;
      jquery(this.domNode).modal('hide');
      const d = this._deferred;
      delete this._deferred;
      d.reject(false);
      this.lock = false;
    },
    confirm() {
      this.lock = true;
      jquery(this.domNode).modal('hide');
      const d = this._deferred;
      delete this._deferred;
      d.resolve(true);
      this.lock = false;
    },
  }));
