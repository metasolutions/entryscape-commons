define([
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/query',
  'dojo/NodeList-dom', // To allow place method on nodelist returned by query.
  'dojo/on',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  './SideDialog',
  'dojo/text!./HeaderDialogTemplate.html',
], (lang, declare, domClass, domConstruct, query, NodeListDom, on, _WidgetBase, _TemplatedMixin,
    SideDialog, template) => {
  const HeaderDialog = declare([SideDialog], {
    templateString: template,
    moveOfHeaderAndFooter: true,
    postCreate() {
      this.inherited('postCreate', arguments);
      on(this.close, 'click', lang.hitch(this, 'conditionalHide'));
      this.moveHeaderAndFooter();
    },
    moveHeaderAndFooter() {
      if (query('header', this.containerNode).place(this.headerExtentionNode).length > 0) {
        domClass.add(this.domNode, 'spaHeader');
      }
      if (query('footer', this.containerNode).place(this.footerExtentionNode).length > 0) {
        domClass.add(this.domNode, 'spaFooter');
      }
    },
  });

  HeaderDialog.Content = declare([_WidgetBase, _TemplatedMixin], {
    templateString: '<h1>Override me!</h1>',

    buildRendering() {
      const dialogNode = this.srcNodeRef || domConstruct.create('div');
      const params = SideDialog.createParams(this, ['indent', 'firstIndent', 'maxWidth']);
      params.moveOfHeaderAndFooter = false;
      this.dialog = new HeaderDialog(params, dialogNode);
      this.srcNodeRef = domConstruct.create('div', null, this.dialog.containerNode);
      this.inherited(arguments);
      if (this.dialogClass) {
        domClass.add(this.dialog.domNode, this.dialogClass);
      }
    },
    postCreate() {
      this.dialog.moveHeaderAndFooter();
    },
  });

  return HeaderDialog;
});
