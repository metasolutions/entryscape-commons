define([
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/window',
  'dojo/on',
  'dojo/aspect',
  'dojo/_base/fx',
  'dojo/fx',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./SideDialogTemplate.html',
], (lang, declare, win, on, aspect, basefx, fx, domStyle, domConstruct, _WidgetBase,
    _TemplatedMixin, template) => {
  const mobileMaxWidth = 415;
  let level = 0;
  let maxWidth = 0;
  let maxWidthOwner = null;
  let busy = false;

  const SideDialog = declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    indent: 25,
    firstIndent: 75,
    maxWidth: 0,
    _isHidden: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      on(this.underlay, 'click', lang.hitch(this, function () {
        if (!busy) {
          this.conditionalHide();
        }
      }));
    },
    show() {
      this._isHidden = false;
      if (this._addedToDom !== true) {
        domConstruct.place(this.domNode, 'entryscapeDialogs');
        this._addedToDom = true;
      }
      const box = win.getBox();
      const indent = (this.indent * level) + this.firstIndent;
      level += 1;
      let end;
      let dialogWidth;
      if (maxWidth === 0 && this.maxWidth > 0 && this.maxWidth < box.w - this.indent) {
        maxWidth = this.maxWidth + this.indent;
        maxWidthOwner = this;
      }

      if (maxWidth > 0) {
        end = box.w - (maxWidth - indent);
        dialogWidth = maxWidth - indent;
      } else if (box.w < mobileMaxWidth) {
        end = 0;
        dialogWidth = box.w;
      } else {
        end = indent;
        dialogWidth = box.w - indent;
      }
      domStyle.set(this.domNode, 'display', 'block');
      domStyle.set(this.domNode, 'zIndex', `${1031 + level}`);
      domStyle.set(this.domNode, 'opacity', 0);
      const slide = basefx.animateProperty({
        node: this.dialogContent,
        properties: {
          left: { start: box.w, end, units: 'px' },
          right: { start: -dialogWidth, end: 0, units: 'px' },
        },
      });
      const fadeIn = basefx.fadeIn({ node: this.domNode });
      fx.combine([slide, fadeIn]).play();
    },
    isHidden() {
      return this._isHidden;
    },
    conditionalHide() {
      this.hide();
    },
    hide() {
      if (level <= 0) { // Avoid to many hide calls
        const err = new Error();
        console.warn(`Too many hide calls to sideDialog, check the code.\n${err.stack}`);
        return;
      }
      busy = true;
      level -= 1;
      const box = win.getBox();
      const indent = (this.indent * level) + this.firstIndent;
      let start;
      let dialogWidth;
      if (maxWidth > 0) {
        start = box.w - (maxWidth - indent);
        dialogWidth = maxWidth - indent;
      } else {
        start = indent;
        dialogWidth = box.w - indent;
      }
      const slide = basefx.animateProperty({
        node: this.dialogContent,
        properties: {
          left: { end: box.w, start, units: 'px' },
          right: { end: -dialogWidth, start: 0, units: 'px' },
        },
      });
      const fadeOut = basefx.fadeOut({ node: this.domNode });

      const anim = fx.combine([slide, fadeOut]);
      aspect.after(anim, 'onEnd', lang.hitch(this, function () {
        domStyle.set(this.domNode, 'display', 'none');
        this._isHidden = true;
        busy = false;
        this.hideComplete();
      }), true);
      anim.play();
      if (this === maxWidthOwner) {
        maxWidth = 0;
        maxWidthOwner = null;
      }
    },
    hideComplete() {
    },
  });

  SideDialog.createParams = function (from, attrs) {
    const obj = {};
    for (let i = 0; i < attrs.length; i++) {
      const p = attrs[i];
      if (typeof from[p] !== 'undefined') {
        obj[p] = from[p];
      }
    }
    return obj;
  };

  SideDialog.Content = declare([_WidgetBase, _TemplatedMixin], {
    templateString: '<h1>Override me!</h1>',

    buildRendering() {
      const dialogNode = this.srcNodeRef || domConstruct.create('div');
      this.dialog = new SideDialog(SideDialog.createParams(this,
        ['indent', 'firstIndent', 'maxWidth']), dialogNode);
      this.srcNodeRef = domConstruct.create('div', null, this.dialog.containerNode);
      this.inherited(arguments);
    },
  });

  return SideDialog;
});
