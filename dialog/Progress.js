define([
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/_base/fx',
  'dojo/dom-style',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'fuelux/loader', // In template
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'jquery',
  'dojo/text!./ProgressTemplate.html',
], (lang, declare, basefx, domStyle, domAttr, domConstruct, loader, _WidgetBase,
    _TemplatedMixin, jquery, template) =>

  declare([_WidgetBase, _TemplatedMixin], {
    templateString: template,
    progressDelay: 120,
    postCreate() {
      this.inherited('postCreate', arguments);
      this.ownerDocumentBody.appendChild(this.domNode);
      domConstruct.place(this.domNode, 'entryscape_dialogs');
    },
    show(promise) {
      if (this.lock) {
        return;
      }
      this.lock = true;
      domStyle.set(this.domNode, 'display', 'block');
      domAttr.set(this.progress, 'innerHTML', '');
      const div = domConstruct.create('div', null, this.progress);
      this._loader = domConstruct.create('div', { class: 'loader' }, div);
      jquery(this._loader).loader();
      this.fadeIn = basefx.fadeIn({ node: this.domNode });
      promise.then(lang.hitch(this, this.hide), lang.hitch(this, this.hide));
      this._timer = setTimeout(lang.hitch(this, function () {
        this.fadeIn.play();
        delete this._timer;
      }), this.progressDelay);
    },
    hide() {
      if (this._timer) {
        clearTimeout(this._timer);
      }
      delete this.lock;
      this.fadeIn.stop();
      jquery(this._loader).loader('destroy');
      domStyle.set(this.domNode, 'display', 'none');
    },
  }));
