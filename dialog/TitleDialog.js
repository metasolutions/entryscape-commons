define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/dom-construct',
  'dojo/dom-geometry',
  'dojo/on',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  './SideDialog',
  'di18n/localize',
  'dojo/text!./TitleDialogTemplate.html',
], (declare, lang, domAttr, domClass, domStyle, domConstruct, domGeometry, on, _WidgetBase,
    _TemplatedMixin, NLSMixin, SideDialog, localize, template) => {
  /**
   * A SideDialog with a header and a footer.
   *
   * The header contains a configurable title and a close button (in the form of an X) on
   * the right of the title.
   *
   * The footer contains a single button aligned to the right with a configurable label
   * and title (html attribute, triggered via mouseover).
   *
   * Make sure you specify the nls keys to be used in the bundle (via the attributes
   * nlsHeaderTitle,
   * nlsFooterButtonLabel and nlsFooterButtonTitle), reasonable defaults are provided.
   */
  const TitleDialog = declare([SideDialog], {
    templateString: template,
    nlsHeaderTitle: 'headerTitle',
    nlsFooterButtonLabel: 'footerButtonLabel',
    nlsFooterButtonTitle: '',
    includeFooter: true,
    maxWidth: 800, // Default
    // Attachpoints
    underlay: null,
    dialogContent: null,
    close: null,
    containerNode: null,
    footerButtonNode: null,
    footerButtonSpinner: null,
    headerNode: null,
    headerExtensionNode: null,
    titleNode: null,
    footerButtonLabelNode: null,
    defaultMessage: null,
    errorMessageAlert: null,
    errorMessage: null,

    postCreate() {
      this.inherited(arguments);
      if (this.includeFooter) {
        domClass.add(this.domNode, 'spaFooter');
      }
      on(this.close, 'click', lang.hitch(this, 'conditionalHide'));
    },

    /**
     * Localize the title (in the header) and button (in the footer),
     * the nlsKeys used must be specified via the
     * nlsHeaderTitle, nlsFooterButtonLabel and nlsFooterButtonTitle attributes.
     *
     * @param {object} bundle the bundle with the localized strings.
     * @param {object} params to be used when localizing the strings, may be left out if
     * the title and label name is not parameterized.
     */
    updateLocaleStrings(bundle, params) {
      this.updateLocaleStringsExplicit(
        localize(bundle, this.nlsHeaderTitle, params || {}),
        (bundle[this.nlsFooterButtonLabel || ''] ? localize(bundle, this.nlsFooterButtonLabel, params || {}) : null),
        (bundle[this.nlsFooterButtonTitle || ''] ? localize(bundle, this.nlsFooterButtonTitle, params || {}) : null));
    },

    updateLocaleStringsExplicit(title, doneLabel, doneTitle) {
      if (this.titleNode != null) {
        domAttr.set(this.titleNode, 'innerHTML', title);
      }
      if (doneLabel && this.footerButtonLabelNode != null) {
        domAttr.set(this.footerButtonLabelNode, 'innerHTML', doneLabel);
      }
      if (doneTitle && this.footerButtonNode != null) {
        domAttr.set(this.footerButtonNode, 'title', doneTitle);
      }
      this.updateHeaderWidth();
    },

    show() {
      this.inherited(arguments);
      this.closeErrorMessage();
      this.updateHeaderWidth();
    },

    updateHeaderWidth() {
      if (this.isHidden() || this.titleNode == null || this.headerNode == null
        || this.headerExtensionNode == null || this.containerNode == null) {
        return;
      }
      domClass.remove(this.headerNode, 'multilineHeader');
      setTimeout(lang.hitch(this, function () {
        const cbox = domGeometry.getContentBox(this.headerNode);
        const tbox = domGeometry.getMarginBox(this.titleNode);
        const lbox = domGeometry.getMarginBox(this.headerExtensionNode);

        // Extra 10px to avoid problems of redraw when it is really close
        if (tbox.w + lbox.w + 10 > cbox.w) {
          domClass.add(this.headerNode, 'multilineHeader');
          setTimeout(lang.hitch(this, function () {
            const pbox = domGeometry.getContentBox(this.headerNode.parentNode);
            domStyle.set(this.containerNode, 'top', `${pbox.h + 2}px`);
          }), 1);
        } else {
          domStyle.set(this.containerNode, 'top', '');
        }
      }), 1);
    },

    closeErrorMessage() {
      domStyle.set(this.errorMessageAlert, 'display', 'none');
      domStyle.set(this.defaultMessage, 'display', '');
    },

    showErrorMessage(message) {
      domStyle.set(this.defaultMessage, 'display', 'none');
      domAttr.set(this.errorMessage, 'innerHTML', message);
      domStyle.set(this.errorMessageAlert, 'display', '');
    },

    /**
     * Override and return a promise if asynchronous operation is required.
     */
    footerButtonAction() {
      // Override.
    },

    lockFooterButton() {
      this.lock = true;
      domClass.add(this.footerButtonNode, 'disabled');
    },
    unlockFooterButton() {
      this.lock = false;
      domStyle.set(this.footerButtonSpinner, 'display', 'none');
      domClass.remove(this.footerButtonNode, 'disabled');
    },


    /**
     * Default behaviour closes the dialog, override if other behaviour is needed.
     */
    footerButtonClick() {
      if (this.lock) {
        return;
      }
      const res = this.footerButtonAction();
      if (res && typeof res.then === 'function') {
        this.lockFooterButton();
        domStyle.set(this.footerButtonSpinner, 'display', '');
        res.then(lang.hitch(this, function (status) {
          this.unlockFooterButton();
          if (!(status && 'stopHide' in status)) {
            this.hide();
          }
        }), lang.hitch(this, function (err) {
          if (lang.isString(err)) {
            this.showErrorMessage(err);
          } else if (typeof err === 'object' && err.message) {
            this.showErrorMessage(err.message);
            if (err.keepButtonLocked) {
              domStyle.set(this.footerButtonSpinner, 'display', 'none');
              return;
            }
          }
          this.unlockFooterButton();
        }));
      } else if (res === true || typeof res === 'undefined') {
        this.hide();
      } else {
        this.showErrorMessage(res);
      }
    },
  });


  TitleDialog.Content = declare([_WidgetBase, _TemplatedMixin], {
    templateString: '<h1>Override me!</h1>',

    buildRendering() {
      const dialogNode = this.srcNodeRef || domConstruct.create('div');
      this.dialog = new TitleDialog(SideDialog.createParams(this,
        ['indent', 'firstIndent', 'maxWidth', 'nlsHeaderTitle',
          'nlsFooterButtonLabel', 'nlsFooterButtonTitle', 'includeFooter',
        ]), dialogNode);
      if (this.footerButtonAction) {
        this.dialog.footerButtonAction = lang.hitch(this, this.footerButtonAction);
      }
      this.srcNodeRef = domConstruct.create('div', null, this.dialog.containerNode);
      this.inherited(arguments);
    },
  });

  TitleDialog.ContentNLS = declare([TitleDialog.Content, NLSMixin.Dijit], {
    nlsHeaderTitle: '',
    nlsFooterButtonLabel: '',
    nlsFooterButtonTitle: '',
    title: '',
    footerButtonLabel: '',
    footerButtonTitle: '',

    /**
     * Assumes you have either provided a title and closeLabel, or
     * an nlsBundles array with the name of the bundle to use and
     * the corresponding keys given by nlsHeaderTitle and nlsFooterButtonLabel.
     */
    localeChange() {
      if (this.title === '') {
        this.dialog.updateLocaleStrings(this.NLSBundle0);
      } else {
        const mesg = {};
        mesg[this.nlsHeaderTitle] = this.title;
        mesg[this.nlsFooterButtonLabel] = this.footerButtonLabel;
        mesg[this.nlsFooterButtonTitle] = this.footerButtonTitle;
        this.dialog.updateLocaleStrings(mesg);
      }
    },
  });

  return TitleDialog;
});
